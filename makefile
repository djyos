export topdir =$(shell pwd)
include ./djyos/port/arch.def

#在此列出所有包含源文件的子目录名，子目录的子目录不必列出
subdir = djyos app

#'('和$之间不能有空格
ifeq ($(MAKECMDGOALS),boot_rom) #编译初始化硬件部分，适合仿真器调试
    target = boot_rom
    op =
    dbg = -gdwarf-2 -Dboot
endif

#'('和'$'之间不能有空格
ifeq ($(MAKECMDGOALS),)
    target = debug
    op =
    dbg = -gdwarf-2 -Ddebug
endif

#加载和运行都在ram，但必须有boot_rom支持。先编译boot_rom，再
#编译debug_ram，把boot_rom写入flash后，上电执行，然后用仿真器调入debug_ram.elf调试。
ifeq ($(MAKECMDGOALS),debug)
    target = debug
    op =
    dbg = -gdwarf-2 -Ddebug
endif

#烧录到flash中，运行时内置bootloader把代码cp到ram中运行
#'('和$之间不能有空格
ifeq ($(MAKECMDGOALS),run_inram)
    target = run_inram
    op =-O2
    dbg =
endif

#烧录到flash中，并且直接在flash中运行
#'('和$之间不能有空格
ifeq ($(MAKECMDGOALS),run_inflash)   #加载和运行都在flash
    target = run_inflash
    op =-O2
    dbg =
endif

export target
export CC = arm-elf-gcc
export AS = arm-elf-gcc
export objcopy = arm-elf-objcopy -I elf32-littlearm -O binary
export size = arm-elf-size
export incdir =-I$(topdir)/djyos/include -I$(topdir)/djyos/port/include

export sub_make = @make --no-print-directory
export CFLAGS  = -c -mcpu=$(CPU) -Wall $(dbg) $(op) -nostartfiles -std=gnu99 #-pedantic
export ASFLAGS = -c -mcpu=$(CPU) -Wall $(dbg) $(op)    #-feliminate-dwarf2-dups
export LDFLAGS = -mcpu=$(CPU) -Wall -nostartfiles -lm -lc

debug \
run_inram \
run_inflash \
boot_rom: rm_obj_list $(subdir)     #rm_obj_list $(subdir)的书写顺序不能颠倒
	$(sub_make) -f make_ld $(MAKECMDGOALS)

#rom_obj、preload_obj、sysload_obj、critical_obj这几个连接指示用的变量在各子目录
#中生成并保存在ld_obj文件中，这里删除之，以便重新生成该文件
.PHONY: rm_obj_list
rm_obj_list:
	rm -f ld_obj_list

#进入各子目录编译，子目录中除编译该目录下的源文件成.o外，还按该目录下各文件的
#加载属性生成ld_obj文件
.PHONY: $(subdir)
$(subdir) :
	$(sub_make) -C $@ $(MAKECMDGOALS)

.PHONY: clean
clean:$(subdir)
	rm -f *.ld ld_obj_list

