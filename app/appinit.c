#include "inc_os.h"
#include "gpio.h"
#include "uart.h"

void flash_led(struct event_script *my_event)
{
    uint32_t led=0x11111111;
    while(1)
    {
        if(my_event->parameter0 ==1)
        {
            pg_gpio_reg->GPBDAT |= (led & 0x1e0);
            pg_gpio_reg->GPBDAT &= (led | 0xfe1f);
            led <<=1;
            if((led&0xf) == 0)
                led += 1;
        }else
        {
            pg_gpio_reg->GPBDAT &= 0xfe1f;
        }
        //执行本句后，事件将被阻塞，由于被同步的事件类型就是本事件的事件类型，
        //所以同步条件就是：再次发生本类型事件。
        //参数 ‘1’表示只要发生一次事件就可以了
        //参数500表示如果一直不发生，500mS后结束等待
        //关于事件类型弹出同步，参见《都江堰操作系统与嵌入式系统设计》的第5.3节
        djy_evtt_pop_sync(pg_event_running->evtt_id,1,500);
    }
}

//本想命名为main的，但调试器总是默认这是整个程序的入口点，罢了
void djy_main(struct event_script *my_event)
{
    uint16_t evtt_flash_led;
    uint32_t reg;
    reg = pg_gpio_reg->GPBCON;
    reg &= 0xfffc03ff;
    reg |= 0x00015400;
    pg_gpio_reg->GPBCON = reg;      //PB5~8 set to output for led
    reg = pg_gpio_reg->GPBUP;
    reg |= 0x1e0;
    pg_gpio_reg->GPBUP  = reg;     // The pull up function is disabled

    evtt_flash_led = djy_evtt_regist(true,true,cn_prio_RRS,1,
                                        flash_led,10,NULL);
    djy_event_pop(evtt_flash_led,1,0,0);
    test_flash_file();
}
int app_init(void)
{
    uint16_t evtt_main;
    //这两行禁止LCD输出
    *(volatile unsigned *)0x4d000000 &= 0x3fffe;
    pg_gpio_reg->GPGCON |= 0x300;

    module_init_uart0();
    module_init_uart1();
    module_init_uart2();
    module_init_fs_nandflash();
    evtt_main = djy_evtt_regist(true,true,cn_prio_RRS,1,
                                        djy_main,8192,"main function");
    //事件的两个参数暂设为0，如果用shell启动，可用来采集shell命令行参数
    djy_event_pop(evtt_main,0,0,0);
}
