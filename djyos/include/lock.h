//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 锁模块
//作者：lst
//版本：V1.0.0
//文件描述: 包含信号量和互斥量
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __semp_h__
#define __semp_h__

#ifdef __cplusplus
extern "C" {
#endif


struct  event_script;

//---------不限量信号的意义--------------
//1、不限量信号可提供受保护的资源的使用计数
//2、为可限量也可不限量的资源提供一致的代码，尤其是提供在运行时配置的可能性
struct semaphore_LCB
{
    struct  rsc_node node;
    uint32_t lamps_limit;   //信号灯数量上限，0表示不限数量，即总不会阻塞
    uint32_t lamp_counter;  //可用信号灯数量。
    uint32_t lamp_used;     //该信号灯被请求的总次数，次数溢出时将环绕到0
    struct  event_script *semp_sync;    //等候信号的事件队列，优先级排队
};

struct mutex_LCB
{
    struct  rsc_node node;
    bool_t  enable;
    ufast_t  prio_bak;          //优先级继承中备份原优先级
    struct  event_script *mutex_sync;    //等候信号的事件队列，优先级排队
    struct  event_script *owner;        //持有互斥量的事件
};

//用于信号量和互斥量共享内存池
union lock_MCB
{
    struct semaphore_LCB sem;
    struct mutex_LCB  mut;
};

bool_t module_init_lock1(void);
bool_t module_init_lock2(void);
struct semaphore_LCB *semp_create(uint32_t lamps_limit,uint32_t init_lamp,char *name);
void __semp_create_knl( struct semaphore_LCB *semp,
                       uint32_t lamps_limit,uint32_t init_lamp,char *name);
void semp_post(struct semaphore_LCB *semp);
bool_t semp_pend(struct semaphore_LCB *semp,uint32_t timeout);
bool_t semp_delete(struct semaphore_LCB *semp);
uint32_t semp_query_used(struct semaphore_LCB *semp);
uint32_t semp_query_capacital(struct semaphore_LCB *semp);
uint32_t semp_query_free(struct semaphore_LCB *semp);
struct mutex_LCB *mutex_create(bool_t init_lamp,char *name);
void __mutex_createe_knl( struct mutex_LCB *mutex,bool_t init_lamp,char *name);
void mutex_post(struct mutex_LCB *mutex);
bool_t mutex_pend(struct mutex_LCB *mutex,uint32_t timeout);
bool_t mutex_delete(struct mutex_LCB *mutex);
bool_t mutex_query_used(struct mutex_LCB *mutex);

#ifdef __cplusplus
}
#endif

#endif //__semp_h__

