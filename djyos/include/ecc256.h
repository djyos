//----------------------------------------------------
//Copyright (C), 2004-2009,  网络.
//版权所有 (C), 2004-2009,   网络.
//所属模块:flash文件系统
//作者：网络
//版本：V1.0.0
//文件描述:flash文件系统中ECC校验部分
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef _ECC256_H_
#define _ECC256_H_

#ifdef __cplusplus
extern "C" {
#endif

uint32_t ecc_count_bits(uint8_t x);
void ecc_make_256(const uint8_t *data,uint8_t *ecc);
uint32_t ecc_corect_256(uint8_t *data, const uint8_t *old_ecc);

#ifdef __cplusplus
}
#endif

#endif // _ECC256_H_

