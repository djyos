//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:泛设备管理模块
//作者：lst
//版本：V1.0.0
//文件描述:提供泛设备管理功能
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __driver_H__
#define __driver_H__

#ifdef __cplusplus
extern "C" {
#endif

#define cn_dev_name_limit   255     //设备名长度不能超过255字符
struct  dev_io;
struct  pan_device;
struct  dev_handle;
struct  rsc_node;

//泛设备模块出错代码，本enum常量从enum_drv_no_error开始依序增1.
enum drv_error_code
{
    enum_drv_no_error = cn_drv_no_error,  //没有错误
    enum_drv_handle_error,                //句柄错误
    enum_drv_homonymy,                    //设备重名错误
    enum_drv_dev_del,                   //删除设备失败
};

typedef ptu32_t (*dev_write_func)(struct  dev_handle *dev_hdl,ptu32_t src_buf,
                                 ptu32_t des_buf,ptu32_t len);
typedef ptu32_t (*dev_read_func) (struct  dev_handle *dev_hdl,ptu32_t src_buf,
                                 ptu32_t des_buf,ptu32_t len);
typedef ptu32_t (*dev_ctrl_func) (struct  dev_handle *dev_hdl,uint32_t cmd,
                                ptu32_t data1,ptu32_t data2);
//从设备的输入输出结构,左右手各一个
struct dev_io
{
    //所有6个（左右手各三个，故6个）io函数中,操作系统并不限定函数参数的类型
    //含义,而是由设备性质决定,如果src_buf是结构数组,len可能是结构体数量,如果
    //src_buf是通信缓冲区,则len可能是完整数据包数量.总之,src_buf和len的类型应该
    //由driver作者根据本身的特性决定,这里的定义只是推荐性质的.
    //微言:src_buf和des_buf被定义成ptu32_t而不是指针,是从移植兼容性出发考虑的.
    //指针是不确定长度的数据类型,这会带来移植方面的困难.有许多系统,尤其是16位系统,
    //指针的宽度是16位的,甚至有24位的(keilC51中就有可能).当应用程序要把参数当作
    //整数使用时,将面临不知道参数的长度的问题.反过来,除64位系统外,所有的系统指针
    //长度都小于32位,做类型转换没有问题.
    //返回值:由设备特性决定.
    dev_write_func io_write;
    dev_read_func  io_read;
    dev_ctrl_func  io_ctrl;
};

//设备端口分为左端口和右端口,用户使用设备时,须严格区分使用左端口还是右端口,
//一般要象管理事件类型那样,项目中要专门管理.
struct pan_device
{
    struct  rsc_node  node;
    struct  dev_io left_hand;
    struct  dev_io right_hand;
    struct semaphore_LCB *left_semp;  //左手信号量
    struct semaphore_LCB *right_semp; //右手信号量
    ptu32_t private_tag;              //本设备特有的数据结构,
};

enum interface_type
{
    enum_iam_left,
    enum_iam_right,
};
struct dev_handle
{
    struct  pan_device *dev_interfase;          //用于设备操作。
    struct dev_handle *previous,*next;          //用于设备清理操作
    struct event_script *owner;                 //用于设备清理操作
    enum interface_type iam;      //左手接口cn_iam_left，右手接口cn_iam_right
};

bool_t module_driver_init(void);
struct  pan_device *dev_add_root_device(char             *name,
                                        struct semaphore_LCB *right_semp,
                                        struct semaphore_LCB *left_semp,
                                        dev_write_func   right_write ,
                                        dev_read_func    right_read,
                                        dev_ctrl_func    right_ctrl ,
                                        dev_write_func   left_write ,
                                        dev_read_func    left_read ,
                                        dev_ctrl_func    left_ctrl );
struct  pan_device *dev_add_device(struct  pan_device    *parent_device,
                                   char                  *name,
                                   struct semaphore_LCB      *right_semp,
                                   struct semaphore_LCB      *left_semp,
                                   dev_write_func        right_write ,
                                   dev_read_func         right_read,
                                   dev_ctrl_func         right_ctrl ,
                                   dev_write_func        left_write ,
                                   dev_read_func         left_read ,
                                   dev_ctrl_func         left_ctrl );
bool_t dev_delete_device(struct  pan_device * device);
struct  dev_handle *dev_open_left(char *name,uint32_t timeout);
struct  dev_handle *dev_open_left_scion(struct  dev_handle *ancestor,
                                      char *scion_name, uint32_t timeout);
bool_t dev_open_left_again(struct  dev_handle *handle,uint32_t timeout);
struct  dev_handle *dev_open_right(char *name,uint32_t timeout);
struct  dev_handle *dev_open_right_scion(struct  dev_handle *ancestor,
                                      char *scion_name, uint32_t timeout);
bool_t dev_open_right_again(struct  dev_handle *handle,uint32_t timeout);
struct  dev_handle * dev_close_left(struct  dev_handle *handle);
struct  dev_handle * dev_close_right(struct  dev_handle *handle);
ptu32_t dev_read(struct  dev_handle *handle,ptu32_t src_buf,
                ptu32_t des_buf,ptu32_t len);
ptu32_t dev_write(struct  dev_handle *handle,ptu32_t src_buf,
                ptu32_t des_buf,ptu32_t len);
ptu32_t dev_ctrl(struct  dev_handle *handle,uint32_t cmd,
                    ptu32_t data1,ptu32_t data2);
void dev_cleanup(struct  event_script *event);

#ifdef __cplusplus
}
#endif
#endif //__driver_H___
