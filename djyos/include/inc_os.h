//----------------------------------------------------
//Copyright (C), 2005-2009,  lst.
//版权所有 (C), 2005-2009,   lst.
//所属模块:包含文件
//作者：lst
//版本：V1.0.0
//文件描述:包含了所有操作系统模块所用到的头文件
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "config.h"
#include "kernel.h"
#include "critical.h"
#include "sys_init.h"
#include "int.h"
#include "djyos.h"
#include "cpu.h"
#include "rsc.h"
#include "ring.h"
#include "line.h"
#include "lock.h"
#include "endian.h"
#include "memory.h"
#include "memb.h"
#include "mems.h"
#include "driver.h"
#include "flashfile.h"
#include "timer.h"
#include "key.h"
#include "wdt.h"
#include "file.h"
