//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:内核模块
//作者：lst
//版本：V1.0.0
//文件描述: 内核使用的一些声明
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __KERNEL_H_
#define __KERNEL_H_

#ifdef __cplusplus
extern "C" {
#endif

#define cn_limit_uint64     0xffffffffffffffff
#define cn_limit_uint32     0xffffffff
#define cn_limit_uint16     0xffff
#define cn_limit_uint8      0xff
#define cn_limit_sint64     0x7fffffffffffffff
#define cn_limit_sint32     0x7fffffff
#define cn_limit_sint16     0x7fff
#define cn_limit_sint8      0x7f

#define cn_min_uint64       0
#define cn_min_uint32       0
#define cn_min_uint16       0
#define cn_min_uint8        0
#define cn_min_sint64       0x8000000000000000
#define cn_min_sint32       0x80000000
#define cn_min_sint16       0x8000
#define cn_min_sint8        0x80

#ifndef false
  #define false 0
#endif
#ifndef true
  #define true 1
#endif

typedef ufast_t         bool_t;

//出错enum常量的定义，所有的出错常量被定义为enum，enum的成员是一个整型常量，该
//常量的高8位用来区别出错模块名称，如下表定义:
#define cn_knl_no_error     (1<<24)         //系统调度起始错误号
#define cn_fs_no_error      (2<<24)         //文件系统起始错误号
#define cn_mem_no_error     (3<<24)         //内存起始错误号
#define cn_drv_no_error     (4<<24)         //pan driver模块起始错误号
#define cn_wdt_no_error     (5<<24)         //wdt起始错误号
#ifdef __cplusplus
}
#endif

#endif // __KERNEL_H_
