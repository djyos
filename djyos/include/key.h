//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:键盘
//作者：lst
//版本：V1.0.0
//文件描述: 提供键盘设备管理功能
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef _KEY_H_
#define _KEY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define cn_key_buf_len      16      //按键缓冲器可保存的按键个数
#define cn_break_code       0xf0    //键盘断码
#define cn_key_mode_sch     0       //阻塞模式，即应用程序读按键时，如果缓冲区
                                    //中没有按键就阻塞
#define cn_key_mode_nsch    1       //非阻塞模式，缓冲区中没有按键直接返回

//按键操作常数
struct key_script
{
    uint8_t key_value[3];       //键值,每个键由3部分组成
    uint32_t time;              //发生时间,ticks数
};

//函数定义
bool_t module_init_keyboard(void);
void key_scan(struct event_script *my_event);
ptu32_t key_right_write(struct  dev_handle *key_rdev,ptu32_t pt_key,
                                ptu32_t  res1,ptu32_t res2);
ptu32_t key_left_read(struct  dev_handle *key_ldev,ptu32_t pt_key,
                                ptu32_t  mode,ptu32_t res2);
ptu32_t key_ctrl(struct  dev_handle *key_devio,uint32_t cmd,uint32_t res1,uint32_t res2);
bool_t key_read(struct key_script *key,uint32_t timeout);
bool_t key_read_direct(struct key_script *key);
bool_t key_hard_init(void);
uint8_t key_scan_hard(uint16_t *key);

#ifdef __cplusplus
}
#endif

#endif // _KEY_H_

