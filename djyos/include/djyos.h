//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:核心模块
//作者：lst
//版本：V1.0.0
//文件描述:事件类型、事件管理以及多事件调度相关的代码全在这里了。
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __yos_H__
#define __yos_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
//结构类型声明,来自其他文件定义的结构
struct dev_handle;
struct semaphore_LCB;
//结构类型声明,本文件定义的结构
struct  thread_vm;
struct  event_script;
struct  event_type;

#define cn_invalid_event_id (-1)
#define cn_invalid_evtt_id  (0xffff)    //非法事件类型号
#define cn_event_id_limit   32767       //最大事件id号
#define cn_evtt_id_limit    32767       //最大事件类型id号
#define cn_timeout_forever  0xffffffff  //无限延时

#define cn_sync_success     0       //同步条件达成
#define cn_sync_timeout     1       //同步条件未达成，超时返回，或timeout设为0
#define cn_sync_error       (-1)    //出错

//出错信息定义
enum knl_error_code
{
    enum_knl_no_error = cn_knl_no_error,  //没有错误
    enum_knl_etcb_error,              //事件类型控制块错误
    enum_knl_no_free_ecb,             //没有空闲事件控制块
    enum_knl_no_free_etcb,            //没有空闲事件类型控制块
    enum_knl_cant_sched,            //企图在禁止调度时执行可能引起事件调度的操作
    enum_knl_clear_mark,              //mark型事件处理时未调用y_clear_mark函数
    enum_knl_invalid_prio,            //非法优先级
    enum_knl_vpu_over,                //事件类型的虚拟机超过限制数
    enum_knl_evtt_homonymy,           //事件类型重名
};

//事件优先级名称定义
#define cn_prio_critical    (100)
#define cn_prio_real        (130)
#define cn_prio_RRS         (200)
#define cn_prio_wdt         (1)
#define cn_prio_sys_service (250)
#define cn_prio_invalid     (255)   //非法优先级

//container_of宏的定义,这是来自linux的代码,但被修改成更广泛支持的c90格式
//container_of - 根据结构成员指针计算容器结构地址
//ptr:      结构成员指针
//type:     容器结构的类型
//member:   成员名称
#define container_of(container,ptr, type, member) do{         \
        const void *__mptr = (void*)(ptr);	\
        container = (type *)( (char *)__mptr - offsetof(type,member) );}while(0)

struct process_vm       //进程虚拟机
{
};

//有mmu的机器上,地址分配:0~1G操作系统,1~2G共享虚拟机,2~4G虚拟机的其他空间.
struct  thread_vm        //虚拟机数据结构
{
    //虚拟机栈指针,在虚拟机被抢占时保存sp,运行时并不动态跟踪sp变化
    uint32_t    *stack;
    uint32_t    *stack_top;     //虚拟机的栈顶指针
    struct thread_vm *next;     //用于把evtt的所有空闲虚拟机连成一个单向开口链表
                                //该链表由evtt的my_free_vm指针索引
    uint32_t    stack_size;      //栈深度
    struct process_vm *host_vm;  //宿主进程虚拟机，在si和dlsp模式中为NULL
};

//事件进入运行态的原因.
//事件应该是内闭的,djyos不提供查询事件当前状态的功能.如果提供查询功能,那么,执行
//查询的必定是当前运行的服务,其自身的当前状态必定是运行态,查询自身是多此一举.
//而查询别人的状态,有干涉内政之嫌.
//使用方法:当事件进入运行态时,根据进入前状态设置事件的last_status.
//查询函数根据last_status值判断事件从何种状态进入运行态.
//因优先级低而等待的不予考虑,例如,某事件延时结束后因为优先级低而等待一定时间后
//才运行,查询结果为"闹铃响"
struct event_status_bit
{
    uint16_t event_ready:1;           //就绪态
    uint16_t event_delay:1;           //闹钟同步
    uint16_t wait_overtime:1;         //超时等待
    uint16_t event_sync:1;            //事件同步
    uint16_t evtt_pop_sync:1;         //事件类型弹出同步
    uint16_t evtt_done_sync:1;        //事件类型完成同步
    uint16_t wait_memory:1;           //从系统堆分配内存同步
    uint16_t wait_semp:1;             //信号量同步
    uint16_t wait_mutex :1;           //互斥量同步
    uint16_t wait_asyn_signal:1;      //异步信号同步
};

union event_status
{
    uint16_t all;
    struct event_status_bit bit;
};

struct event_script
{
    //事件链指针,用于构成下列链表
    //pg_event_free: 特殊，参见文档
    //pg_event_ready:特殊链表，参见文档
    //pg_event_delay:双向循环
    struct event_script *next,*previous;
    //多功能链表指针，用于连接以下链表:
    //1、各同步队列，比如事件同步，事件类型弹出同步等
    //2、就绪队列中同优先级首事件链表，双向循环，用以实现轮询和y_event_ready中
    //   的O(1)算法。
    struct event_script *multi_next,*multi_previous;      //条件同步队列
    struct  thread_vm  *vm;     //处理本事件的线程虚拟机指针,
    struct  event_script *sync; //同步事件队列,完成本事件后执行链表中的事件
    struct event_script **sync_head;    //记住自己在哪一个同步队列中，以便超时
                                        //返回时从该同步队列取出事件
    uint32_t    start_time;     //事件发生时间，绝对时钟
    uint32_t    consumed_time;  //事件消耗的时间
    uint32_t    delay_start;    //设定闹铃时间
    uint32_t    delay_end;      //闹铃响时间
    uint32_t    error_no;       //本事件执行产生的最后一个错误号
    uint32_t    parameter0;     //事件参数0,具体访问类型由程序员约定
    uint32_t    parameter1;     //事件参数1,具体访问类型由程序员约定
    uint32_t    wait_mem_size;  //等待分配的内存数量.
    union event_status last_status;    //最后状态,用于查询事件进入运行态前的状态
    union event_status event_status;   //当前状态,本变量由操作系统内部使用,
    ufast_t     prio;       //事件优先级，0~255,事件一经弹出，就不能再改变优先级

    uint16_t    evtt_id;                //事件类型id，0~32767
    uint16_t    sync_counter;           //同步计数
    //事件id范围:0~32767(cn_event_id_limit)
    uint16_t    event_id;   //事件序列编号,等同于事件在事件块数组中的偏移位置
    uint16_t    repeats;        //该事件开始执行前同类型事件积累发生的次数
    struct  dev_handle  *held_device;//本事件持有的设备指针，事件返回时强行释放
    uint32_t    local_memory;       //本事件处理过程中申请的局部内存块数
                                     //不收回将导致内存泄漏.
};

//事件属性定义表
struct evtt_property
{
    uint16_t    mark:1;     //0=表示独立事件,事件队列中可能同时存在多条该类事件
                            //1=表示mark型事件,事等待队列中只允许一条
    uint16_t    overlay:1;  //本属性只在 mark=1时有效
                            //0=保持原始事件参数
                            //1=事件再次发生时用新的事件参数替换原来的事件.只替
                            //  换参数,不替换优先级等.
    uint16_t    registered:1; //0=该事件类型还没有注册,系统将拒绝pop该类型事件
                            //1=该事件类型已经注册,可以pop该类型事件
    uint16_t    in_use:1;   //0=所有队列中都没有该类型的事件
                            //1=队列中(包括等待中)至少有一条该类型的事件
};

struct event_type
{
    //同一类型的事件可以有多条正在执行或等待调度,但这些事件有相同的属性.
    struct evtt_property    property;
    //空闲线程指针,分配线程给事件时，优先从这里分配
    struct  thread_vm  *my_free_vm;
    char evtt_name[32]; //事件类型允许没有名字，但只要有名字，就不允许同名
                        //如果一个类型不希望别的模块弹出事件，可以不用名字。
                        //如模块间需要交叉弹出事件，用名字访问。
    //优先级小于0x80为紧急优先级,它影响虚拟机的构建.类型优先级在初始化时设定,
    ufast_t     default_prio;       //事件类型优先级.不同于事件优先级,1~255,0非法.
    uint16_t    pop_sum,done_sum;   //已弹出事件总数，已完成事件总数
    uint16_t    repeats;    //未开始处理的该类型事件发生的次数
    uint16_t    vpus_limit;  //本类型事件允许同时建立的线程虚拟机个数
    uint16_t    vpus;       //本类型事件已经拥有的线程虚拟机个数
    void (*thread_routine)(struct event_script *my_event);//函数指针,可能是死循环.
    uint32_t stack_size;              //thread_routine所需的栈大小

    //mark_unclear指针指向mark型尚未被clear的事件。
    //在pop一个mark型事件时,若队列中已经有该类型的、未被clear的事件，则只是增加
    //该类型的repeats数，若又是参数替换型事件类型，替换事件参数。这种情况下，
    //y_event_pop函数的参数prio是无效的。
    //若该mark型事件没有未clear的事件，但仍有事件在处理，则新事件并不加入就绪
    //队列，而是挂在mark_unclear指针下，待正在处理的事件done时把它加入就绪队列。
    //若该mark型事件既无未clear的事件，又没有正在处理的事件，则直接加入就绪队列
    //如果不是mark型事件,不使用本指针
    struct event_script *mark_unclear;
    //这两队列都是以剩余次数排队的双向循环链表
    struct  event_script *done_sync,*pop_sync;//弹出同步和完成同步队列头指针,
};

//就绪队列(优先级队列),始终执行队列头部的事件,若有多个优先级相同,轮流执行
extern struct  event_script  *pg_event_ready;
extern struct  event_script  *pg_event_running;   //当前正在执行的事件
extern uint32_t u32g_ns_of_u32for;
extern uint32_t u32g_ns_of_u16for;
extern uint32_t u32g_ns_of_u8for;

#define djy_event_delay(x) djy_timer_sync(x)

void __djy_set_delay(void);
void djy_delay_10us(volatile uint16_t time);
sint32_t djy_rtstrlen(const char *s,uint32_t over);
uint32_t djy_get_time(void);
uint32_t djy_get_fine_time(void);
uint32_t __djy_isr_tick(ufast_t line);
void djy_error_login(uint32_t error_type,const char *text);
void __djy_cut_ready_event(struct event_script *event);
void djy_set_RRS_slice(uint32_t slices);
uint32_t djy_get_RRS_slice(void);
uint32_t djy_get_last_error(char *text);
uint32_t djy_get_RRS_slice(void);
void __djy_select_event_to_run(void);
void djy_create_process_vm(void);
bool_t __djy_schedule(void);
void __djy_schedule_asyn_signal(void);
uint16_t djy_evtt_regist(bool_t mark,bool_t overlay,
                       ufast_t default_prio,
                       uint16_t vpus_limit,
                       void (*thread_routine)(struct event_script *),
                       uint32_t stack_size,
                       char *evtt_name);
uint16_t djy_get_evtt_id(char *evtt_name);
bool_t djy_evtt_unregist(uint16_t evtt_id);
void __djy_init_sys(void);
bool_t djy_query_sch(void);
void __djy_event_ready(struct  event_script *event_ready);
void __djy_resume_delay(struct  event_script *delay_event);
void __djy_addto_delay(uint32_t u32l_mS);
void djy_clear_mark(void);
uint32_t djy_timer_sync(uint32_t u32l_mS);
uint32_t djy_event_sync(uint16_t event_id,uint32_t timeout);
uint32_t djy_evtt_done_sync(uint16_t evtt_id,uint16_t done_times,uint32_t timeout);
uint32_t djy_evtt_pop_sync(uint16_t evtt_id,uint16_t pop_times,uint32_t timeout);
uint16_t djy_event_pop(   uint16_t evtt_id,
                        uint32_t parameter0,
                        uint32_t parameter1,
                        ufast_t prio);
void __djy_init_tick(void);
void __djy_start_os(void);
void djy_event_done(void);
union event_status djy_wakeup_from(void);
struct  event_script *__djy_lookup_id(uint16_t id);
void __djy_vm_engine(void (*thread_routine)(struct event_script *my_event));
void __djy_service(struct event_script *my_event);
void djy_api_start(uint32_t api_no);
void NULL_func(void);

#ifdef __cplusplus
}
#endif
#endif //__yos_H___
