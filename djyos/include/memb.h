//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:内存池管理模块
//作者：lst
//版本：V1.0.0
//文件描述:提供固定块分配策略
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __memb_H__
#define __memb_H__

#ifdef __cplusplus
extern "C" {
#endif

struct mem_cell_pool
{
    struct rsc_node memb_node;    //资源结点
    void  *continue_pool;    //连续内存池首地址，使用它可以增加实时性。
    void  *free_list;        //未分配块链表,单向,NULL结尾
    struct semaphore_LCB memb_semp;
    ptu32_t pool_offset;    //连续池中的偏移量(当前地址)
    uint32_t cell_size;      //块大小,初始化时将按系统对其尺寸调整。
};

bool_t module_init_memb(void);
struct mem_cell_pool *mb_create(void *pool_original,uint32_t capacital,
                                uint32_t cell_size, char *name);
void *mb_malloc(struct mem_cell_pool *pool,uint32_t timeout);
void mb_free(struct mem_cell_pool *pool,void *block);

#ifdef __cplusplus
}
#endif

#endif //__memb_H__

