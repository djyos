//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 资源管理
//作者：lst
//版本：V1.0.0
//文件描述: 创建、添加、移动、删除、搜索资源结点的服务
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __rsc_h__
#define __rsc_h__

#ifdef __cplusplus
extern "C" {
#endif
#define cn_rsc_name_limit   255

struct rsc_node
{
    struct rsc_node *next,*previous,*parent,*child;
    uint32_t  node_size;    //包含node的数据结构的尺寸，用于调试
    char *name;       //资源名,当用于文件系统为文件名或目录名,用于设备是设备名
                      //用于gui则是窗口名.
};

bool_t module_init_rsc1(void);
bool_t module_init_rsc2(void);
struct  rsc_node * rsc_add_tree_root(struct  rsc_node *node,uint32_t size,char *name);
struct  rsc_node * rsc_insert_node(struct  rsc_node *node,struct  rsc_node *new_node,
                                uint32_t size,char *name);
struct  rsc_node * rsc_add_node(struct  rsc_node *node,struct  rsc_node *new_node,
                                uint32_t size,char *name);
struct  rsc_node * rsc_add_son(struct  rsc_node *parent_node,
                               struct  rsc_node *new_node,
                               uint32_t size,char *name);
struct  rsc_node * rsc_add_eldest_son(struct  rsc_node *parent_node,
                                    struct  rsc_node *new_node,
                                    uint32_t size,char *name);
bool_t rsc_del_node(struct  rsc_node *node);
bool_t rsc_moveto_tree(struct rsc_node *parent,struct  rsc_node *node);
bool_t rsc_moveto_least(struct  rsc_node *node);
bool_t rsc_movto_eldest(struct  rsc_node *node);
bool_t rsc_movto_lesser(struct  rsc_node *elder,struct  rsc_node *node);
bool_t rsc_movto_elder(struct  rsc_node *lesser,struct  rsc_node *node);
bool_t rsc_round_back(struct  rsc_node *parent);
bool_t rsc_round_forward(struct  rsc_node *parent);
struct  rsc_node *rsc_get_tree(struct  rsc_node *scion_node);
struct  rsc_node *rsc_get_root(void);
struct  rsc_node *rsc_get_parent(struct  rsc_node *son_node);
struct  rsc_node *rsc_get_son(struct  rsc_node *parent_node);
struct  rsc_node *rsc_get_next(struct  rsc_node *elder_node);
struct  rsc_node *rsc_get_twig(struct  rsc_node *parent_node);
uint32_t rsc_get_class(struct  rsc_node *node);
struct  rsc_node *rsc_trave_scion(struct  rsc_node *parent_node,
                                 struct  rsc_node *current_node);
struct  rsc_node *rsc_search_sibling(struct  rsc_node *layer,char *name);
struct  rsc_node *rsc_search_son(struct  rsc_node *parent,char *name);
struct  rsc_node *rsc_search_scion(struct  rsc_node *parent_node,char *name);
struct  rsc_node *rsc_search(struct  rsc_node *parent_node,char *path);

#ifdef __cplusplus
}
#endif

#endif //__rsc_h__

