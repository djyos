//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:线性缓冲区
//作者：lst
//版本：V1.0.0
//文件描述: 提供线性缓冲区服务
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __line_h__
#define __line_h__

#ifdef __cplusplus
extern "C" {
#endif

struct line_buf
{
    uint32_t    current;      //缓冲区中的字节数/当前指针
    uint32_t    limit;        //缓冲区最大长度,元素个数.
    uint8_t     *buf;         //缓冲区指针,用户自己保证所开辟的缓冲区是否与设定
                              //参数一致,djyos不做检查.
};

void line_init(struct line_buf *line, uint8_t *buf, uint32_t len);
uint32_t line_capacity(struct line_buf *line);
uint32_t line_skip_tail(struct line_buf *line,uint32_t len);
uint32_t line_write(struct line_buf *line,uint8_t *buffer,uint32_t len);
uint32_t line_read(struct line_buf *line,uint8_t *buffer);
uint8_t *line_get_buf(struct line_buf *line);
uint32_t    line_check(struct line_buf *line);
bool_t   line_if_empty(struct line_buf *line);
bool_t   line_if_full(struct line_buf *line);
void    line_flush(struct line_buf *line);
uint32_t line_search_ch(struct line_buf *line, char c);
uint32_t line_search_str(struct line_buf *line, char *string,uint32_t str_len);

#ifdef __cplusplus
}
#endif

#endif //__line_h__

