//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:调度器
//作者：lst
//版本：V1.0.0
//文件描述:调度器中与CPU直接相关的代码。
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------


#ifndef __CPU_H__
#define __CPU_H__

#ifdef __cplusplus
extern "C" {
#endif

struct hard_reg_mem_ctrl   //地址在0x48000000~0x48000030
{
    uint32_t BWSCON;    //0x48000000
    uint32_t BANKCON0;  //0x48000004
    uint32_t BANKCON1;  //0x48000008
    uint32_t BANKCON2;  //0x4800000C
    uint32_t BANKCON3;  //0x48000010
    uint32_t BANKCON4;  //0x48000014
    uint32_t BANKCON5;  //0x48000018
    uint32_t BANKCON6;  //0x4800001C
    uint32_t BANKCON7;  //0x48000020
    uint32_t REFRESH;   //0x48000024
    uint32_t BANKSIZE;  //0x48000028
    uint32_t MRSRB6;    //0x4800002C
    uint32_t MRSRB7;    //0x48000030
};

struct  thread_vm *__create_thread(struct  event_type *evtt);
void *__asm_reset_thread(void (*thread_routine)(struct event_script *),
                           struct  thread_vm  *vm);
void __asm_reset_switch(void (*thread_routine)(struct event_script *my_event),
                           struct  thread_vm *new_vm,struct  thread_vm *old_vm);
void __asm_turnto_context(struct  thread_vm  *new_vm);
void __asm_switch_context(struct  thread_vm *new_vm,struct  thread_vm *old_vm);
void __asm_switch_context_int(struct  thread_vm *new_vm,struct  thread_vm *old_vm);

#ifdef __cplusplus
}
#endif

#endif /*__CPU_H__*/

