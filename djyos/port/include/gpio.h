//----------------------------------------------------
//Copyright (C), 2005-2009,  lst.
//版权所有 (C), 2005-2009,   lst.
//所属模块:IO初始化
//作者：lst
//版本：V1.0.0
//文件描述:IO初始化
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __gpio_h_
#define __gpio_h_

#ifdef __cplusplus
extern "C" {
#endif

#define cn_bus_width    16

struct hard_reg_gpio         //地址在0x56000000
{
    uint32_t GPACON;     //0x56000000
    uint32_t GPADAT;     //0x56000004
    uint32_t rev1;      //0x56000008
    uint32_t rev2;      //0x5600000c
    uint32_t GPBCON;     //0x56000010
    uint32_t GPBDAT;     //0x56000014
    uint32_t GPBUP ;     //0x56000018
    uint32_t rev4;      //0x5600001c
    uint32_t GPCCON;     //0x56000020
    uint32_t GPCDAT;     //0x56000024
    uint32_t GPCUP ;     //0x56000028
    uint32_t rev5;      //0x5600002c
    uint32_t GPDCON;     //0x56000030
    uint32_t GPDDAT;     //0x56000034
    uint32_t GPDUP ;     //0x56000038
    uint32_t rev6;      //0x5600003c
    uint32_t GPECON;     //0x56000040
    uint32_t GPEDAT;     //0x56000044
    uint32_t GPEUP ;     //0x56000048
    uint32_t rev7;      //0x5600001c
    uint32_t GPFCON;     //0x56000050
    uint32_t GPFDAT;     //0x56000054
    uint32_t GPFUP ;     //0x56000058
    uint32_t rev8;      //0x5600005c
    uint32_t GPGCON;     //0x56000060
    uint32_t GPGDAT;     //0x56000064
    uint32_t GPGUP ;     //0x56000068
    uint32_t rev9;      //0x5600006c
    uint32_t GPHCON;     //0x56000070
    uint32_t GPHDAT;     //0x56000074
    uint32_t GPHUP ;     //0x56000078
    uint32_t rev10;      //0x5600007c
};

void gpio_init(void);

extern struct hard_reg_gpio volatile * const pg_gpio_reg;
#ifdef __cplusplus
}
#endif

#endif /*__gpio_h_*/
