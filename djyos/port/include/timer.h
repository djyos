//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 硬件定时器
//作者：lst
//版本：V1.0.0
//文件描述: 硬件定时器操作
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __timer_hard_h__
#define __timer_hard_h__

#ifdef __cplusplus
extern "C" {
#endif

struct hard_reg_timer
{
    uint32_t TCFG0;     //0x51000000
    uint32_t TCFG1;     //0x51000004
    uint32_t TCON;      //0x51000008
    uint32_t TCNTB0;    //0x5100000C
    uint32_t TCMPB0;    //0x51000010
    uint32_t TCNTO0;    //0x51000014
    uint32_t TCNTB1;    //0x51000018
    uint32_t TCMPB1;    //0x5100001C
    uint32_t TCNTO1;    //0x51000020
    uint32_t TCNTB2;    //0x51000024
    uint32_t TCMPB2;    //0x51000028
    uint32_t TCNTO2;    //0x5100002C
    uint32_t TCNTB3;    //0x51000030
    uint32_t TCMPB3;    //0x51000034
    uint32_t TCNTO3;    //0x51000038
    uint32_t TCNTB4;    //0x5100003C
    uint32_t TCNTO4;    //0x51000040
};

extern struct hard_reg_timer volatile * const pg_timer_reg;

void timer_set_clk_source(ufast_t timer,ufast_t value);
void timer_set_precale(ufast_t group,uint16_t value);
void timer_set_counter(ufast_t timer,uint16_t value);
void timer_set_compare(ufast_t timer,uint16_t value);
void timer_set_type(ufast_t timer,ufast_t type);
void timer_reload(ufast_t timer);
void timer_start(ufast_t timer);
void timer_stop(ufast_t timer);
uint16_t timer_read(ufast_t timer);

#ifdef __cplusplus
}
#endif

#endif //__timer_hard_h__
