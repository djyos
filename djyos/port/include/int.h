//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:中断模块
//作者：lst
//版本：V1.0.0
//文件描述: 与中断相关的代码，包含异步信号与实时中断
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#ifndef __int_h__
#define __int_h__

#ifdef __cplusplus
extern "C" {
#endif

struct hard_reg_int
{
    uint32_t SRCPND   ;    //0x4a000000 有中断产生就置1
    uint32_t INTMOD   ;    //0x4a000004 Interrupt mode control
    uint32_t INTMSK   ;    //0x4a000008 Interrupt mask control
    uint32_t PRIORITY ;    //0x4a00000c IRQ priority control
    uint32_t INTPND   ;    //0x4a000010 SRCPND=1的位，若允许则置1
    uint32_t INTOFFSET;    //0x4a000014 Interruot request source offset
    uint32_t SUBSRCPND;    //0x4a000018 Sub source pending
    uint32_t INTSUBMSK;    //0x4a00001c Interrupt sub mask
};

#define cn_noirq    0x80
#define cn_nofiq    0x40
#define cn_noint    0xc0

#define cn_irq_line_eint0     (0)
#define cn_irq_line_eint1     (1)
#define cn_irq_line_eint2     (2)
#define cn_irq_line_eint3     (3)
#define cn_irq_line_eint4_7   (4)
#define cn_irq_line_eint8_23  (5)
#define cn_irq_line_resv6     (6)
#define cn_irq_line_bat_flt   (7)
#define cn_irq_line_tick      (8)
#define cn_irq_line_wdt       (9)
#define cn_irq_line_timer0    (10)
#define cn_irq_line_timer1    (11)
#define cn_irq_line_timer2    (12)
#define cn_irq_line_timer3    (13)
#define cn_irq_line_timer4    (14)
#define cn_irq_line_uart2     (15)
#define cn_irq_line_lcd       (16)
#define cn_irq_line_dma0      (17)
#define cn_irq_line_dma1      (18)
#define cn_irq_line_dma2      (19)
#define cn_irq_line_dma3      (20)
#define cn_irq_line_sdi       (21)
#define cn_irq_line_spi0      (22)
#define cn_irq_line_uart1     (23)
#define cn_irq_line_resv24    (24)
#define cn_irq_line_usbd      (25)
#define cn_irq_line_usbh      (26)
#define cn_irq_line_iic       (27)
#define cn_irq_line_uart0     (28)
#define cn_irq_line_spi1      (29)
#define cn_irq_line_rtc       (30)
#define cn_irq_line_adc       (31)

#define cn_int_msk_all_line   (0xffffffff)

#define  cn_asyn_signal     (0)
#define  cn_real            (1)

//表示各中断线状态的位图占ucpu_t类型的字数
#define cn_int_bits_words   ((cn_int_num+cn_cpu_bits-1)/cn_cpu_bits)

//中断线数据结构，每中断一个
struct int_line
{
    uint32_t (*ISR)(ufast_t line);
    struct  event_script *sync_event;       //正在等待本中断发生的事件
    ucpu_t en_counter;     //禁止次数计数,等于0时表示允许中断
    ucpu_t int_type;         //1=实时中断,0=异步信号
    //嵌套优先级和子优先级功能的实现强烈依赖于特定系统的中断控制器。即使相同内核
    //的CPU,其中断控制器也可能不同
    //嵌套优先级是指:当允许嵌套时，嵌套优先级高的中断可以抢占正在服务的嵌套优先
    //级低的中断
    //子优先级是指:当嵌套优先级相同的多个中断同时挂起时，先服务子优先级高的中断
    //44B0X只支持嵌套优先级，而cortex-m3同时支持嵌套和子优先级
    sint16_t  nest_prio;                //嵌套优先级，数越小，优先级越高
    sint16_t  sub_prio;                 //子优先级，数越小，优先级越高
    uint16_t my_evtt_id;
};

//中断总控数据结构.
struct int_master_ctrl
{
    //中断线属性位图，0=异步信号，1=实时中断,数组的位数刚好可以容纳中断数量,与
    //中断线数据结构的int_type成员含义相同。
    ucpu_t  property_bit_map[cn_int_bits_words];
    ucpu_t nest_asyn_signal;   //中断嵌套深度,主程序=0,第一次进入中断=1,依次递加
    ucpu_t nest_real;   //中断嵌套深度,主程序=0,第一次进入中断=1,依次递加
    //中断线使能位图,1=使能,0=禁止,反映相应的中断线的控制状态,
    //与总开关/异步信号开关的状态无关.
    ucpu_t  enable_bit_map[cn_int_bits_words];
    bool_t  en_trunk;           //1=总中断使能,  0=总中断禁止
    bool_t  en_asyn_signal;         //1=异步信号使能,0=异步信号禁止
    ucpu_t en_trunk_counter;   //全局中断禁止计数,=0表示允许全局中断
    ucpu_t en_asyn_signal_counter; //异步信号禁止计数,=0表示允许异步信号
};

extern struct int_master_ctrl  tg_int_global;          //定义并初始化总中断控制结构
extern struct hard_reg_int volatile * const pg_int_reg;

void __int_contact_real(void);
void __int_cut_real(void);
void __int_contact_asyn_signal(void);
void __int_cut_asyn_signal(void);
void __int_contact_trunk(void);
void __int_cut_trunk(void);
void __int_contact_line(ufast_t ufl_line);
void __int_cut_line(ufast_t ufl_line);
void __int_cut_all_line(void);
void int_disable_line(ufast_t ufl_line);
void int_enable_line(ufast_t ufl_line);
void int_save_trunk(void);
void int_restore_trunk(void);
void int_reset_trunk(void);
void int_save_real(void);
void int_restore_real(void);
void int_reset_real(void);
void int_save_asyn_signal(void);
void __int_reset_asyn_signal(void);
void int_restore_asyn_signal(void);
bool_t int_check_asyn_signal(void);
bool_t int_check_real(void);
bool_t int_check_trunk(void);
void int_save_line(ufast_t ufl_line);
void int_restore_line(ufast_t ufl_line);
void int_reset_line(ufast_t ufl_line);
void int_enable_nest_asyn_signal(void);
void int_disable_nest_asyn_signal(void);
void int_enable_nest_real(void);
void int_disable_nest_real(void);
bool_t int_check_line(ufast_t ufl_line);
bool_t int_query_line(ufast_t ufl_line);
void int_echo_line(ufast_t ufl_line);
bool_t int_tap_line(ufast_t ufl_line);
void __int_echo_all_line(void);
void __int_engine_real(ufast_t ufl_line);
void __int_engine_asyn_signal(ufast_t ufl_line);
void __int_engine_all(ufast_t ufl_line);
bool_t int_setto_asyn_signal(ufast_t ufl_line);
bool_t int_setto_real(ufast_t ufl_line);
void int_isr_connect(ufast_t ufl_line, uint32_t (*isr)(ufast_t));
void int_evtt_connect(ufast_t ufl_line,uint16_t my_evtt_id);
void int_isr_disconnect(ufast_t ufl_line);
void int_evtt_disconnect(ufast_t ufl_line);
bool_t int_asyn_signal_sync(ufast_t ufl_line);
void __int_affirm(void);
void __int_init(void);

#ifdef __cplusplus
}
#endif

#endif //__int_h__
