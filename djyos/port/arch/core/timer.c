//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 硬件定时器
//作者：lst
//版本：V1.1.0
//文件描述: 用于44b0硬件定时器操作
//其他说明:
//修订历史:
//2. 日期: 2009-04-24
//   作者: lst
//   新版本号: V1.1.0
//   修改说明: 原djyos.c中定时功能与硬件相关的部分转移到这里。
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 移植自44b0版本
//------------------------------------------------------
#include "inc_os.h"
#include "gpio.h"
struct hard_reg_timer volatile * const pg_timer_reg
                        = (struct hard_reg_timer *)0x51000000;

extern uint32_t  u32g_os_ticks;             //操作系统运行ticks数
volatile uint32_t    u32g_delay_10uS = 40;

//2410的定时器功能简述:
//1、减计数方式工作
//2、每个定时器可选5个时钟源，分别为主频的1/2分频~1/16分频，或EXTCLK。
//3、有两个8位预分频器，01共享一个，234共享一个。
//4、每个定时器都有比较寄存器，用于产生PWM波形。
//5、每个定时器可以设定为单次运行和重复运行，可以手动启动和停止。

void timer_init(void)
{
    //DMA:No select(All Interrupt),
    pg_timer_reg->TCFG1 = 0;
}

//----设置定时器时钟源----------------------------------------------------------
//功能: 设置各定时器时钟源.
//参数: timer,定时器号
//      value,定时器输入时钟源,timer和value的对应表如下:
//      value:  0           1           2            3          4
//      timer0: 1/2分频     1/4分频     1/8分频      1/16分频   EXTCLK0
//      timer1: 1/2分频     1/4分频     1/8分频      1/16分频   EXTCLK0
//      timer2: 1/2分频     1/4分频     1/8分频      1/16分频   EXTCLK1
//      timer3: 1/2分频     1/4分频     1/8分频      1/16分频   EXTCLK1
//      timer4: 1/2分频     1/4分频     1/8分频      1/16分频   EXTCLK1
//返回: 无
//-----------------------------------------------------------------------------
void timer_set_clk_source(ufast_t timer,ufast_t value)
{
    pg_timer_reg->TCFG1 &= ~(0xf << (timer<<2));
    pg_timer_reg->TCFG1 |= value << (timer<<2);
}

//----设置定时器预分频数-------------------------------------------------------
//功能: 设置各定时器预分频数.
//参数: group,定时器组号,01为1组,234为1组
//      value,定时器预分频数,0~255对应1~256分频
//返回: 无
//-----------------------------------------------------------------------------
void timer_set_precale(ufast_t group,uint16_t value)
{
    pg_timer_reg->TCFG0 &= ~(0xff << (group<<3));
    pg_timer_reg->TCFG0 |= value << (group<<3);
}

//----设置定时器计数值----------------------------------------------------------
//功能: 设置各定时器计数值.定时器的溢出中断时间为:value*(预分频+1)/时钟源分频数
//参数: timer,定时器号
//      value,计数值
//返回: 无
//-----------------------------------------------------------------------------
void timer_set_counter(ufast_t timer,uint16_t value)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCNTB0 = value;
            break;
        case 1:
            pg_timer_reg->TCNTB1 = value;
            break;
        case 2:
            pg_timer_reg->TCNTB2 = value;
            break;
        case 3:
            pg_timer_reg->TCNTB3 = value;
            break;
        case 4:
            pg_timer_reg->TCNTB4 = value;
            break;
        default:break;
    }
}

//----设置定时器比较值----------------------------------------------------------
//功能: 用于pwm占空比设计
//参数: timer,定时器号
//      value,比较值
//返回: 无
//-----------------------------------------------------------------------------
void timer_set_compare(ufast_t timer,uint16_t value)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCMPB0 = value;
            break;
        case 1:
            pg_timer_reg->TCMPB1 = value;
            break;
        case 2:
            pg_timer_reg->TCMPB2 = value;
            break;
        case 3:
            pg_timer_reg->TCMPB3 = value;
            break;
        default:break;
    }
}

//----设置定时器工作方式-------------------------------------------------------
//功能: 设定定时器是连续工作还是单次工作
//参数: timer,定时器号
//      type,0=单次工作,1=自动加载连续工作
//返回: 无
//-----------------------------------------------------------------------------
void timer_set_type(ufast_t timer,ufast_t type)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCON &= ~(1<<3);
            pg_timer_reg->TCON |= type<<3;
            break;
        case 1:
            pg_timer_reg->TCON &= ~(1<<11);
            pg_timer_reg->TCON |= type<<11;
            break;
        case 2:
            pg_timer_reg->TCON &= ~(1<<15);
            pg_timer_reg->TCON |= type<<15;
            break;
        case 3:
            pg_timer_reg->TCON &= ~(1<<19);
            pg_timer_reg->TCON |= type<<19;
            break;
        case 4:
            pg_timer_reg->TCON &= ~(1<<22);
            pg_timer_reg->TCON |= type<<22;
            break;
        default:break;
    }
}

//----重载定时器计数和比较值---------------------------------------------------
//功能: 手动重新加载定时器的计数和比较寄存器值
//参数: timer,定时器号
//返回: 无
//-----------------------------------------------------------------------------
void timer_reload(ufast_t timer)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCON |= 1<<1;
            pg_timer_reg->TCON &= ~(1<<1);
            break;
        case 1:
            pg_timer_reg->TCON |= 1<<9;
            pg_timer_reg->TCON &= ~(1<<9);
            break;
        case 2:
            pg_timer_reg->TCON |= 1<<13;
            pg_timer_reg->TCON &= ~(1<<13);
            break;
        case 3:
            pg_timer_reg->TCON |= 1<<17;
            pg_timer_reg->TCON &= ~(1<<17);
            break;
        case 4:
            pg_timer_reg->TCON |= 1<<21;
            pg_timer_reg->TCON &= ~(1<<21);
            break;
        default:break;
    }
}

//----启动定时器--------------------------------------------------------
//功能: 启动定时器
//参数: timer,定时器号
//返回: 无
//-----------------------------------------------------------------------------
void timer_start(ufast_t timer)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCON |=1;
            break;
        case 1:
            pg_timer_reg->TCON |=1<<8;
            break;
        case 2:
            pg_timer_reg->TCON |=1<<12;
            break;
        case 3:
            pg_timer_reg->TCON |=1<<16;
            break;
        case 4:
            pg_timer_reg->TCON |=1<<20;
            break;
        default:break;
    }
}

//----停止定时器--------------------------------------------------------
//功能: 停止定时器
//参数: timer,定时器号
//返回: 无
//-----------------------------------------------------------------------------
void timer_stop(ufast_t timer)
{
    switch (timer)
    {
        case 0:
            pg_timer_reg->TCON &=~1;
            break;
        case 1:
            pg_timer_reg->TCON &=~(1<<8);
            break;
        case 2:
            pg_timer_reg->TCON &=~(1<<12);
            break;
        case 3:
            pg_timer_reg->TCON &=~(1<<16);
            break;
        case 4:
            pg_timer_reg->TCON &=~(1<<20);
            break;
        default:break;
    }
}

//----读定时器当前值--------------------------------------------------------
//功能: 读出定时器的当前计数值
//参数: timer,定时器号
//返回: 当前计数值
//-----------------------------------------------------------------------------
uint16_t timer_read(ufast_t timer)
{
    switch (timer)
    {
        case 0:
            return pg_timer_reg->TCNTO0;
            break;
        case 1:
            return pg_timer_reg->TCNTO1;
            break;
        case 2:
            return pg_timer_reg->TCNTO2;
            break;
        case 3:
            return pg_timer_reg->TCNTO3;
            break;
        case 4:
            return pg_timer_reg->TCNTO4;
            break;
        default:break;
    }
    return 0;
}

//----设置指令延时常数---------------------------------------------------------
//功能: 设置指令延时常数,使y_delay_10us函数的延时常数=10uS，不管用何种编译器和
//      编译优化选项
//参数：无
//返回: 无
//备注: 本函数移植关键
//-----------------------------------------------------------------------------
void __djy_set_delay(void)
{
    uint32_t counter,u32_fors=64000,i;
    uint16_t u16_fors=64000;
    uint8_t  u8_fors=250;
    uint32_t u32_u8 = 128;
    volatile uint32_t u32loops;
    volatile uint16_t u16loops;
    volatile uint8_t  u8loops;

    timer_set_clk_source(3,0);          //pclk预分频数的1/2分频
    timer_set_precale(1,7);            //时钟输入频率pclk的8分频
    timer_set_type(3,1);                //自动加载连续工作
    //2000uS，减计数,扩充64位是为了减小舍入误差
    timer_set_counter(3,(uint16_t)(2*cn_timer_clk/16000));
    timer_reload(3);
    timer_start(3);
    do  //测量32位变量循环时间(nS)
    {
        int_echo_line(cn_irq_line_timer3);      //清中断标志
        while( ! int_query_line(cn_irq_line_timer3));//直到发生中断，重新计数
        for(u32loops=u32_fors;u32loops>0;u32loops--); //循环u32_fors次
        counter = timer_read(3);                  //读取循环u32_fors次所需时间
        u32_fors >>= 1;                         //u32_fors减半
    }while(int_query_line(cn_irq_line_timer3));//中断已发生，说明u32_fors次循环
                               //大于2mS，u32_fors减半，再次循环，直到中断不发生
    counter = 2*cn_timer_clk/16000 - counter;    //取实际脉冲数。
    u32g_ns_of_u32for = counter*2000/(2*cn_timer_clk/16000)*1000 /(u32_fors<<1);
    do  //测量16位变量循环时间(nS)
    {
        int_echo_line(cn_irq_line_timer3);      //清中断标志
        while( ! int_query_line(cn_irq_line_timer3));//直到发生中断，重新计数
        for(u16loops=u16_fors;u16loops>0;u16loops--); //循环u16_fors次
        counter = timer_read(3);                  //读取循环u32_fors次所需时间
        u16_fors >>= 1;                           //u16_fors减半
    }while(int_query_line(cn_irq_line_timer3));//中断已发生，说明u32_fors次循环
                              //大于2mS，u32_fors减半，再次循环，直到中断不发生
    counter = 2*cn_timer_clk/16000 - counter;    //取实际脉冲数。
    u32g_ns_of_u16for = counter*2000/(2*cn_timer_clk/16000)*1000 /(u16_fors<<1);

    do  //测量8位变量循环时间(nS)
    {
        int_echo_line(cn_irq_line_timer3);      //清中断标志
        while( ! int_query_line(cn_irq_line_timer3));//直到发生中断，重新计数
        for(i = (uint8_t)u32_u8; i > 0; i--)
            for(u8loops=u8_fors;u8loops>0;u8loops--); //循环u8_fors次
        counter = timer_read(3);                      //读取循环k次所需时间
        u32_u8 >>= 1;                           //u32_u8减半
    }while(int_query_line(cn_irq_line_timer3));//中断已发生，说明u32_fors次循环
                              //大于2mS，u32_fors减半，再次循环，直到中断不发生
    counter = 2*cn_timer_clk/16000 - counter;    //取实际脉冲数。
    u32g_ns_of_u8for = counter*2000/(2*cn_timer_clk/16000)*1000
                                /((u8_fors+1) * (u32_u8<<1));

    u32g_delay_10uS = 10000/u32g_ns_of_u32for;
}

//----初始化tick---------------------------------------------------------------
//功能: 初始化定时器,并连接tick中断函数,启动定时器.
//参数: 无
//返回: 无
//备注: 本函数是移植关键函数.
//-----------------------------------------------------------------------------
void __djy_init_tick(void)
{
    //连接timer3作为tick中断
    int_isr_connect(cn_irq_line_timer3,__djy_isr_tick);
    int_setto_asyn_signal(cn_irq_line_timer3);          //tick中断被设为异步信号
    //以下设置定时器参数，需与port_config.h中cn_tick_ms、cn_tick_hz和
    //cn_fine_us、cn_fine_hz的定义一致
    timer_set_clk_source(3,0);          //pclk预分频数的1/2分频
    timer_set_precale(1,cn_timer_clk/cn_fine_hz/2 -1);
    //以上把定时器输入时钟的频率设为cn_fine_hz
    timer_set_counter(3,(uint16_t)(cn_tick_ms*1000/cn_fine_us));
    timer_set_type(3,1);                //设置tick定时器连续工作
    timer_reload(3);                    //重载定时值
    timer_start(3);                     //启动定时器
    int_restore_line(cn_irq_line_timer3);//启动tick中断
}

//----读取当前ticks-------------------------------------------------------------
//功能：读取操作系统时钟
//      u32g_os_ticks 为32位无符号变量，ARM是32位机，可以直接读取，非32位系统中
//      读取 u32g_os_ticks 需要超过1个周期,因此访问SysTimer时需要关中断。
//参数：无
//返回：当前时钟
//-----------------------------------------------------------------------------
uint32_t djy_get_time(void)
{
    uint32_t time;
#if (32 > cn_cpu_bits)
    //若处理器字长不是32位,需要多个周期才能读取os_ticks,该过程不能被时钟中断打断.
    int_save_line(cn_irq_line_timer3);
#endif

    time = u32g_os_ticks;

#if (32 > cn_cpu_bits)
    //若处理器字长不是32位,需要多个周期才能读取os_ticks,该过程不能被时钟中断打断.
    int_restore_line(cn_irq_line_timer3);
#endif
    return time;
}

//----读取精密时间-----------------------------------------------------------
//功能：读取操作系统精密时钟,时间单位在配置文件中设置,依硬件不同而不同,一般设为
//      tick的百分之一或者千分之一,44b0x中设为千分之一.结果是从上次ticks改变到
//      现在的时间间隔/
//参数：无
//返回：时间值
//备注: 本函数是移植关键函数.
//-----------------------------------------------------------------------------
uint32_t djy_get_fine_time(void)
{
    return 1000-pg_timer_reg->TCNTO3;
}

