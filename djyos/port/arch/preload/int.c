//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:中断模块
//作者：lst
//版本：V1.0.0
//文件描述: s3c2410与中断相关的代码，包含异步信号与实时中断
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 从44b0版本移植而成
//------------------------------------------------------
#include "inc_os.h"

extern void (*user_irq)(ufast_t ufl_line);      //在脚本中定义
extern void (*user_fiq)(ufast_t ufl_line);      //在脚本中定义

struct hard_reg_int volatile * const pg_int_reg
                        = (struct hard_reg_int *)0x4a000000;

struct int_line tg_int_table[cn_int_num];       //定义中断线控制数据结构

struct int_master_ctrl  tg_int_global;

//cpsr的IF位清0，允许全部ARM7内核中断
#define int_enable_arm_int()               \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "bic %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_noint)              \
                :"cc"                       \
            );                              \
        }while(0)

//cpsr的IF位置1，禁止ARM7内核全部中断
#define int_disable_arm_int()              \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "ORR %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_noint)              \
                :"cc"                       \
            );                              \
        }while(0)

//cpsr的I位置1，禁止ARM7内核irq中断
#define __int_disable_irq()                   \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "ORR %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_noirq)              \
                :"cc"                       \
            );                              \
        }while(0)

//cpsr的I位清0，允许ARM7内核irq中断
#define __int_enable_irq()                    \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "bic %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_noirq)              \
                :"cc"                       \
            );                              \
        }while(0)

//cpsr的F位置1，禁止ARM7内核fiq中断
#define int_disable_fiq()                   \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "ORR %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_nofiq)              \
                :"cc"                       \
            );                              \
        }while(0)

//cpsr的F位清0，允许ARM7内核irq中断
#define int_enable_fiq()                    \
        do{                                 \
            register ucpu_t xr0;            \
            asm volatile                    \
            (                               \
                "MRS %0, CPSR       \n\t"   \
                "bic %0,%0,%1       \n\t"   \
                "MSR CPSR_cxsf, %0  \n\t"   \
                :"=r"(xr0)                  \
                :"i"(cn_nofiq)              \
                :"cc"                       \
            );                              \
        }while(0)

//----接通异步信号开关---------------------------------------------------------
//功能：接通异步信号开关,如果总开关接通且中断线开关接通,该中断将被允许
//      1.当有独立的硬件开关时,把该开关接通即可
//      2.如果没有独立硬件开关,则接通所有被允许的异步信号的线开关.
//      3.44b0x属于第二中情况,把符合条件的中断线在rINTMSK相应位清0.
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_contact_asyn_signal(void)
{
    //INTMSK中异步信号且中断线被使能的位被清0
    pg_int_reg->INTMSK &= ~((~tg_int_global.property_bit_map[0])
                         & tg_int_global.enable_bit_map[0]);
}

//----断开异步信号开关---------------------------------------------------------
//功能：断开异步信号开关,所有的异步信号将被禁止
//      1.当有独立的硬件开关时,把该开关断开即可
//      2.如果没有独立硬件开关,则断开所有异步信号的线开关.
//      3.44b0x属于第二中情况,把所有异步信号线在rINTMSK相应位置1.
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_cut_asyn_signal(void)
{
    register ucpu_t msk;
    msk = (~tg_int_global.property_bit_map[0]) & cn_int_msk_all_line;
    pg_int_reg->INTMSK |= msk;
}

//----接通总中断开关-----------------------------------------------------------
//功能：接通总中断开关,所有cpu都会有一个总开关,直接操作该开关即可.
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_contact_trunk(void)
{
    __int_enable_irq();
}

//----断开总中断开关---------------------------------------------------------
//功能：断开总中断开关,所有cpu都会有一个总开关,直接操作该开关即可.
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_cut_trunk(void)
{
    __int_disable_irq();
}

//----接通单个中断线开关-------------------------------------------------------
//功能：接通单个中断线开关,该中断是否允许还要看后面的开关状态
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_contact_line(ufast_t ufl_line)
{
    //如果硬件提供独立的总中断开关和异步信号开关,则不需要做以下判断,直接
    //允许该中断线就可以了.
    if(tg_int_table[ufl_line].int_type == cn_asyn_signal)
    {//如果该中断线属于异步信号,且异步信号开关允许,允许该中断线
        if(tg_int_global.en_asyn_signal == true)
            pg_int_reg->INTMSK &= ~(1<<ufl_line);
    }else if(tg_int_global.en_trunk == true)
    //如果该中断属于实时中断,且中断总开关允许,允许该中断线
        pg_int_reg->INTMSK &= ~(1<<ufl_line);
}

//----断开单个中断线开关-------------------------------------------------------
//功能：断开单个中断线开关，无论总中断和异步信号开关状态如何，该中断线被禁止
//参数：无
//返回：无
//备注：这是内部函数,只允许模块内调用,移植关键
//-----------------------------------------------------------------------------
void __int_cut_line(ufast_t ufl_line)
{
    register ucpu_t ucl_msk = 1<<ufl_line;
    pg_int_reg->INTMSK |=ucl_msk;
}

//----保存总中断状态并禁止总中断--------------------------------------------
//功能：本函数是int_restore_trunk()的姊妹函数，调用本函数使禁止次数增1，调用一
//      次int_restore_trunk使禁止次数减1。
//      若当前次数为0，增加为1并禁止总中断，不为0时简单地增1
//参数：无
//返回：无
//------------------------------------------------------------------------------
void int_save_trunk(void)
{
    if(tg_int_global.en_trunk_counter != cn_limit_ucpu)//达上限后再加会回绕到0
        tg_int_global.en_trunk_counter++;
    //原算法是从0->1的过程中才进入，但如果在en_trunk_counter != 0的状态下因故障
    //使中断关闭，将使用户后续调用的int_save_trunk起不到作用
    __int_cut_trunk();
    tg_int_global.en_trunk =false;
    return;
}

//----恢复保存的总中断状态---------------------------------------------------
//功能：本函数是int_save_trunk()的姊妹函数，调用本函数使禁止次数减1，调用
//      一次int_save_trunk使禁止次数增1，
//      当次数减至0时激活总中断,否则简单减1
//参数：无
//返回：无
//------------------------------------------------------------------------------
void int_restore_trunk(void)
{
    if(tg_int_global.en_trunk_counter != 0)
        tg_int_global.en_trunk_counter--;
    if(tg_int_global.en_trunk_counter == 0)
    {
        tg_int_global.en_trunk = true;
        __int_contact_trunk();
        if(tg_int_global.en_asyn_signal == true)
        {
            if(pg_event_running != pg_event_ready)
                __djy_schedule();
        }
    }else
    {
        __int_cut_trunk();
    }
    return;
}

//----查看总中断是否允许-----------------------------------------------------
//功能：
//参数：无
//返回：允许返回true,禁止返回false
//-----------------------------------------------------------------------------
bool_t int_check_trunk(void)
{
    if(tg_int_global.en_trunk == true)
        return true;
    else
        return false;
}

//----保存当前状态并禁止异步信号------------------------------------------------
//功能：本函数是int_restore_asyn_signal()的姊妹函数，调用本函数使禁止次数增加，
//      调用一次int_restore_asyn_signal()使禁止次数减少。
//      若当前次数为0，增加为1并禁止异步信号，不为0时简单地增1
//参数：无
//返回：无
//------------------------------------------------------------------------------
void int_save_asyn_signal(void)
{
    //达上限后再加会回绕到0
    if(tg_int_global.en_asyn_signal_counter != cn_limit_ucpu)
        tg_int_global.en_asyn_signal_counter++;
    //原算法是从0->1的过程中才进入，但如果在en_asyn_signal_counter != 0的状态下
    //因故障使中断关闭，将使用户后续调用的en_asyn_signal_counter起不到作用
    __int_cut_asyn_signal();
    tg_int_global.en_asyn_signal =false;
    return;
}

//----复位异步信号开关------------------------------------------------
//功能：把异步信号开关恢复到初始状态，即en_asyn_signal_counter=1的状态，初始化
//      中断系统后，还要做大量的模块初始化工作才能启动多事件调度，在启动多事件
//      调度前调用本函数复位异步信号状态，可以防止模块初始化代码的bug意外修改
//      了异步信号使能状态。
//参数：无
//返回：无
//-----------------------------------------------------------------------------
void __int_reset_asyn_signal(void)
{
    __int_cut_asyn_signal();
    tg_int_global.en_asyn_signal =false;
    tg_int_global.en_asyn_signal_counter =1;
    return;
}

//----恢复保存的异步信号状态----------------------------------------------------
//功能：本函数是int_save_asyn_signal()的姊妹函数，调用本函数使禁止次数减少，调用
//      一次int_save_asyn_signal()是禁止次数增加。
//      当次数减至0时激活异步信号,否则简单减1
//参数：无
//返回：无
//------------------------------------------------------------------------------
void int_restore_asyn_signal(void)
{
    if(tg_int_global.en_asyn_signal_counter != 0)
        tg_int_global.en_asyn_signal_counter--;
    if(tg_int_global.en_asyn_signal_counter==0)
    {
        tg_int_global.en_asyn_signal = true;   //异步信号设为使能
        __int_contact_asyn_signal();
        if(tg_int_global.en_trunk == true)
        {
            if(pg_event_running != pg_event_ready)
                __djy_schedule();
        }
    }else
    {
        __int_cut_asyn_signal();    //防止counter>0期间意外(bug)打开
    }
    return;
}

//----查看异步信号是否允许-----------------------------------------------------
//功能：
//参数：无
//返回：允许返回true,禁止返回false
//-----------------------------------------------------------------------------
bool_t int_check_asyn_signal(void)
{
    return tg_int_global.en_asyn_signal;
}

//----保存当前状态并禁止中断线--------------------------------------------------
//功能：本函数是int_restore_line（）的姊妹函数，调用本函数使禁止次数增加，调
//      用一次int_restore_line是禁止次数减少。
//      若当前次数为0，增加为1并禁止中断线，不为0时简单地增1
//参数：ufl_line
//返回：无
//------------------------------------------------------------------------------
void int_save_line(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    if(tg_int_table[ufl_line].en_counter!=cn_limit_ucpu)//达上限后再加会回绕到0
        tg_int_table[ufl_line].en_counter++;
    //原算法是从0->1的过程中才进入，但如果在en_counter != 0的状态下
    //因故障使中断关闭，将使用户后续调用的en_counter起不到作用
    __int_cut_line(ufl_line);
    tg_int_global.enable_bit_map[ufl_line/cn_cpu_bits]
                &= ~(1<<(ufl_line % cn_cpu_bits));
}

//----恢复保存的中断线状态-----------------------------------------------------
//功能：本函数是int_save_line（）的姊妹函数，调用本函数使禁止次数减少，调
//      用一次int_save_line是禁止次数增加。
//      当次数减至0时激活中断线,否则简单减1
//参数：ufl_line
//返回：无
//-----------------------------------------------------------------------------
void int_restore_line(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    if(tg_int_table[ufl_line].en_counter != 0)
        tg_int_table[ufl_line].en_counter--;
    if(tg_int_table[ufl_line].en_counter==0)
    {
        tg_int_global.enable_bit_map[ufl_line/cn_cpu_bits]
                |= 1<<(ufl_line % cn_cpu_bits);
        __int_contact_line(ufl_line);
    }else
    {
        __int_cut_line(ufl_line);
    }
}

//----直接禁止中断线-----------------------------------------------------------
//功能：本函数是int_enable_line（）的姊妹函数，调用本函数使中断线的使能计数器
//      置位，并掐断中断线
//参数：ufl_line
//返回：无
//------------------------------------------------------------------------------
void int_disable_line(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    tg_int_table[ufl_line].en_counter = 1;
    __int_cut_line(ufl_line);
    tg_int_global.enable_bit_map[ufl_line/cn_cpu_bits]
                &= ~(1<<(ufl_line % cn_cpu_bits));
}

//----直接允许中断线-----------------------------------------------------------
//功能：本函数是int_disable_line（）的姊妹函数，调用本函数使中断线的使能计数器
//      归零，并接通中断线
//参数：ufl_line
//返回：无
//------------------------------------------------------------------------------
void int_enable_line(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    tg_int_table[ufl_line].en_counter = 0;
    __int_contact_line(ufl_line);
    tg_int_global.enable_bit_map[ufl_line/cn_cpu_bits]
                |= 1<<(ufl_line % cn_cpu_bits);
}

//----查询中断线使能状态-------------------------------------------------------
//功能：查询中断线是否允许
//参数：ufl_line，欲查询的中断线
//返回：true = 使能，false = 禁止。
//-----------------------------------------------------------------------------
bool_t int_check_line(ufast_t ufl_line)
{
    if(tg_int_table[ufl_line].en_counter == 0)
        return true;
    else
        return false;
}

//----查询中断线请求状态-------------------------------------------------------
//功能：查询并清除相应中断线状态，可用于查询式中断程序
//参数：ufl_line，欲查询的中断线
//返回：若中断挂起，返回true，否则返回false
//备注: 与硬件结构相关,有些结构可能不提供这个功能,慎用!
//      本函数是移植关键函数
//-----------------------------------------------------------------------------
bool_t int_query_line(ufast_t ufl_line)
{
    ucpu_t  ucl_msk;
    if(ufl_line>=cn_int_num)
        return false;
    ucl_msk=1<<ufl_line;
    if(pg_int_reg->SRCPND & ucl_msk)
    {
        pg_int_reg->SRCPND = ucl_msk;
        pg_int_reg->INTPND = ucl_msk;
        return true;
    }else
        return false;
}

//----允许异步信号嵌套----------------------------------------------------------
//功能: 在ISR函数里调用,以允许中断嵌套,实际上就是允许处于使能状态的异步信号.
//参数: 无
//返回: 无
//注:本函数移植关键
//------------------------------------------------------------------------------
void int_enable_nest_asyn_signal(void)
{
    if(tg_int_global.nest_asyn_signal != 0)
        __int_contact_asyn_signal();
}

//----禁止异步信号嵌套----------------------------------------------------------
//功能: 在ISR函数里调用,以禁止中断嵌套,实际上就是禁止所有异步信号.中断响应
//		后，系统默认禁止异步信号嵌套，只有在人为打开异步信号嵌套后需要重新关闭，
//		才需要调用本函数
//参数: 无
//返回: 无
//注:本函数移植关键
//------------------------------------------------------------------------------
void int_disable_nest_asyn_signal(void)
{
    if(tg_int_global.nest_asyn_signal != 0)
        __int_cut_asyn_signal();
}

//----允许实时中断嵌套----------------------------------------------------------
//功能: 在中断处理函数里调用,以允许中断嵌套,实际上就是允许处于使能状态的实时中断.
//参数: 无
//返回: 无
//注:本函数移植关键
//------------------------------------------------------------------------------
void int_enable_nest_real(void)
{
    if(tg_int_global.nest_real != 0)
    {
        int_save_asyn_signal();
        __int_contact_trunk();
    }
}

//----禁止实时中断嵌套----------------------------------------------------------
//功能: 在中断处理函数里调用,以禁止中断嵌套,实际上就是禁止所有实时中断.中断响应
//		后，系统默认禁止实时中断嵌套，只有在人为打开实时中断嵌套后需要重新关闭，
//		才需要调用本函数
//参数: 无
//返回: 无
//注:本函数移植关键
//------------------------------------------------------------------------------
void int_disable_nest_real(void)
{
    if(tg_int_global.nest_real != 0)
    {
        __int_cut_trunk();
        int_restore_asyn_signal();
    }
}

//----应答中断，清除相应中断线的中断挂起状态-----------------------------------
//功能：硬件应该有相应的功能，提供清除中断挂起的操作，清除前，不能响应同一中断线
//      的后续中断，清除后，才可以响应后续中断。本函数与该中断线被设置为实时中断
//      还是异步信号无关
//参数：ufast ufl_line，指定应答的中断线号
//返回：无
//备注：有些体系中断响应时硬件应答，本函数为空函数。
//      本函数是移植关键函数
//-----------------------------------------------------------------------------
void int_echo_line(ufast_t ufl_line)
{
    pg_int_reg->SRCPND = 1<<ufl_line;
    pg_int_reg->INTPND = 1<<ufl_line;
}

//----激发中断-----------------------------------------------------------------
//功能: 触发一个中断.如果中断本已悬挂,本函数无影响.本函数与该中断线被设置为实时
//      中断还是异步信号无关
//参数：ufast ufl_line，欲触发的中断线号
//返回：如果相应的中断线硬件不提供用软件触发中断功能,返回 false,否则返回 true
//备注: 本函数实现依赖于硬件,有些硬件系统不支持此功能.
//      本函数是移植关键函数
//-----------------------------------------------------------------------------
bool_t int_tap_line(ufast_t ufl_line)
{
    return false;    //2410不支持软件触发硬件中断的能力.
}

//----应答全部中断，清除全部中断线的中断挂起状态-------------------------------
//功能：硬件应该有相应的功能，提供清除中断挂起的操作，清除前，不能响应后续中断，
//      清除后，可以响应后续中断
//参数：ufast ufl_line，指定应答的中断线号
//返回：无
//备注：有些体系中断响应时硬件应答，本函数为空函数。
//      本函数是移植关键函数
//-----------------------------------------------------------------------------
void __int_echo_all_line(void)
{
    pg_int_reg->SRCPND = cn_int_msk_all_line;
    pg_int_reg->INTPND = cn_int_msk_all_line;;
}

//----实时中断引擎-------------------------------------------------------------
//功能：响应实时中断，根据中断号调用用户ISR
//参数：ufast ufl_line，响应的中断线号
//返回：无
//      本函数是移植关键函数
//-----------------------------------------------------------------------------
void __int_engine_real(ufast_t ufl_line)
{
    tg_int_global.nest_real++;

    tg_int_global.en_trunk= false;
    tg_int_global.en_trunk_counter = 1;
//    __int_cut_trunk();            //移植提示:若硬件没有关闭总中断，需增加这句
    int_echo_line(ufl_line);      //中断应答
    tg_int_table[ufl_line].ISR(ufl_line);  //调用用户中断函数

    tg_int_global.en_trunk = true;
    tg_int_global.en_trunk_counter = 0;
    //    __int_contact_trunk();    //移植提示:若硬件没有关闭总中断，需增加这句

    tg_int_global.nest_real--;
}

//----异步事件中断引擎---------------------------------------------------------
//功能：响应异步信号，根据中断号调用用户ISR，随后弹出中断线控制块的my_evtt_id
//      成员指定的事件类型，最后在返回前查看是否需要做上下文切换，如需要则切换
//      之。
//参数：ufast ufl_line，响应的中断线号
//返回：无
//备注: 本函数是移植关键函数
//-----------------------------------------------------------------------------
void __int_engine_asyn_signal(ufast_t ufl_line)
{
    struct event_script *event;
    tg_int_global.nest_asyn_signal++;
    //以下几句移植很关键，请用户根据自己的硬件仔细设计，需要使CPU进入这样的状态：
    //异步信号被禁止而总开关打开的状态，类似于依序调用 int_save_asyn_signal和
    //__int_contact_trunk两个函数，
    __int_cut_asyn_signal();        //移植提示:若硬件关闭了异步信号，则无需这句
    int_echo_line(ufl_line);      //中断应答
    tg_int_global.en_asyn_signal = false;
    tg_int_global.en_asyn_signal_counter = 1;
    __int_contact_trunk();          //移植提示:若硬件没关闭总中断，则无需这句

    event = tg_int_table[ufl_line].sync_event;
    if(event != NULL)   //看同步指针中有没有事件(注：不是同步队列)
    {
        event->last_status.all = event->event_status.all;
        event->event_status.bit.wait_asyn_signal = 0;
        __djy_event_ready(event);   //把该事件放到ready队列
        tg_int_table[ufl_line].sync_event = NULL;   //解除同步
    }
    //调用用户中断函数,此时嵌套中断是禁止的，用户如果需要允许嵌套，可以在
    //vec_func函数中打开异步信号来达到。
    tg_int_table[ufl_line].ISR(ufl_line);
    djy_event_pop(tg_int_table[ufl_line].my_evtt_id,ufl_line,0,0);
    if(tg_int_global.nest_asyn_signal == 1)
    {//已经是最后一级中断嵌套了,看看是否要调度
        if(pg_event_ready != pg_event_running)
            __djy_schedule_asyn_signal();       //执行中断内调度
    }
    //以下几句移植很关键，请用户根据自己的硬件仔细设计:
    //既要调用int_restore_asyn_signal使en_asyn_signal_counter归0，又不能使
    //异步信号真的打开，而是要恢复到CPU响应中断后的状态，由中断返回指令打开。
    __int_cut_trunk();              //移植提示:若硬件没关闭总中断，则无需这句
    tg_int_global.en_asyn_signal = true;
    tg_int_global.en_asyn_signal_counter = 0;
    __int_contact_asyn_signal();    //移植提示:若硬件没关闭异步信号，则无需这句

    tg_int_global.nest_asyn_signal--;
}

void __int_engine_all(ufast_t ufl_line)
{
    if(tg_int_table[ufl_line].int_type == cn_real)
        __int_engine_real(ufl_line);           //是实时中断
    else
        __int_engine_asyn_signal(ufl_line);        //是异步信号
}

//----把指定中断线设置为异步信号--------－－－---------------------------------
//功能：把指定中断线设置为异步信号,若中断正在响应,则当前中断返回后生效
//参数：ufast ufl_line，指定被设置的中断线号
//返回：true=成功，false=失败
//-----------------------------------------------------------------------------
bool_t int_setto_asyn_signal(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return false;
    tg_int_table[ufl_line].int_type = cn_asyn_signal;   //中断线类型

    tg_int_global.property_bit_map[ufl_line/cn_cpu_bits]
            &= ~(1<<(ufl_line % cn_cpu_bits));    //设置位图
    return true;
}

//----把指定中断线设置为实时中断--------－－－---------------------------------
//功能：把指定中断线设置为实时中断,若中断正在响应,则当前中断返回后生效
//参数：ufast ufl_line，指定被设置的中断线号
//返回：true=成功，false=失败
//-----------------------------------------------------------------------------
bool_t int_setto_real(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return false;
    if(tg_int_table[ufl_line].sync_event != NULL)
        return false;     //有线程在等待这个中断，不能设为实时中断
    tg_int_table[ufl_line].int_type = cn_real;    //中断线类型
    tg_int_global.property_bit_map[ufl_line/cn_cpu_bits]
            |= 1<<(ufl_line % cn_cpu_bits);   //设置位图
    return true;
}

//----设定嵌套优先级-----------------------------------------------------------
//功能: 设定指定中断线的嵌套优先级，本函数严重依赖硬件功能。如果硬件不支持，可
//      保持空函数。
//参数：ufast ufl_line，指定被设置的中断线号
//返回：无
//注: 本函数移植关键
//-----------------------------------------------------------------------------
void int_set_nest_prio(ufast_t ufl_line)
{
}

//----设定子优先级-----------------------------------------------------------
//功能: 设定指定中断线的子优先级，本函数严重依赖硬件功能。如果硬件不支持，可
//      保持空函数。
//参数：ufast ufl_line，指定被设置的中断线号
//返回：无
//注: 本函数移植关键
//-----------------------------------------------------------------------------
void int_set_sub_prio(ufast_t ufl_line)
{
}

//----关联中断线与ISR----------------------------------------------------------
//功能：为指定中断线指定中断响应函数，该函数为普通函数，
//参数：ufl_line,需要设置的中断线号
//      isr，中断响应函数，由用户提供，原型：void isr(ufast_t)
//返回：无
//-----------------------------------------------------------------------------
void int_isr_connect(ufast_t ufl_line, uint32_t (*isr)(ufast_t))
{
    if(ufl_line>=cn_int_num)
        return;
    if(isr == NULL)
        tg_int_table[ufl_line].ISR = (uint32_t (*)(ufast_t))NULL_func;
    else
        tg_int_table[ufl_line].ISR = isr;
    return;
}

//----关联中断线与事件类型-----------------------------------------------------
//功能：为指定该中断线指定一个事件类型id，如果是异步信号，则在返回主程序前弹出
//      事件类型为该id的事件，如果是实时中断，则不弹出事件。
//参数：ufl_line,需要设置的中断线号
//      my_evtt_id，事件类型id
//返回：无
//-----------------------------------------------------------------------------
void int_evtt_connect(ufast_t ufl_line,uint16_t my_evtt_id)
{
    if(ufl_line>=cn_int_num)
        return;
    tg_int_table[ufl_line].my_evtt_id = my_evtt_id;
    return;
}

//----断开中断线与中断响应函数的关联-------------------------------------------
//功能：断开指定中断线指定中断响应函数的关联，代之以空函数
//参数：ufl_line,需要设置的中断线号
//返回：无
//-----------------------------------------------------------------------------
void int_isr_disconnect(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    tg_int_table[ufl_line].ISR = (uint32_t (*)(ufast_t))NULL_func; //指向空函数
    return;
}

//----断开中断线与事件类型的关联-----------------------------------------------
//功能：断开指定中断线指定事件类型的关联，代之以cn_invalid_evtt_id
//参数：ufl_line,需要设置的中断线号
//返回：无
//-----------------------------------------------------------------------------
void int_evtt_disconnect(ufast_t ufl_line)
{
    if(ufl_line>=cn_int_num)
        return;
    tg_int_table[ufl_line].my_evtt_id = cn_invalid_evtt_id;
    return;
}

//----设定中断同步-------------------------------------------------------------
//功能: 阻塞正在处理的事件的线程，直到指定的中断线的中断发生、响应并返回后才激活，
//参数: ufl_line,等待的目标中断线
//返回: false = 该中断已经被其他线程等待，直接返回。
//      true = 该中断发生返回。
//备注: 1.中断是一种临界资源，不宜在中断函数中太多的事情，故中断同步的功能比较简
//      单，每条中短线同一时刻只能有一个线程等待，也不允许设置超时等待
//      2.秉承djyos一贯风格，中断同步函数只能把自己置入等待状态，而不能控制别的
//      线程，故函数原型不能是 bool_t int_sync(ufast_t line,uint16_t event_id)
//      3.实时中断设置等待无效，调用本函数时，如果line已经被设置为实时中断，则
//      直接返回false，如果调用本函数后，line不能被设置为实时中断。
//----------------------------------------------------------------------------
bool_t int_asyn_signal_sync(ufast_t ufl_line)
{
    bool_t result;
    if(ufl_line>=cn_int_num)
        return false;
    if( !djy_query_sch())
    {   //禁止调度，不能进入闹钟同步状态。
        djy_error_login(enum_knl_cant_sched,NULL);
        return false;
    }
    int_save_asyn_signal();   //在操作就绪队列期间不能发生中断
    //实时中断不能设置同步，一个中断只接受一个同步事件
    if((tg_int_table[ufl_line].int_type == cn_real)
            || (tg_int_table[ufl_line].sync_event != NULL))
    {
        result = false; //实时中断或已经有同步事件
    }else
    {
        //以下三行从就绪链表中取出running事件
        __djy_cut_ready_event(pg_event_running);
        pg_event_running->next = NULL;
        pg_event_running->previous = NULL;
        pg_event_running->last_status.all
                        = pg_event_running->event_status.all;
        pg_event_running->event_status.bit.wait_asyn_signal = 1;
        tg_int_table[ufl_line].sync_event = pg_event_running;
        result = true;
    }
    int_restore_asyn_signal();
    return result;
}

//特注: 不提供周期性中断同步功能，因为djyos不提供无条件休眠或者挂起的功能，已周
//      期性时钟中断为例，一次时钟中断把线程触发进入ready后，到下次时钟中断到来
//      之前，该线程要么还在ready态(可能被其他线程抢占)，此时不需要再次触发，
//      要么在等待其他触发条件，比如等待内存分配、等待延时到、等待信号量等，此时
//      若被时钟中断触发，则破坏了软件的结构。周期性中断同步可用多次调用单次同步
//      的方法完成，即每次触发后，线程完成了必要的工作以后重新再次调用单次同步
//      函数，这样，程序的每一步都有明确的目标，而不是无目的的休眠或挂起
//----周期性中断同步-----------------------------------------------------------
//功能: 当前正在执行的线程进入等待状态，待被等待的中断发生后再调度,只要发生中断
//      就触发，直到调用int_sync_quash解除同步
//参数: ufl_line,等待的目标中断线
//返回: false = 该中断已经被其他线程等待，直接返回。
//      true = 该中断发生返回。
//备注: 1.中断是一种临界资源，不宜在中断函数中太多的事情，故中断同步的功能比较简
//      单，每条中短线同一时刻只能有一个线程等待，也不允许设置超时等待
//      2.秉承djyos一贯风格，中断同步函数只能把自己置入等待状态，而不能控制别的
//      线程，故函数原型不能是 bool_t int_sync(ufast_t line,uint16_t event_id)
//      3.实时中断设置等待无效，调用本函数时，如果line已经被设置为实时中断，则
//      直接返回false，如果调用本函数后，line不能被设置为实时中断。
//----------------------------------------------------------------------------
//bool_t int_sync_cycle(ufast_t ufl_line)
//{
//    bool_t result;
//    if(ufl_line>=cn_int_num)
//        return false;
//    int_save_asyn_signal();   //在操作就绪队列期间不能发生中断
//    if(tg_int_table[ufl_line].uf_flags.bits.type == cn_real)
//        result = false;
//    else
//    {
//        //以下三行从就绪链表中取出running事件
//        if(pg_event_running == pg_event_ready)
//            pg_event_ready = pg_event_ready->next;
//        pg_event_ready->previous=pg_event_running->previous;
//        pg_event_running->previous->next=pg_event_ready;
//        pg_event_running->next = NULL;
//        pg_event_running->previous = NULL;
//        pg_event_running->last_status.all
//                        = pg_event_running->event_status.all;
//        pg_event_running->event_status.bit.wait_asyn_signal = 1;
//        tg_int_table[line].sync_event = pg_event_running;
//        tg_int_table[line].sync_cycle = true;
//        result = true;
//    }
//    int_restore_asyn_signal();   //在上下文切换期间不能发生中断
//    return result;
//}

//----解除周期性中断同步-------------------------------------------------------
//功能: 解除中断线与当前正在执行的线程的周期性同步状态，这个状态是经调用
//      int_sync_cycle函数进入的。
//参数: ufl_line,等待的目标中断线
//备注: 1.中断是一种临界资源，不宜在中断函数中太多的事情，故中断同步的功能比较简
//      单，每条中短线同一时刻只能有一个线程等待，也不允许设置超时等待
//      2.秉承djyos一贯风格，中断同步函数只能把自己置入等待状态，而不能控制别的
//      线程，故函数原型不能是 bool_t int_sync(ufast_t line,uint16_t event_id)
//      3.实时中断设置等待无效，调用本函数时，如果line已经被设置为实时中断，则
//      直接返回false，如果调用本函数后，line不能被设置为实时中断。
//----------------------------------------------------------------------------
//void int_sync_quash(ufast_t ufl_line)
//{
//    if(ufl_line>=cn_int_num)
//        return false;
//    if(tg_int_table[ufl_line].sync_event != NULL)   //同步指针中有事件
//    {
//        tg_int_table[ufl_line].sync_event = NULL;
//    }
//}

//----初始化中断硬件相关部分---------------------------------------------------
//功能: 如标题
//参数: 无
//返回: 无
//注: 移植关键，与硬件相关，也与软件策略有关
//-----------------------------------------------------------------------------
void __int_init_hard(void)
{
    int_disable_arm_int();     //禁止中断

    pg_int_reg->INTMSK = cn_int_msk_all_line;
    //中断管理器的F位总是禁止,2410的中断管理器有缺陷，除非只允许一
    //个中断配置为fiq，否则fiq检出中断线的过程使得fiq实际比irq慢，故不使用fiq，
    //不管异步信号还是实施中断，均使用irq实现。
    pg_int_reg->INTMOD=0;    //设置所有中断线为IRQ中断
    user_irq=__int_engine_all;
}
//----初始化中断---------------------------------------------------------------
//功能：初始化中断硬件,初始化中断线数据结构
//      2.异步信号保持禁止,它会在虚拟机启动引擎中打开.
//      3.总中断允许，
//      用户初始化过程应该遵守如下规则:
//      1.系统开始时就已经禁止所有异步信号,用户初始化时无须担心异步信号发生.
//      2.初始化过程中如果需要操作总中断/实时中断/异步信号,应该成对使用.禁止使
//        异步信号实际处于允许状态(即异步和总中断开关同时允许).
//      3.可以操作中断线,比如连接、允许、禁止等,但应该成对使用.
//      4.建议使用save/restore函数对,不要使用enable/disable函数对.
//参数：无
//返回：无
//-----------------------------------------------------------------------------
void __int_init(void)
{
    ufast_t ufl;
    __int_init_hard();
    __int_echo_all_line();
    for(ufl=0;ufl<cn_int_num;ufl++)
    {
        __int_cut_line(ufl);
        tg_int_table[ufl].en_counter = 1; //禁止中断,计数为1
        tg_int_table[ufl].int_type = cn_asyn_signal;
        //所有中断函数指针指向空函数
        tg_int_table[ufl].ISR = (uint32_t (*)(ufast_t))NULL_func;
        tg_int_table[ufl].sync_event = NULL;
        tg_int_table[ufl].my_evtt_id = cn_invalid_evtt_id;
    }
    for(ufl=0; ufl < cn_int_bits_words; ufl++)
    {
        //属性位图清零,全部置为异步信号方式
        tg_int_global.property_bit_map[ufl] = 0;
        //中断使能位图清0,全部处于禁止状态
        tg_int_global.enable_bit_map[ufl] = 0;
    }
    tg_int_global.nest_asyn_signal =0;
    tg_int_global.nest_real=0;

    tg_int_global.en_asyn_signal = false;
    tg_int_global.en_asyn_signal_counter = 1;   //异步信号计数
    __int_cut_asyn_signal();
    tg_int_global.en_trunk = true;
    tg_int_global.en_trunk_counter = 0;   //总中断计数
    __int_contact_trunk();                //接通总中断开关
}
