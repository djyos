@----------------------------------------------------
@Copyright (C), 2005-2008,  lst.
@版权所有 (C), 2005-2008,   lst.
@所属模块:  中断模块
@作者：     lst
@版本：      V1.00
@初始版本完成日期：2008-08-30
@文件描述:  中断模块中的汇编部分
@其他说明:  无
@修订历史:
@2. ...
@1. 日期:
@   作者:
@   新版本号：
@   修改说明:
@------------------------------------------------------

@在2410中谨慎使用fiq中断,除非你只有一个中断被设置成fiq,否则,没有任何直接的信息
@会告诉你正在响应哪个中断.FIQ模式中断号寄存器，你只能通过F_ISPR来确认中断源，
@ARM9没有直接返回前导0个数的指令，只能用循环移位的方法。确定中断源所需的时间开
@销很大，故2410中FIQ变得毫无意义，因此djyos在2410版本并不使用FIQ中断.

.equ    NOINT,              0xc0
.equ    NOIRQ,              0x80
.equ    FIQMODE,            0x11
.equ    IRQMODE,            0x12
.equ    MODEMASK,           0x1f
.equ    SVCMODE,            0x13
.equ    INTOFFSET,          0x4a000014

    .extern     tg_int_globe
    .extern     IRQ_stack

    .global     start_int

@中断相关的栈安排：
@1、IRQ_stack放被中断的上下文，顺序:lr，r12-r0，cpsr，共14字
@2、SVC_stack运行中断服务函数，包括用户编写的中断服务函数。
@3、SYS_stack，除非要在中断里切换上下文，否则无关

start_int:

    stmfd   sp!,{r0-r12,lr}         @保护寄存器,以及返回地址
    ldr r0,=INTOFFSET
    ldr r0,[r0]

    mrs     r1,spsr
    stmfd   sp!,{r1}            @保护SPSR_irq，以支持中断嵌套
    msr     cpsr_c,#SVCMODE|NOIRQ @进入SVCMODE,以便允许中断嵌套
    stmfd   sp!,{r0-r3,lr}            @保存lr_svc,

    ldr     r2,=user_irq    @取异步信号地址

    mov     lr,pc           @这两条指令模拟函数调用(4G空间)，调用用户中断处理函数,
    ldr     pc,[r2]         @int_isr_real和int_isr_asyn_signal分别是实时中断和异步
                            @事件处理函数的入口地址,该函数原型为
                            @void int_isr_asyn_signal(ucpu_t intn);intn为中断号,
                            @根据atpcs,intn用r0传递

    ldmfd   sp!,{r0-r3,lr}        @恢复lr_svc,
    msr     cpsr_c,#IRQMODE|NOINT    @更新cpsr,进入IRQ模式并禁止中断
    ldmfd   sp!,{r0}        @spsr->r0
    msr     spsr_cxsf,r0    @恢复spsr
    ldmfd   sp!,{r0-r12,lr}
    subs    pc,lr,#4        @此后，中断被重新打开

