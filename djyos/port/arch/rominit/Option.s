	.GLOBAL	PLL_ON_START
	.GLOBAL	ENDIAN_CHANGE
	.GLOBAL	ENTRY_BUS_WIDTH

.EQU  PLL_ON_START, 	1 @TRUE
.EQU  ENDIAN_CHANGE,	0 @FALSE
.EQU  ENTRY_BUS_WIDTH,  32

	.GLOBAL	FCLK
.EQU  FCLK,	400000000    @must modify in config.h too

@.EQU FCLK, 202800000
.EQU HCLK, (FCLK/2)
.EQU PCLK, (FCLK/4)

.IF	(FCLK == 48000000)
.EQU   M_MDIV,	0x38	@FIN=12.0MHz Fout=48.0MHz
.EQU   M_PDIV,	0x2
.EQU   M_SDIV,	0x2
.ENDIF

.IF	(FCLK == 96000000)
.EQU   M_MDIV,	0x38	@FIN=12.0MHz Fout=96.0MHz
.EQU   M_PDIV,	0x2
.EQU   M_SDIV,	0x1
.ENDIF

.IF	(FCLK == 271500000)
.EQU   M_MDIV,	0xad	@FIN=12.0MHz Fout=271.5MHz
.EQU   M_PDIV,	0x2
.EQU   M_SDIV,	0x2
.ENDIF

.IF	(FCLK == 304000000)
.EQU   M_MDIV,	0x44	@FIN=12.0MHz Fout=304.0MHz
.EQU   M_PDIV,	0x1
.EQU   M_SDIV,	0x1
.ENDIF

.IF	(FCLK == 400000000)
.EQU   M_MDIV,	92	@FIN=12.0MHz Fout=400.0MHz
.EQU   M_PDIV,	0x1
.EQU   M_SDIV,	0x1
.ENDIF

.EQU   U_MDIV,	0x20	@FIN=12.0MHz Fout=48MHz,for USB
.EQU   U_PDIV,	0x8
.EQU   U_SDIV,	0x0

.EQU MPLLVAL,((M_MDIV<<12)+(M_PDIV<<4)+M_SDIV)  @set MPLL
.EQU UPLLVAL,((U_MDIV<<12)+(U_PDIV<<4)+U_SDIV)  @set UPLL

    .MACRO MOV_PC_LR
      .ifdef THUMBCODE
            bx      lr
      .else

            mov     pc,lr
      .endif
    .ENDM

    .MACRO MOVEQ_PC_LR
      .ifdef THUMBCODE
            bxeq      lr
      .else

            moveq     pc,lr
      .endif
    .ENDM

