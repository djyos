//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 内核模块
//作者：lst
//版本：V1.0.0
//文件描述: 预加载操作系统
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "inc_os.h"
#include <string.h>
void pre_start(void);

extern uint8_t init_start[];
extern uint8_t init_limit[];
extern uint8_t text_preload_load_start[];
extern uint8_t text_preload_run_start[];
extern uint8_t text_preload_run_limit[];
extern uint8_t rodata_preload_load_start[];
extern uint8_t rodata_preload_run_start[];
extern uint8_t rodata_preload_run_limit[];
extern uint8_t rw_preload_load_start[];
extern uint8_t rw_preload_run_start[];
extern uint8_t rw_preload_run_limit[];
extern uint8_t zi_preload_start[];
extern uint8_t zi_preload_limit[];

//----预加载程序---------------------------------------------------------------
//功能：加载主加载器、中断管理模块，紧急代码
//参数: 无。
//返回: 无。
//----------------------------------------------------------------------------
//备注: 本函数移植关键，与开发系统有关，也与目标硬件配置有关
void load_preload(void)
{
    uint8_t *src,*des;
    void (*volatile pl_1st)(void) = pre_start;

    if(text_preload_run_start != text_preload_load_start)       //拷贝代码段
    {
        for(src=text_preload_load_start,des=text_preload_run_start;
                                        des<text_preload_run_limit;src++,des++)
            *des=*src;
    }
    if(rodata_preload_run_start != rodata_preload_load_start)   //拷贝只读数据段
    {
        for(src=rodata_preload_load_start,des=rodata_preload_run_start;
                                    des<rodata_preload_run_limit;src++,des++)
            *des=*src;
    }
    if(rw_preload_run_start != rw_preload_load_start)       //拷贝初始化数据段
    {
        for(src=rw_preload_load_start,des=rw_preload_run_start;
                                    des<rw_preload_run_limit;src++,des++)
            *des=*src;
    }
    for(src=zi_preload_start;src<zi_preload_limit;src++)
        *src=0;

    pl_1st();
}

