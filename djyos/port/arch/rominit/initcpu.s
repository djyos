@----------------------------------------------------
@Copyright (C), 2005-2008,  lst.
@版权所有 (C), 2005-2008,   lst.
@所属模块:  CPU初始化
@作者：     lst
@版本：      V1.0.0
@初始版本完成日期：2009-02-05
@文件描述:  CPU初始化必须用汇编语言实现的部分
@其他说明:  无
@修订历史:
@2. ...
@1. 日期: 2009-03-10
@   作者: lst
@   新版本号：V1.0.0
@   修改说明: s3c2410的原始版本
@------------------------------------------------------
    .include "Option.s"

.equ    USERMODE,           0x10
.equ    SYSMODE,            0x1f
.equ    FIQMODE,            0x11
.equ    IRQMODE,            0x12
.equ    SVCMODE,            0x13
.equ    ABORTMODE,          0x17
.equ    UNDEFMODE,          0x1b
.equ    MODEMASK,           0x1f
.equ    NOINT,              0xc0

.EQU   MPLLCON  ,  0x4c000004     @ MPLL Control
.EQU   UPLLCON  ,  0x4c000008     @ MPLL Control
.EQU   CLKCON   ,  0x4c00000c     @ Clock generator control
.EQU   CLKDIVN  ,  0x4c000014     @ Clock divider control
.EQU   CAMDIVN  ,  0x4c000018     @ Camera clock divider register
.EQU   WTCON    ,  0x53000000     @ Watch-dog timer mode
.equ   BWSCON   ,  0x48000000

    .global  _start

    .extern       load_preload          @ 加载程序，在C中定义

    .extern       FIQ_stack             @栈顶地址，在脚本中定义
    .extern       IRQ_stack_bottom      @栈顶地址，在脚本中定义
    .extern       Abort_stack           @栈顶地址，在脚本中定义
    .extern       Undef_stack           @栈顶地址，在脚本中定义
    .extern       SVC_stack             @栈顶地址，在脚本中定义
    .extern       USR_stack             @栈顶地址，在脚本中定义

    .text
    .balign 4

_start:
    b reset_start       @ handlerReset
    b except_undef      @ handlerUndef
    b except_swi        @ SWI interrupt handler
    b except_pabort     @ handlerPAbort
    b except_dabort     @ handlerDAbort
    b .                 @ handlerReserved
    b start_int  		@ handlerIRQ
    b .					@ fiq no use


except_undef:      @ 未定义指令异常
    b       .
except_swi:        @ SWI异常
    b       .
except_pabort:     @ 取址异常
    b       .
except_dabort:     @ 取数据异常
    b       .

reset_start:        @复位向量地址
    mrs     r0,cpsr                             @取CPSR
    bic     r0,r0,#MODEMASK                     @清模式位
    orr     r1,r0,#SVCMODE|NOINT    @设置为管理态，并禁止中断
    msr     cpsr_cxsf,r1            @切换到管理态,可防止意外返回0地址时出错.

    ldr     r0,=WTCON       @禁止看门狗
    ldr     r1,=0x0
    str     r1,[r0]

    mrc     p15,0,r0,c1,c0,0
    orr     r0,r0,#0xc0000000   @2410特有的控制位，设置cpu工作在异步时钟
    bic     r0,r0,#0x1000       @禁止Icache
    bic     r0,r0,#0x1          @禁止mmu
    bic     r0,r0,#0x4          @禁止Dcache
    mcr     p15,0,r0,c1,c0,0

    ldr     r0,=CAMDIVN
    ldr     r1,=0x0         @ HCLK=FLCK/4
    str     r1,[r0]

    ldr     r0,=CLKDIVN
    ldr     r1,=0x05        @ HCLK 由CAMDIVN.9控制  PCLK=HCLK/2
    str     r1,[r0]

    ldr     r0,=CLKCON
    ldr     r1,=0x7fff0     @ all clk enable
    str     r1,[r0]

    ldr     r0,=UPLLCON
    ldr     r1,=UPLLVAL  @ Fin=12MHz,48Mhz for USB
    str     r1,[r0]
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    ldr     r0,=MPLLCON
    ldr     r1,=MPLLVAL  @ Fin=12MHz,Fout=FCLK in option.s
    str     r1,[r0]

    @ initial ram here
/*;****************************************************
;*  Set memory control registers                                    *
;****************************************************/

    ldr     r0,=SMRDATA
    ldmia   r0,{r1-r13}
    ldr     r0,=BWSCON
    stmia   r0,{r1-r13}


    /*
     * Set processor and MMU to known state as follows (we may have not
     * been entered from a reset). We must do this before setting the CPU
     * mode as we must set PROG32/DATA32.
     *
     * MMU Control Register layout.
     *
     * bit
     *  0 M 0 MMU disabled
     *  1 A 0 Address alignment fault disabled, initially
     *  2 C 0 Data cache disabled
     *  3 W 0 Write Buffer disabled
     *  4 P 1 PROG32
     *  5 D 1 DATA32
     *  6 L 1 Should Be One (Late abort on earlier CPUs)
     *  7 B ? Endianness (1 => big)
     *  8 S 0 System bit to zero } Modifies MMU protections, not really
     *  9 R 1 ROM bit to one     } relevant until MMU switched on later.
     * 10 F 0 Should Be Zero
     * 11 Z 0 Should Be Zero (Branch prediction control on 810)
     * 12 I 0 Instruction cache control
     */

@以下初始化L1页表，平板式全映射
    ldr     r0,=0x30000000  @L1页表地址
    mov     r1,#0
    ldr     r3,=0xc12   @不开cache
loopnommu:
    mov     r2,r1,lsl #20   @目标段编号写入L1条目的高12位
    add     r2,r2,r3
    str     r2,[r0],#4
    add     r1,r1,#1
    cmp     r1,#0x300  @0~2fffffff不cache，不开写缓冲
    bne     loopnommu

    ldr     r3,=0xc1e   @开cache
loopmmu:
    mov     r2,r1,lsl #20   @目标段编号写入L1条目的高12位
    add     r2,r2,r3
    str     r2,[r0],#4
    add     r1,r1,#1
    cmp     r1,#0x340   @64M sdram区
    bne     loopmmu

    ldr     r3,=0xc12   @不开cache
loopnommu1:
    mov     r2,r1,lsl #20   @目标段编号写入L1条目的高12位
    add     r2,r2,r3
    str     r2,[r0],#4
    add     r1,r1,#1
    cmp     r1,#0x1000  @高端地址，不开cache
    bne     loopnommu1

    ldr     r0,=0x30000000
    mcr     p15,0,r0,c2,c0,0    @页表基地址
    ldr     r0,=0xffffffff      @全部域具有管理者权限
    mcr     p15,0,r0,c3,c0,0    @写域寄存器


@    mov     r0,#1
@delay:
@    sub     r0,r0,#1
@    cmp     r0,#0
@    bne     delay

@以下打开cache,使能mmu.
    mrc     p15,0,r0,c1,c0,0
    orr     r0,r0,#0xc0000000   @2410特有的控制位，设置cpu工作在异步时钟
    orr     r0,r0,#0x1000       @允许Icache
    orr     r0,r0,#0x1          @允许mmu
    orr     r0,r0,#0x4          @允许Dcache
    mcr     p15,0,r0,c1,c0,0

@    mrs     r1,cpsr
@    bic     r1,r1,#MODEMASK
@    orr     r1,r1,#SYSMODE|NOINT
@    msr     cpsr_cxsf,r1    @ userMode
@    ldr     sp,=USR_stack

@;****************************************************
@;* Initialize stacks               *
@;****************************************************
    mrs     r0,cpsr
    bic     r0,r0,#MODEMASK

    orr     r1,r0,#UNDEFMODE|NOINT
    msr     cpsr_cxsf,r1        @ UndefMode
    ldr     sp,=Undef_stack

    orr     r1,r0,#ABORTMODE|NOINT
    msr     cpsr_cxsf,r1        @ AbortMode
    ldr     sp,=Abort_stack

    orr     r1,r0,#IRQMODE|NOINT
    msr     cpsr_cxsf,r1        @ IRQMode
    ldr     sp,=IRQ_stack

    orr     r1,r0,#FIQMODE|NOINT
    msr     cpsr_cxsf,r1        @ FIQMode
    ldr     sp,=FIQ_stack

    orr     r1,r0,#SVCMODE|NOINT
    msr     cpsr_cxsf,r1        @ SVCMode
    ldr     sp,=SVC_stack

    orr     r1,r0,#SYSMODE|NOINT
    msr     cpsr_cxsf,r1        @ userMode
    ldr     sp,=USR_stack

#ifdef boot
    b          .
#else
    bl      load_preload
#endif

/*;*****************************************************************
;* Memory configuration has to be optimized for best performance *
;* The following parameter is not optimized.                     *
;*****************************************************************

;*** memory access cycle parameter strategy ***
; 1) Even FP-DRAM, EDO setting has more late fetch point by half-clock
; 2) The memory settings,here, are made the safe parameters even at 64Mhz.
; 3) FP-DRAM Parameters:tRCD=3 for tRAC, tcas=2 for pad delay, tcp=2 for bus load.
; 4) DRAM refresh rate is for 40Mhz.

;**********MEMORY CONTROL PARAMETERS*******************************/

    .GLOBAL    BUSWIDTH @max. bus width for the GPIO configuration
@ BUSWIDTH : 16,32
.EQU BUSWIDTH,    32

@ BWSCON
.EQU   DW8      ,       (0x0)
.EQU   DW16     ,       (0x1)
.EQU   DW32     ,       (0x2)
.EQU   WAIT     ,       (0x1<<2)
.EQU   UBLB     ,       (0x1<<3)

.ifeq BUSWIDTH-16
.EQU   B1_BWSCON, (DW16)
.EQU   B2_BWSCON, (DW16)
.EQU   B3_BWSCON, (DW16)
.EQU   B4_BWSCON, (DW16)
.EQU   B5_BWSCON, (DW16)
.EQU   B6_BWSCON, (DW16)
.EQU   B7_BWSCON, (DW16)
.endif
.ifeq BUSWIDTH-32
.EQU   B1_BWSCON, (DW16)
.EQU   B2_BWSCON, (DW16)
.EQU   B3_BWSCON, (DW16)
.EQU   B4_BWSCON, (DW16)
.EQU   B5_BWSCON, (DW16)
.EQU   B6_BWSCON, (DW32)
.EQU   B7_BWSCON, (DW32)
.endif

@ BANK0CON

.EQU   B0_Tacs  ,       0x3     @ 0clk
.EQU   B0_Tcos  ,       0x3     @ 0clk
.EQU   B0_Tacc  ,       0x7     @ 14clk
.EQU   B0_Tcoh  ,       0x3     @ 0clk
.EQU   B0_Tah   ,       0x3     @ 0clk
.EQU   B0_Tacp  ,       0x1
.EQU   B0_PMC   ,       0x0     @ normal

@ BANK1CON
.EQU   B1_Tacs  ,       0x1     @ 0clk
.EQU   B1_Tcos  ,       0x1     @ 0clk
.EQU   B1_Tacc  ,       0x6     @ 14clk
.EQU   B1_Tcoh  ,       0x1     @ 0clk
.EQU   B1_Tah   ,       0x1     @ 0clk
.EQU   B1_Tacp  ,       0x0
.EQU   B1_PMC   ,       0x0     @ normal

@ Bank 2 parameter
.EQU   B2_Tacs  ,       0x1     @ 0clk
.EQU   B2_Tcos  ,       0x1     @ 0clk
.EQU   B2_Tacc  ,       0x6     @ 14clk
.EQU   B2_Tcoh  ,       0x1     @ 0clk
.EQU   B2_Tah   ,       0x1     @ 0clk
.EQU   B2_Tacp  ,       0x0
.EQU   B2_PMC   ,       0x0     @ normal

@ Bank 3 parameter
.EQU   B3_Tacs  ,       0x1     @ 0clk
.EQU   B3_Tcos  ,       0x1     @ 0clk
.EQU   B3_Tacc  ,       0x6     @ 14clk
.EQU   B3_Tcoh  ,       0x1     @ 0clk
.EQU   B3_Tah   ,       0x1     @ 0clk
.EQU   B3_Tacp  ,       0x0
.EQU   B3_PMC   ,       0x0     @ normal

@ Bank 4 parameter
.EQU   B4_Tacs  ,       0x1     @ 0clk
.EQU   B4_Tcos  ,       0x1     @ 0clk
.EQU   B4_Tacc  ,       0x6     @ 14clk
.EQU   B4_Tcoh  ,       0x1     @ 0clk
.EQU   B4_Tah   ,       0x1     @ 0clk
.EQU   B4_Tacp  ,       0x0
.EQU   B4_PMC   ,       0x0     @ normal

@ Bank 5 parameter
.EQU   B5_Tacs  ,       0x1     @ 0clk
.EQU   B5_Tcos  ,       0x1     @ 0clk
.EQU   B5_Tacc  ,       0x6     @ 14clk
.EQU   B5_Tcoh  ,       0x1     @ 0clk
.EQU   B5_Tah   ,       0x1     @ 0clk
.EQU   B5_Tacp  ,       0x0
.EQU   B5_PMC   ,       0x0     @ normal

@ Bank 6 parameter
.EQU   B6_MT    ,       0x3     @ SDRAM
@ B6_Trcd       ,       0x0     @ 2clk
.EQU   B6_Trcd  ,       0x1     @ 3clk
.EQU   B6_SCAN  ,       0x1     @ 9bit

@ Bank 7 parameter
.EQU   B7_MT    ,       0x3     @ SDRAM
@ B7_Trcd       ,       0x0     @ 2clk
.EQU   B7_Trcd  ,       0x1     @ 2clk
.EQU   B7_SCAN  ,       0x1     @ 9bit

@ REFRESH parameter
.EQU   REFEN    ,       0x1     @ Refresh enable
.EQU   TREFMD   ,       0x0     @ CBR(CAS before RAS)/Auto refresh
.EQU   Trp      ,       0x1     @ 2clk
.EQU   Trc      ,       0x1     @ 4clk

.EQU   Tchr     ,       0x2     @ 3clk
.EQU   REFCNT   ,       1268     @ period=15.6us, HCLK=60Mhz, (2048+1-15.6*60) it should be 879

/*;************************************************
;bank0  16bit BOOT ROM
;bank1  8bit NandFlash
;bank2  16bit IDE
;bank3  8bit UDB
;bank4  rtl8019
;bank5  ext
;bank6  16bit SDRAM
;bank7  16bit SDRAM*/

        .balign 4
SMRDATA:
# Memory configuration should be optimized for best performance
# The following parameter is not optimized.
# Memory access cycle parameter strategy
# 1) The memory settings is  safe parameters even at HCLK=75Mhz.
# 2) SDRAM refresh period is for HCLK=75Mhz.

        .long ((B1_BWSCON<<4)+(B2_BWSCON<<8)+(B3_BWSCON<<12)+(B4_BWSCON<<16)+(B5_BWSCON<<20)+(B6_BWSCON<<24)+(B7_BWSCON<<28))
        .long ((B0_Tacs<<13)+(B0_Tcos<<11)+(B0_Tacc<<8)+(B0_Tcoh<<6)+(B0_Tah<<4)+(B0_Tacp<<2)+(B0_PMC))   @ GCS0
        .long ((B1_Tacs<<13)+(B1_Tcos<<11)+(B1_Tacc<<8)+(B1_Tcoh<<6)+(B1_Tah<<4)+(B1_Tacp<<2)+(B1_PMC))   @ GCS1
        .long ((B2_Tacs<<13)+(B2_Tcos<<11)+(B2_Tacc<<8)+(B2_Tcoh<<6)+(B2_Tah<<4)+(B2_Tacp<<2)+(B2_PMC))   @ GCS2
        .long ((B3_Tacs<<13)+(B3_Tcos<<11)+(B3_Tacc<<8)+(B3_Tcoh<<6)+(B3_Tah<<4)+(B3_Tacp<<2)+(B3_PMC))   @ GCS3
        .long ((B4_Tacs<<13)+(B4_Tcos<<11)+(B4_Tacc<<8)+(B4_Tcoh<<6)+(B4_Tah<<4)+(B4_Tacp<<2)+(B4_PMC))   @ GCS4
        .long ((B5_Tacs<<13)+(B5_Tcos<<11)+(B5_Tacc<<8)+(B5_Tcoh<<6)+(B5_Tah<<4)+(B5_Tacp<<2)+(B5_PMC))   @ GCS5
        .long ((B6_MT<<15)+(B6_Trcd<<2)+(B6_SCAN))    @ GCS6
        .long ((B7_MT<<15)+(B7_Trcd<<2)+(B7_SCAN))    @ GCS7
        .long ((REFEN<<23)+(TREFMD<<22)+(Trp<<20)+(Trc<<18)+(Tchr<<16)+REFCNT)
            .long 0x32                  @SCLK power down mode, BANKSIZE 32M/32M  16-8M 17-16M
            .long 0x30                  @MRSR6 CL=2clk
            .long 0x30                  @MRSR7


