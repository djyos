//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 文件系统芯片驱动
//作者：lst
//版本：V1.0.0
//文件描述: 用于三星nand flash的文件系统驱动模块
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-03-10
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 移植自yf44b0
//------------------------------------------------------
#ifndef _samsung_nand_H_
#define _samsung_nand_H_

#ifdef __cplusplus
extern "C" {
#endif

#define cn_oob_size     16
#define cn_reserve_blocks   0                   //定义一个保留区，紧跟MDR表

//下面定义各操作等待时间，单位微秒
#define cn_wait_address_ready   10
#define cn_wait_page_write      200
#define cn_wait_block_erase     2000
#define cn_wait_reset           500

//ecc校验操作结果
#define cn_all_right_verify     0               //完全正确
#define cn_ecc_right_verify     1               //经ecc纠正正确
#define cn_ecc_error_verify     2               //错误，不能纠正
#define cn_other_error_verify   cn_limit_uint32 //其他错误，一般是非法参数

struct nand_reg         //地址：0x4e000000
{
    uint16_t NFCONF;    //  0x4e000000 NAND Flash configuration
    uint16_t revs0;
    uint16_t NFCONT;    //  0x4e000004
    uint16_t revs1;
    uint16_t NFCMD ;    //  0x4e000008 NADD Flash command
    uint16_t revs2;
    uint16_t NFADDR;    //  0x4e00000c NAND Flash address
    uint16_t revs3;
    uint8_t NFDATA;     //  0x4e000010 NAND Flash data
    uint8_t revs30;
    uint8_t revs31;
    uint8_t revs32;
    uint32_t NFMECCD0 ; //  0x4e000014 NAND Flash ECCD0
    uint32_t NFMECCD1 ; //  0x4e000018 NAND Flash ECCD1
    uint32_t NFSECCD ;  //  0x4e00001C NAND Flash SPARE AREA ECC REGISTER
    uint16_t NFSTAT;    //  0x4e000020 NAND Flash operation status
    uint16_t revs4;
    uint32_t NFESTAT0;  //  0x4e000024 NAND flash ECC status for I/O[7:0]
    uint32_t NFESTAT1;  //  0x4e000028 NAND flash ECC status for I/O[15:8]
    uint32_t NFMECC0;   //  0x4e00002c NAND flash main area ECC0 status
    uint32_t NFMECC1;   //  0x4e000030 NAND flash main area ECC1 status
    uint32_t NFSECC;    //  0x4e000034 NAND flash spare area ECC status
    uint32_t NFSBLK;    //  0x4e000038 NAND flash start block address
    uint32_t NFEBLK;    //  0x4e00003c NAND flash end block address
};

#define nand_ce_bit     (0x40)
#define nand_busy_bit   (0x20)
#define nand_cle_bit    (0x80)
#define nand_ale_bit    (0x100)

// HCLK=100Mhz
#define cn_talcs		1  //1clk(10ns)
#define cn_twrph0		4  //3clk(30ns)
#define cn_twrph1		0  //1clk(10ns)  //cn_talcs+cn_twrph0+cn_twrph1>=50ns
// HCLK=50Mhz
//#define cn_talcs		0  //1clk(20ns)
//#define cn_twrph0		1  //2clk(40ns)
//#define cn_twrph1		0  //1clk(20ns)

#define ce_active()     (pg_nand_reg->NFCONT &= ~(1<<1))
#define ce_inactive()   (pg_nand_reg->NFCONT |= (1<<1))

#define CN_FS_BUF_TICKS (1000)      //文件自动写入时间,降低掉电损失
#define CN_FS_BUF_BLOCKS (16)       //文件缓冲块数,

// Flash commands:
#define cn_nand_select_page0        0x00
#define cn_nand_select_page1        0x01
#define cn_nand_select_oob          0x50
#define cn_nand_reset               0xff
#define cn_nand_page_program        0x80
#define cn_nand_startup_write       0x10
#define cn_nand_block_erase         0x60
#define cn_nand_startup_erase       0xd0
#define cn_nand_read_status         0x70
#define cn_nand_read_id             0x90

#define cn_nand_failure             0x01
#define RB                          0x40

struct nand_table
{
    uint16_t vendor_chip_id;
    uint16_t pages_per_block;
    uint32_t blocks_sum;
    uint32_t block_size;
    char *chip_name;
};

void __read_sector_and_oob(uint32_t sector,uint8_t *data);
bool_t write_PCRB_nand(uint32_t PCRB_block,
                       uint32_t protected_block,uint8_t *buf);
bool_t restore_PCRB_nand(uint32_t PCRB_block,uint32_t *restored);
bool_t __wait_ready_nand(void);
bool_t __wait_ready_nand_slow(uint16_t wait_time);
void __write_command_nand(uint8_t val);
uint32_t __read_sector_nand_no_ecc(uint32_t sector,uint32_t offset,
                                 uint8_t *data,uint32_t size);
uint32_t read_block_ss_no_ecc(uint32_t block,uint32_t offset,
                         uint8_t *buf,uint32_t size);
uint32_t __correct_sector(uint8_t *data,const uint8_t *old_ecc);
uint32_t __read_sector_nand_with_ecc(uint32_t sector,uint32_t offset,
                                 uint8_t *data,uint32_t size);
uint32_t read_block_ss_with_ecc(uint32_t block,uint32_t offset,
                         uint8_t *buf,uint32_t size);
uint8_t __read_status_nand(void);
uint32_t __write_sector_nand_no_ecc(uint32_t sector,uint32_t offset,
                                 uint8_t *data,uint32_t size);
uint32_t write_block_ss_no_ecc(uint32_t block,uint32_t offset,
                          uint8_t *buf,uint32_t size);
uint32_t __write_sector_nand_with_ecc(uint32_t sector,uint32_t offset,
                                 uint8_t *data,uint32_t size);
uint32_t write_block_ss_with_ecc(uint32_t block,uint32_t offset,
                          uint8_t *buf,uint32_t size);
bool_t erase_block_nand(uint32_t block_no);
bool_t __erase_all_nand(void);
bool_t query_block_ready_ss_with_ecc(uint32_t block,uint32_t offset,
                              uint8_t *buf,uint32_t size);
bool_t query_block_ready_nand_no_ecc(uint32_t block,uint32_t offset,
                              uint8_t *buf,uint32_t size);
bool_t query_ready_with_data_nand(uint8_t *new_data,uint8_t *org_data,
                                        uint32_t size);
bool_t check_block_nand(uint32_t block_no);
bool_t __mark_invalid_block(uint32_t block);
uint32_t __check_all_ss(void);
void __make_sector_ecc(const uint8_t *data,uint8_t *ecc);
uint16_t __read_chip_id (void);
bool_t __parse_chip(uint16_t id);
void __reset_nand(void);
bool_t module_init_fs_nandflash(void);

#ifdef __cplusplus
}
#endif

#endif // _samsung_nand_H_

