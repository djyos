//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 内核模块
//作者：lst
//版本：V1.0.0
//文件描述: 加载操作系统
//其他说明:
//修订历史:
//2. 日期: 2009-04-24
//   作者: lst
//   新版本号：V1.1.0
//   修改说明: 原版本中，把系统初始化也放在这里了，本版本分离之
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "inc_os.h"
//#include <string.h>
#include "uart.h"


#ifndef debug
extern uint8_t init_start[];
extern uint8_t init_limit[];
extern uint8_t text_sysload_load_start[];
extern uint8_t text_sysload_run_start[];
extern uint8_t text_sysload_run_limit[];
extern uint8_t rodata_sysload_load_start[];
extern uint8_t rodata_sysload_run_start[];
extern uint8_t rodata_sysload_run_limit[];
extern uint8_t rw_sysload_load_start[];
extern uint8_t rw_sysload_run_start[];
extern uint8_t rw_sysload_run_limit[];
#endif
extern uint8_t zi_sysload_start[];
extern uint8_t zi_sysload_limit[];

#ifdef debug
void __asm_set_debug(void);
#endif
void loader(void);
void pre_start(void);

//----操作系统内核加载程序-----------------------------------------------------
//功能：加载所有操作系统内核代码，以及在si模式下全部应用程序代码。
//参数: 无。
//返回: 无。
//----------------------------------------------------------------------------
//备注: 本函数移植关键，与开发系统有关，也与目标硬件配置有关
void pre_start(void)
{
#ifdef debug
        loader();
#endif
        __int_init();
        critical();
#ifndef debug
        loader();
#endif
    start_sys();        //开始启动系统
}

//----操作系统内核加载程序-----------------------------------------------------
//功能：加载所有操作系统内核代码，以及在si模式下全部应用程序代码。
//参数: 无。
//返回: 无。
//----------------------------------------------------------------------------
//备注: 本函数移植关键，与开发系统有关，也与目标硬件配置有关
void loader(void)
{
    uint8_t *src;
#ifndef debug
    uint8_t *des;
    if(text_sysload_run_start != text_sysload_load_start)       //拷贝代码段
        for(src=text_sysload_load_start,des=text_sysload_run_start;des<text_sysload_run_limit;src++,des++)
            *des=*src;
    if(rodata_sysload_run_start != rodata_sysload_load_start)   //拷贝只读数据段
        for(src=rodata_sysload_load_start,des=rodata_sysload_run_start;des<rodata_sysload_run_limit;src++,des++)
            *des=*src;
    if(rw_sysload_run_start != rw_sysload_load_start)           //拷贝初始化数据段
        for(src=rw_sysload_load_start,des=rw_sysload_run_start;des<rw_sysload_run_limit;src++,des++)
            *des=*src;
        for(src=zi_sysload_start;src<zi_sysload_limit;src++)
            *src=0;
#endif

#ifdef debug
    for(src=zi_sysload_start;src<zi_sysload_limit;src++)
        *src=0;
#endif

}

