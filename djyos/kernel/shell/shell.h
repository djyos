#ifndef __db_dm_h__
#define __db_dm_h__

#ifdef __cplusplus
extern "C" {
#endif

bool_t module_init_shell(void);
void printf_str(const char *str);
void printf_char(const char ch);
void printf_hex(uint32_t hex,uint32_t size);
void itostring(uint32_t data,char buf[]);
bool_t sh_show_for_cycle(void);
bool_t sh_cmd_parser(const char *buf,const char *keyword,char **param);
char *sh_get_word(char *buf,char **next);
void __sh_show_branche(struct rsc_node *branche);
void sh_list_rsc_all(void);
void sh_list_flash_chip(void);
void sh_list_pool(void);
void sh_list_opened_files(void);
void sh_list_device(void);
void sh_list_mutex(void);
void sh_list_semp(void);
void test_recv(struct event_script *my_event);
bool_t sh_uart_test();
bool_t sh_keypad_test();
bool_t sh_exec_command(char *cmd);
void sh_service(struct event_script *my_event);

#ifdef __cplusplus
}
#endif

#endif //__db_dm_h__
