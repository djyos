#include "inc_os.h"
#include "gpio.h"
#include <string.h>
#include <stdlib.h>
#include "uart.h"
#include "shell.h"

struct  dev_handle *pg_shell_lhdl;
struct dev_handle *test_uart_lhdl;

//----调试模块初始化函数-------------------------------------------------------
//功能: 初始化调试模块
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
bool_t module_init_shell(void)
{
    uint16_t db_dm_evtt;
    db_dm_evtt = djy_evtt_regist(true,                //mark型事件
                                true,               //参数覆盖
                                cn_prio_real,       //默认优先级
                                1,                  //虚拟机限制，mark型无效
                                sh_service,         //入口函数
                                5120,                //栈尺寸
                                "debug spy service" //事件类型名
                                );
    if(db_dm_evtt == cn_invalid_evtt_id)
        return false;
    pg_shell_lhdl = dev_open_left("uart0",0);
    if(pg_shell_lhdl == NULL)
    {
        djy_evtt_unregist(db_dm_evtt);
        return false;
    }
    dev_ctrl(pg_shell_lhdl,enum_uart_connect_recv_evtt,db_dm_evtt,0);
    dev_ctrl(pg_shell_lhdl,enum_uart_start,0,0);
    printf_str("\r\r欢迎使用都江堰操作系统\r\n>");
    return true;
}

//这是printf函数就绪前的临时函数
void printf_str(const char *str)
{
    uint32_t len;
    len = djy_rtstrlen(str,256);
    if(len == cn_limit_uint32)
        len = 256;
    dev_write(pg_shell_lhdl,(ptu32_t)str,0,len);
}
//这是printf函数就绪前的临时函数
void printf_char(const char ch)
{
    dev_write(pg_shell_lhdl,(ptu32_t)&ch,0,1);
}
//这是printf函数就绪前的临时函数
void printf_hex(uint32_t hex,uint32_t size)
{
    char buf[8];
    uint8_t uni;
    uint32_t off;
    if((size < 1 )|| (size >8))
        return ;
    off = size;
    for(;off > 0; off--)
    {
        uni = (hex >>(off*4-4))&0xf;
        if(uni < 10)
            buf[size-off] = uni+0x30;
        else
            buf[size-off] = uni+0x37;
    }
    dev_write(pg_shell_lhdl,(ptu32_t)buf,0,size);
}
//这是printf函数就绪前的临时函数
void itostring(uint32_t data,char buf[])
{
    ufast_t loop;
    uint32_t num = 1000000000;
    ufast_t sig,i=0;
    for(loop = 0; loop<10; loop++)
    {
        if((data/num) != 0)
            break;
        num = num/10;
    }
    for(; loop<10; loop++)
    {
        sig = data/num %10;
        buf[i] = (uint8_t)sig + 0x30;
        i++;
        num = num/10;
    }
    buf[i] = '\0';
}

//----显示for循环时间----------------------------------------------------------
//功能: 显示一个for(;;)循环的执行时间，循环变量分别是8位、16位、32位。
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
bool_t sh_show_for_cycle(void)
{
    char buf[40];

    strcpy(buf,"32位循环变量 ");
    itostring(u32g_ns_of_u32for,buf+13);
    printf_str(buf);
    printf_str("nS\r\n");

    strcpy(buf,"16位循环变量 ");
    itostring(u32g_ns_of_u16for,buf+13);
    printf_str(buf);
    printf_str("nS\r\n");

    strcpy(buf," 8位循环变量 ");
    itostring(u32g_ns_of_u8for,buf+13);
    printf_str(buf);
    printf_str("nS\r\n");
    return true;
}

//----显示资源队列----------------------------------------------------------
//功能: 按参数要求显示资源队列中资源的名字，若该资源名字为空，则显示"无名资源"
//参数: 参数字符串，含义:
//          all，显示全部资源队列
//          flash chip，显示文件系统加载的flash芯片
//          semaphore，显示系统中的所有信号量
//          mutex，显示系统中的所有互斥量
//          device，显示系统中的所有泛设备
//          opened files，显示打开的文件和路径
//          pool，显示所有内存池
//返回: true
//-----------------------------------------------------------------------------
bool_t sh_list_rsc(char *param)
{
    char *next_para;
    if(sh_cmd_parser(param,"all",&next_para) == true)
        sh_list_rsc_all();
    else if(sh_cmd_parser(param,"flash chip",&next_para) == true)
        sh_list_flash_chip();
    else if(sh_cmd_parser(param,"semaphore",&next_para) == true)
        sh_list_semp();
    else if(sh_cmd_parser(param,"mutex",&next_para) == true)
        sh_list_mutex();
    else if(sh_cmd_parser(param,"device",&next_para) == true)
        sh_list_device();
    else if(sh_cmd_parser(param,"opened files",&next_para) == true)
        sh_list_opened_files();
    else if(sh_cmd_parser(param,"pool",&next_para) == true)
        sh_list_pool();
    else
        printf_str("参数错误\r\n");
    return true;
}

//----显示内存池资源----------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示内存池资源
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_pool(void)
{
    struct rsc_node *pool_tree;
    pool_tree = rsc_search_son(rsc_get_root(),"固定块分配池");
    if(pool_tree == NULL)
    {
        printf_str("no 固定块分配池 found");
        printf_str("\r\n");
    }
    __sh_show_branche(pool_tree);
}

//----显示打开的文件资源-------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示打开的文件资源，如果文件有多级目录，则每级
//      目录算是一个独立资源
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_opened_files(void)
{
    struct rsc_node *opened_tree;
    opened_tree = rsc_search_son(rsc_get_root(),"opened files");
    if(opened_tree == NULL)
    {
        printf_str("no opened files found");
        printf_str("\r\n");
    }
    __sh_show_branche(opened_tree);
}

//----显示泛设备---------------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示泛设备
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_device(void)
{
    struct rsc_node *dev_tree;
    dev_tree = rsc_search_son(rsc_get_root(),"dev");
    if(dev_tree == NULL)
    {
        printf_str("no dev found");
        printf_str("\r\n");
    }
    __sh_show_branche(dev_tree);
}

//----显示互斥量----------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示互斥量
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_mutex(void)
{
    struct rsc_node *mutex_tree;
    mutex_tree = rsc_search_son(rsc_get_root(),"mutex");
    if(mutex_tree == NULL)
    {
        printf_str("no mutex found");
        printf_str("\r\n");
    }
    __sh_show_branche(mutex_tree);
}

//----显示信号量----------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示信号量
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_semp(void)
{
    struct rsc_node *semp_tree;
    semp_tree = rsc_search_son(rsc_get_root(),"semaphore");
    if(semp_tree == NULL)
    {
        printf_str("no semaphore found");
        printf_str("\r\n");
    }
    __sh_show_branche(semp_tree);
}

//----显示flash芯片----------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示文件系统中加载的全部flash芯片
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_flash_chip(void)
{
    struct rsc_node *chip_tree;
    chip_tree = rsc_search_son(rsc_get_root(),"flash chip");
    if(chip_tree == NULL)
    {
        printf_str("no flash chip found");
        printf_str("\r\n");
    }
    __sh_show_branche(chip_tree);
}

//----显示全部资源----------------------------------------------------------
//功能: 由sh_list_rsc函数调用，显示全部资源
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_list_rsc_all(void)
{
    struct rsc_node *rsc_root;
    rsc_root = rsc_get_root();
    __sh_show_branche(rsc_root);
}

//----显示资源树---------------------------------------------------------------
//功能: 显示某资源结点起始的一个资源分支，不包含该资源自身
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void __sh_show_branche(struct rsc_node *branche)
{
    struct rsc_node *current_node = branche;
    ucpu_t len;
    char neg[20];
    for(len = 0; len<20; len++)
        neg[len] = '-';
    while(1)
    {
        current_node = rsc_trave_scion(branche,current_node);
        if(current_node == NULL)
        {
            printf_str("\r\n");
            break;
        }
        len = rsc_get_class(current_node);
        neg[len] = '\0';
        printf_str(neg);
        neg[len] = '-';
        if(current_node->name != NULL)
        {
            printf_str(current_node->name);
            printf_str("\r\n");
        }else
        {
            printf_str("无名资源\r\n");
        }
    }
}

//----显示内存-----------------------------------------------------------------
//功能: 显示某地址开始的一段内存，每行显示16个单元，只显示，不能修改
//参数: param，参数串，本命令需要三个参数，用空格隔开
//          参数1:起始地址，10进制或16进制(0x开头)
//          参数2:显示的单元数
//          参数3:没单元字节数，合法值是1、2、4、8，其他数值将返回错误
//返回: true=正常显示，false=错误
//-----------------------------------------------------------------------------
bool_t sh_show_memory(char *param)
{
    ptu32_t addr;
    uint32_t unit_num,unit_bytes,len;
    char *word,*next_param,*temp;

    word = sh_get_word(param,&next_param);
    addr = strtoul(word,&temp,0);
    if(*temp != 0)
    {
        printf_str("address error");
        return false;
    }
    word = sh_get_word(next_param,&next_param);
    unit_num = strtoul(word,&temp,0);
    if(*temp != 0)
    {
        printf_str("unit_num error");
        return false;
    }
    word = sh_get_word(next_param,&next_param);
    unit_bytes = strtoul(word,&temp,0);
    if(*temp != 0)
    {
        printf_str("unit_bytes error");
        return false;
    }
#if (cn_byte_wide == 8)  //字节位宽=8，最常见的情况
    switch(unit_bytes)
    {
        case 1:
        {
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint8_t*)addr,2);
                printf_char(' ');
                addr ++;
                if(addr %16 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 2:
        {
            addr &= ~(ptu32_t)1;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint16_t*)addr,4);
                printf_char(' ');
                addr +=2;
                if(addr %32 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 4:
        {
            addr &= ~(ptu32_t)3;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr +=4;
                if(addr %64 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 8:
        {
            addr &= ~(ptu32_t)7;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                addr +=4;
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr +=4;
                if(addr %128 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        default :
        {
            printf_str("unit_bytes error");
            return false;
        } break;
    }
#elif (cn_byte_wide == 16)  //字节位宽=16
    switch(unit_bytes)
    {
        case 1:
        {
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint16_t*)addr,4);
                printf_char(' ');
                addr ++;
                if(addr %16 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 2:
        {
            addr &= ~(ptu32_t)1;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr +=2;
                if(addr %32 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 4:
        {
            addr &= ~(ptu32_t)3;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                addr +=2;
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr +=2;
                if(addr %64 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        default :
        {
            printf_str("unit_bytes error");
            return false;
        } break;
    }
#elif (cn_byte_wide == 32)  //字节位宽=32
    switch(unit_bytes)
    {
        case 1:
        {
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr ++;
                if(addr %16 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        case 2:
        {
            addr &= ~(ptu32_t)1;
            printf_hex(addr,8);
            printf_char(':');
            for(len = 0; len < unit_num; len++)
            {
                printf_hex(*(uint32_t*)addr,8);
                addr +=2;
                printf_hex(*(uint32_t*)addr,8);
                printf_char(' ');
                addr +=2;
                if(addr %32 == 0)
                {
                    printf_str("\r\n");
                    if(len != unit_num)
                    {
                        printf_hex(addr,8);
                        printf_char(':');
                    }
                }
            }
        } break;
        default :
        {
            printf_str("unit_bytes error");
            return false;
        } break;
    }
#endif
    return true;
}

//测试用---db
void test_recv(struct event_script *my_event)
{
    uint32_t completed,i;
    uint16_t recv_buf[2600];
    completed=0;
    do
    {
        dev_read(test_uart_lhdl,0,(ptu32_t)((uint8_t*)recv_buf+completed),
                        my_event->parameter1);
        completed += my_event->parameter1;
        if(completed >= 5000)
            break;
        if(djy_evtt_pop_sync(pg_event_running->evtt_id,1,1000) == cn_sync_timeout)
        {
            printf_str("通信错误\r\n");
            break;
        }
    }while(1);
    completed=2000;
    for(i = 0; i<2500; i++)
    {
        if(recv_buf[i] != i)
        {
            printf_str("有误码\r\n");
            return;
        }
    }
    printf_str("无误码\r\n");
}

//测试用----db
bool_t sh_uart_test()
{
    uint16_t send_buf[2500],i,test_evtt;
    for(i = 0; i < 2500; i++)
        send_buf[i] = i;
    test_uart_lhdl = dev_open_left("uart0",0);
    test_evtt = djy_evtt_regist(true,                 //mark型事件
                                true,               //参数覆盖
                                cn_prio_real,       //默认优先级
                                1,                  //虚拟机限制，mark型无效
                                test_recv,          //入口函数
                                6000,               //栈尺寸
                                "debug test_recv"   //事件类型名
                                );
    dev_ctrl(test_uart_lhdl,enum_uart_connect_recv_evtt,test_evtt,0);
    dev_ctrl(test_uart_lhdl,enum_uart_start,0,0);
    dev_write(test_uart_lhdl,(ptu32_t)send_buf,0,5000);
    djy_evtt_done_sync(test_evtt,1,5000);
    dev_close_left(test_uart_lhdl);
    djy_evtt_unregist(test_evtt);
    return true;
}

//----测试键盘---------------------------------------------------------------
//功能: 显示按键扫描码，连续10秒无按键则返回
//参数: 无
//返回: true
//-----------------------------------------------------------------------------
bool_t sh_keypad_test()
{
    struct key_script key;
    char buf[20];
    printf_str("请按键，将显示键盘扫描码\r\n");
    printf_str("10秒内无按键则退出\r\n");
    while(1)
    {
        if( ! key_read(&key,10000))
        {
            return true;
        }
        itostring(key.time,buf);
        printf_str("按键时间：");
        printf_str(buf);
        printf_str(" 键值 ");
        printf_hex(key.key_value[2],2);
        printf_hex(key.key_value[1],2);
        printf_hex(key.key_value[0],2);
        printf_str("\r\n");
    }

}

//----命令串分析---------------------------------------------------------------
//功能: 分析buf包含的字符串第一个单词是否keyword命令。并且在param中返回keyword
//      后第一个非零且非回车换行的字符的指针，该指针实际包含第一个命令行参数的
//      地址。如果没有参数，则param=NULL。
//参数: buf，待分析的字符串
//      keyword，待匹配的命令
//      param，返回命令行参数指针
//返回: true = 命令匹配，false=命令不匹配
//-----------------------------------------------------------------------------
bool_t sh_cmd_parser(const char *buf,const char *keyword,char **param)
{
    uint32_t i=0;
    *param = NULL;
    while(keyword[i] != 0)
    {
        if(buf[i] != keyword[i])
            return false;
        i++;
    }
    while(buf[i] != 0)
    {
        if((buf[i]!=' ') && (buf[i] != '\n') && (buf[i] != '\r'))
        {
            *param = (char*)&buf[i];
            break;
        }
        i++;
    }
    return true;
}

//----提取单词---------------------------------------------------------------
//功能: 从buf中提取一个由空格或行结束符隔开的单词，next用于返回下一个单词首地址，
//      如果没有下一个单词，则next=NULL。
//参数: buf，待分析的字符串
//      param，返回下一个单词指针
//返回: 提取的单词指针，已将单词后面的分隔符换成串结束符'\0'
//-----------------------------------------------------------------------------
char *sh_get_word(char *buf,char **next)
{
    uint32_t i=0;
    *next = NULL;
    while(1)
    {
        if((buf[i] == ' ') || (buf[i] == 0))
        {
            buf[i] = '\0';
            if(buf[i] != 0)
                return buf;
            else
                break;
        }
        i++;
    }
    i++;
    while(buf[i] != 0)
    {
        if((buf[i]!=' ') && (buf[i] != '\n') && (buf[i] != '\r'))
        {
            *next = &buf[i];
            break;
        }
        i++;
    }
    return buf;
}

//----执行控制台命令-----------------------------------------------------------
//功能: 分析并执行控制台命令
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
bool_t sh_exec_command(char *buf)
{
    bool_t result;
    char *param;
    if(sh_cmd_parser(buf,"ListRSC",&param)==true)
        result = sh_list_rsc(param);
    else if(sh_cmd_parser(buf,"d",&param)==true)
        result = sh_show_memory(param);
    else if(sh_cmd_parser(buf,"speed",&param)==true)
        result = sh_show_for_cycle();
    else if(sh_cmd_parser(buf,"Uart0Test",&param)==true)
        result = sh_uart_test();
    else if(sh_cmd_parser(buf,"Keypad",&param)==true)
        result = sh_keypad_test();
    else
        result = false;
    return result;
}

//----控制台服务函数-----------------------------------------------------------
//功能: 返回console输入的字符，带console输入回车符时，执行命令。一次命令不得超过
//      255字符。
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void sh_service(struct event_script *my_event)
{
    char command[257],commands[256];
    uint8_t chars=0,reads;
    uint8_t offset;
    while(1)
    {
        do
        {
            //读串口设备
            reads = dev_read(pg_shell_lhdl,0,(ptu32_t)commands,255);
            for(offset = 0; offset < reads; offset++)
            {
                //检测命令串结束符
                if((commands[offset] == '\r')||(commands[offset] == '\n'))
                {
                    if(commands[offset+1] == '\n')
                        offset++;
                    printf_str("\r\n");
                    command[chars] = '\0';
                    if(sh_exec_command(command))   //执行命令
                        printf_str("\r\n执行成功\r\n");
                    else
                        printf_str("\r\n执行失败\r\n");
                    printf_char('>');
                    chars = 0;
                }else
                {
                    if(commands[offset] == 8) //这是delete字符
                    {
                        if(chars > 0)
                        {
                            chars --;
                            printf_str("\b \b");
                        }
                    }else
                    if(chars < 255)  //命令串最长是32字节，超长的被忽略
                    {
                        command[chars] = commands[offset];
                        printf_char((char)command[chars]);
                        chars++;
                    }
                }
            }
        }while(reads ==255); //直到所有的数据都被读出
        //执行本句后，事件将被阻塞，由于被同步的事件类型就是本事件的事件类型，
        //所以同步条件就是：再次发生本类型事件。
        //参数 ‘1’表示只要发生一次事件就可以了
        //参数cn_timeout_forever表示如果一直不发生，就永久等待
        //关于事件类型弹出同步，参见《都江堰操作系统与嵌入式系统设计》的第5.3节
        djy_evtt_pop_sync(pg_event_running->evtt_id,1,cn_timeout_forever);
    }
}

