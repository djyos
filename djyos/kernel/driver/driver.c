//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:泛设备管理模块
//作者：lst
//版本：V1.0.0
//文件描述:提供泛设备管理功能
//其他说明:
//修订历史:
//2. 日期:2009-03-03
//   作者:lst
//   新版本号：1.0.1
//   修改说明: 修正了dev_close_left函数和dev_close_right函数的错误，该bug由
//             网友sniper提交
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "inc_os.h"
#include <string.h>

static struct  pan_device *pg_device_rsc_tree;

static struct pan_device tg_mem_of_device[cn_device_limit+1];//泛设备控制块内存池
static struct dev_handle tg_mem_of_handle[cn_handle_limit];  //泛设备句柄内存池
static struct mem_cell_pool *pg_pan_device_pool;     //设备控制块内存池头指针
static struct mem_cell_pool *pg_device_handle_pool;  //设备句柄内存池头指针
static struct mutex_LCB tg_dev_mutex;       //保护设备资源树的并发访问安全

//----初始化泛设备驱动---------------------------------------------------------
//功能：1.在资源管理链中增加名为"dev"的根结点.
//      2.初始化泛设备控制块内存池
//      3.初始化泛设备句柄内存池
//参数：无
//返回：无
//-----------------------------------------------------------------------------
bool_t module_driver_init(void)
{
    static struct  pan_device root;

    pg_device_rsc_tree = (struct  pan_device *)
            rsc_add_tree_root(&root.node,sizeof(struct  pan_device),"dev");
    //初始化泛设备控制块内存池
    pg_pan_device_pool = mb_create((void*)tg_mem_of_device,
                                    cn_device_limit,
                                    sizeof(struct  pan_device),
                                    "泛设备控制块池");
    //初始化泛设备句柄内存池.
    pg_device_handle_pool = mb_create((void*)tg_mem_of_handle,
                                    cn_handle_limit,
                                    sizeof(struct  dev_handle),
                                    "泛设备句柄池");
    __mutex_createe_knl(&tg_dev_mutex,true,"device driver");
    return true;
}

//----添加根设备---------------------------------------------------------------
//功能: 添加一个根设备，根设备是pg_device_root的子设备，本函数直接调用
//      dev_add_device实现，纯粹是为了保护全局变量不被pg_device_root别人访问而
//      设立。
//参数: 见dev_add_device的说明
//返回:新加入的设备的资源结点指针.资源不足则返回NULL
//本函数由泛设备driver作者调用，不建议应用程序使用
//-----------------------------------------------------------------------------
struct  pan_device *dev_add_root_device(char         *name,
                                        struct semaphore_LCB *right_semp,
                                        struct semaphore_LCB *left_semp,
                                        dev_write_func   right_write ,
                                        dev_read_func    right_read,
                                        dev_ctrl_func    right_ctrl ,
                                        dev_write_func    left_write ,
                                        dev_read_func     left_read ,
                                        dev_ctrl_func     left_ctrl )
{
    return dev_add_device(pg_device_rsc_tree,name,
                          right_semp,left_semp,
                          right_write,right_read,right_ctrl,
                          left_write,left_read,left_ctrl);
}
//----添加设备-----------------------------------------------------------------
//功能: 新设备加入到兄弟设备的尾部,
//参数: parent_device,待添加设备的父设备
//      name，新设备的名字
//      right_semp，设备右手接口信号量指针
//      left_semp，设备左手接口信号量指针
//      right_write，右手写函数指针
//      right_read，右手读函数指针
//      right_ctrl，右手控制函数指针
//      left_write，左手写函数指针
//      left_read，左手读函数指针
//      left_ctrl，左手控制函数指针
//返回:新加入的设备的资源结点指针.资源不足则返回NULL
//本函数由泛设备driver作者调用，不建议应用程序使用，故不使用struct  dev_handle
//而是用struct  pan_device类型参数
//-----------------------------------------------------------------------------
struct  pan_device *dev_add_device(struct  pan_device *parent_device,
                                   char               *name,
                                   struct semaphore_LCB   *right_semp,
                                   struct semaphore_LCB   *left_semp,
                                   dev_write_func     right_write ,
                                   dev_read_func      right_read,
                                   dev_ctrl_func      right_ctrl ,
                                   dev_write_func     left_write ,
                                   dev_read_func      left_read ,
                                   dev_ctrl_func      left_ctrl )
{
    struct  pan_device  *new_device,*result;
    if((parent_device == NULL) || (name == NULL))
        return NULL;        //设备不能没有名字
    if(strchr(name,'\\'))   //名字中不能包含字符 \.
        return NULL;
    mutex_pend(&tg_dev_mutex,cn_timeout_forever);
    if(rsc_search_son(&parent_device->node,name) != NULL)
    {
        djy_error_login(enum_drv_homonymy,"设备重名");
        result = NULL;
    }else
    {
        if(parent_device != NULL)
        {
            //分配泛设备控制块给新设备
            new_device = mb_malloc(pg_pan_device_pool,0);
            if(new_device != NULL)
            {
                //新设备添加到父设备下成为满子结点
                rsc_add_son(&parent_device->node,&new_device->node,
                                        sizeof(struct  pan_device),name);
                new_device->left_semp = left_semp;
                new_device->right_semp = right_semp;
                if(right_write != NULL)
                    new_device->right_hand.io_write = right_write;
                else
                    new_device->right_hand.io_write = (dev_write_func)NULL_func;
                if(right_read != NULL)
                    new_device->right_hand.io_read = right_read;
                else
                    new_device->right_hand.io_read = (dev_read_func)NULL_func;
                if(right_ctrl != NULL)
                    new_device->right_hand.io_ctrl = right_ctrl;
                else
                    new_device->right_hand.io_ctrl = (dev_ctrl_func)NULL_func;
                if(left_write != NULL)
                    new_device->left_hand.io_write = left_write;
                else
                    new_device->left_hand.io_write = (dev_write_func)NULL_func;
                if(left_read != NULL)
                    new_device->left_hand.io_read = left_read;
                else
                    new_device->left_hand.io_read = (dev_read_func)NULL_func;
                if(left_ctrl != NULL)
                    new_device->left_hand.io_ctrl = left_ctrl;
                else
                    new_device->left_hand.io_ctrl = (dev_ctrl_func)NULL_func;
                result = new_device;
            }else
            {
                djy_error_login(enum_mem_tried,"内存不足");
                result = NULL;
            }
        }else
            result = NULL;
    }
    mutex_post(&tg_dev_mutex);
    return result;
}

//----卸载设备------------------------------------------------------------------
//功能: 把设备从设备链表中删除,并释放其占用的内存.该设备需符合以下条件:
//      1.该设备没有子设备.
//      2.正在使用该设备的用户数为0.
//参数: device,待释放的设备
//返回: 成功释放返回true,否则返回false
//与对称函数dev_add_device一样，本函数由泛设备driver作者调用，不建议应用程序
//使用，故不使用struct  dev_handle而是用struct  pan_device类型参数
//-----------------------------------------------------------------------------
bool_t dev_delete_device(struct  pan_device * device)
{
    struct  rsc_node *node;
    bool_t en_del = true;
    mutex_pend(&tg_dev_mutex,cn_timeout_forever);
    if(device->left_semp != NULL)
        if(semp_query_used(device->left_semp) != 0)
            en_del = false;
    if(device->right_semp != NULL)
        if(semp_query_used(device->right_semp) != 0)
            en_del = false;
    if(en_del)
    {   //如果没有用户使用设备,则可以删除.不能使用左右相加==0来判断,因为相加的
        //和刚好溢出时等于0.
        node = &device->node;
        if(rsc_del_node(node) == true)
        {
            mutex_post(&tg_dev_mutex);
            mb_free(pg_pan_device_pool,device);
            return(true);           //成功卸载设备
        }
    }
    mutex_post(&tg_dev_mutex);
    djy_error_login(enum_drv_dev_del,NULL);
    return false;
}

//----打开设备左手接口---------------------------------------------------------
//功能: 根据设备名打开设备的左手接口，搜索整个设备资源树，找到名称与name匹配的
//      资源结点，然后从泛设备句柄池中分配控制块,把左手接口指针赋值给句柄后,
//      返回该句柄指针。
//参数: name,设备名字符串,包含路径名，但不必包含'dev\'这4个字符
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备(含经过等待后打开)返回设备句柄,否则返回NULL.
//-----------------------------------------------------------------------------
struct  dev_handle *dev_open_left(char *name,uint32_t timeout)
{
    struct  pan_device     *pan;
    struct  dev_handle     *handle;
    struct  dev_handle     *result;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return NULL;
    //在设备树中搜索设备
    pan = (struct  pan_device *)rsc_search(&pg_device_rsc_tree->node,name);
    if(pan == NULL)     //如果没有找到name设备,返回空
    {
        result = NULL;
        goto end_of_dev_open_left;
    }
    if(semp_pend(pan->left_semp,timeout)==false)//获取信号量,这是保护设备的
    {
        result = NULL;
        goto end_of_dev_open_left;
    }
    //在分配内存之前判断信号量，可避免在没有信号的情况下长时间占用内存
    handle = mb_malloc(pg_device_handle_pool,0);  //从池中分配句柄内存块
    if(handle != NULL)
    {//成功分配句柄
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_left;              //我是左手接口句柄
        if(pg_event_running->held_device == NULL)   //事件还没有打开过设备
        {
            handle->next = handle;      //句柄指针自成双向循环链表
            handle->previous = handle;
            pg_event_running->held_device = handle; //事件指针指向设备链表
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result = handle;
    }else
        result = NULL;

end_of_dev_open_left:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----打开后代设备左手接口-----------------------------------------------------
//功能: 打开后代设备的左手接口，搜索ancestor设备的整个子设备树，找到名称与
//      scion_name匹配的资源结点，然后从泛设备句柄池中分配控制块,把左手接口指针
//      赋值给句柄后,返回该句柄指针。
//参数: ancestor，被打开设备的祖先设备。
//      scion_name,设备名字符串,包含路径名,
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备(含经过阻塞后打开)返回设备句柄,否则返回NULL.
//-----------------------------------------------------------------------------
struct  dev_handle *dev_open_left_scion(struct  dev_handle *ancestor,
                                      char *scion_name, uint32_t timeout)
{
    struct  pan_device     *pan;
    struct  dev_handle     *handle;
    struct  dev_handle     *result;
    if(ancestor == NULL)
        return NULL;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return NULL;
    //在设备树中搜索设备
    pan = (struct pan_device*)rsc_search(&ancestor->dev_interfase->node,
                                              scion_name);
    if(pan == NULL)     //如果没有找到子设备,返回空
    {
        result = NULL;
        goto end_of_dev_open_left_scion;
    }
    if(semp_pend(pan->left_semp,timeout)==false)//获取信号量
    {
        result = NULL;
        goto end_of_dev_open_left_scion;
    }
    handle = mb_malloc(pg_device_handle_pool,0);  //从池中分配句柄内存块
    if(handle != NULL)
    {//成功分配句柄
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_left;              //我是左手接口句柄
        if(pg_event_running->held_device == NULL)   //事件还没有打开过设备
        {
            handle->next = handle;      //句柄指针自成双向循环链表
            handle->previous = handle;
            pg_event_running->held_device = handle; //事件指针指向设备链表
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result =  handle;
    }else
    {
        semp_post(pan->left_semp);
        result =  NULL;
    }
end_of_dev_open_left_scion:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----快速打开设备左手接口-----------------------------------------------------
//功能: 按快速打开设备左手接口,只适用于曾经打开且一直没有被删除的设备,因为不用
//      按设备名查找设备,速度很快
//参数: handle,待打开的设备句柄,实际上存的是泛设备指针,
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备则返回true,否则返回flase.
//备注: 如果有多个用户共享本设备,为了充分共享,每次使用完毕后应该关闭设备,当需要
//      再次用同一句柄打开时,可以使用本函数.
//-----------------------------------------------------------------------------
bool_t dev_open_left_again(struct  dev_handle *handle,uint32_t timeout)
{
    struct  pan_device     *pan;
    bool_t result;
    if (handle == NULL) //句柄空
        return false;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return false;

    pan = (struct  pan_device *)handle;    //句柄里存的实际是泛设备指针
    if(semp_pend(pan->left_semp,timeout)==false)//获取信号量
    {
        result = false;
        goto end_of_dev_open_left_again;
    }
    handle = mb_malloc(pg_device_handle_pool,0);  //分配泛设备句柄控制块
    if(handle == NULL)
    {//分配不成功
        handle = (struct  dev_handle *)pan;    //恢复句柄指针
        result = false;
    }else
    {
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_right;              //我是左手接口句柄
        if(pg_event_running->held_device == NULL)   //事件还没有打开过设备
        {
            handle->next = handle;
            handle->previous = handle;
            pg_event_running->held_device = handle;
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result = true;
    }
end_of_dev_open_left_again:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----打开设备右手接口---------------------------------------------------------
//功能: 根据设备名打开设备的右手接口，搜索整个设备资源树，找到名称与name匹配的
//      资源结点，然后从泛设备句柄池中分配控制块,把右手接口指针赋值给句柄后,
//      返回该句柄指针。
//参数: name,设备名字符串,包含路径名，但不包含'dev\'这4个字符
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备返回设备句柄,否则返回NULL.
//-----------------------------------------------------------------------------
struct  dev_handle *dev_open_right(char *name,uint32_t timeout)
{
    struct  pan_device     *pan;
    struct  dev_handle     *handle;
    struct  dev_handle     *result;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return NULL;
    //在设备树中搜索设备
    pan = (struct  pan_device *)rsc_search(&pg_device_rsc_tree->node,name);
    if(pan == NULL)     //如果没有找到name设备,返回空
    {
        result = NULL;
        goto end_of_dev_open_right;
    }
    if(semp_pend(pan->right_semp,timeout)==false)//获取信号量,这是保护设备本身的
    {
        result = NULL;
        goto end_of_dev_open_right;
    }
    //在分配内存之前判断信号量，可避免在没有信号的情况下长时间占用内存
    handle = mb_malloc(pg_device_handle_pool,0);  //从池中分配句柄内存块
    if(handle != NULL)
    {//成功分配句柄
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_right;              //我是右手接口句柄
        if(pg_event_running->held_device == NULL)   //事件还没有打开过设备
        {
            handle->next = handle;      //句柄指针自成双向循环链表
            handle->previous = handle;
            pg_event_running->held_device = handle; //事件指针指向设备链表
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result = handle;
    }else
        result = NULL;

end_of_dev_open_right:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----打开后代设备右手接口-----------------------------------------------------
//功能: 打开后代设备的右手接口，搜索ancestor设备的整个子设备树，找到名称与
//      scion_name匹配的资源结点，然后从泛设备句柄池中分配控制块,把右手接口指针
//      赋值给句柄后,返回该句柄指针。
//参数: ancestor，被打开设备的祖先设备。
//      scion_name,设备名字符串,包含路径名,
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备(含经过阻塞后打开)返回设备句柄,否则返回NULL.
//-----------------------------------------------------------------------------
struct  dev_handle *dev_open_right_scion(struct  dev_handle *ancestor,
                                      char *scion, uint32_t timeout)
{
    struct  pan_device     *pan;
    struct  dev_handle     *handle;
    struct  dev_handle     *result;
    if(ancestor == NULL)
        return NULL;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return NULL;
    //在设备树中搜索设备
    pan = (struct pan_device*)rsc_search(&ancestor->dev_interfase->node,
                                              scion);
    if(pan == NULL)     //如果没有找到子设备,返回空
    {
        result = NULL;
        goto end_of_dev_open_right_scion;
    }
    if(semp_pend(pan->right_semp,timeout)==false)//获取信号量
    {
        result = false;
        goto end_of_dev_open_right_scion;
    }
    handle = mb_malloc(pg_device_handle_pool,0);  //从池中分配句柄内存块
    if(handle != NULL)
    {//成功分配句柄
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_right;              //我是左手接口句柄
        if(pg_event_running->held_device == NULL)  //事件还没有打开过设备
        {
            handle->next = handle;      //句柄指针自成双向循环链表
            handle->previous = handle;
            pg_event_running->held_device = handle; //事件指针指向设备链表
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result =  handle;
        }else
        {
            semp_post(pan->right_semp);
            result =  NULL;
        }
end_of_dev_open_right_scion:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----快速打开设备右手接口-----------------------------------------------------
//功能: 按快速打开设备右手接口,只适用于曾经打开且一直没有被删除的设备,因为不用
//      按设备名查找设备,速度很快
//参数: handle,待打开的设备句柄,实际上存的是泛设备指针,
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: 成功打开设备则返回true,否则返回flase.
//备注: 如果有多个用户共享本设备,为了充分共享,每次使用完毕后应该关闭设备,当需要
//      再次用同一句柄打开时,可以使用本函数.
//-----------------------------------------------------------------------------
bool_t dev_open_right_again(struct  dev_handle *handle,uint32_t timeout)
{
    struct  pan_device     *pan;
    bool_t result;
    if (handle == NULL) //句柄空
        return false;
    if( ! mutex_pend(&tg_dev_mutex,timeout))    //这是保护设备树的互斥量
        return false;

    pan = (struct  pan_device *)handle;    //句柄里存的实际是泛设备指针
    if(semp_pend(pan->right_semp,timeout)==false)//获取信号量
    {
        result = false;
        goto end_of_dev_open_right_again;
    }
    handle = mb_malloc(pg_device_handle_pool,0);  //分配泛设备句柄控制块
    if(handle == NULL)
    {//分配不成功
        handle = (struct  dev_handle *)pan;    //恢复句柄指针
        result = false;
    }else
    {
        handle->dev_interfase = pan;
        handle->owner = pg_event_running;
        handle->iam = enum_iam_right;              //我是右手接口句柄
        if(pg_event_running->held_device == NULL)  //事件还没有打开过设备
        {
            handle->next = handle;
            handle->previous = handle;
            pg_event_running->held_device = handle;
        }else                                       //事件已经有打开的设备
        {
            //以下把新设备插入到事件打开的设备队列头部
            handle->next = pg_event_running->held_device;
            handle->previous = pg_event_running->held_device->previous;
            pg_event_running->held_device->previous->next = handle;
            pg_event_running->held_device->previous = handle;
        }
        result = true;
    }
end_of_dev_open_right_again:
    mutex_post(&tg_dev_mutex);
    return result;
}

//----关闭设备左手接口---------------------------------------------------------
//功能: 关闭设备左手接口,执行如下操作:
//      1.把设备的用户数减1,
//      2.从事件的held_device队列中删除泛设备句柄
//      3.释放句柄
//      4.句柄指针指向原打开设备的泛设备控制块,以备快速打开
//参数: handle,被关闭的设备句柄
//返回: 成功关闭返回被关闭设备的设备控制块地址,否则返回NULL
//-----------------------------------------------------------------------------
struct dev_handle * dev_close_left(struct  dev_handle *handle)
{
    struct pan_device *pan;
    if (handle == NULL)
        return NULL;
    if(handle->iam != enum_iam_left)
        return NULL;
    mutex_pend(&tg_dev_mutex,cn_timeout_forever);
    pan = handle->dev_interfase;
    if(handle->previous == handle)
    {//事件的held_device队列中只有一个设备
        handle->owner->held_device = NULL;
    }else
    {//held_device队列中有多个设备,把本设备从队列中取出.
        handle->next->previous = handle->previous;
        handle->previous->next = handle->next;
        if(handle->owner->held_device == handle)
            handle->owner->held_device = handle->next;
    }
    mutex_post(&tg_dev_mutex);
    mb_free(pg_device_handle_pool,handle);  //释放内存
    semp_post(pan->left_semp);
    //返回原打开设备的泛设备控制块,以备快速打开
    return (struct  dev_handle *)pan;
}

//----关闭设备右手接口---------------------------------------------------------
//功能: 关闭设备右手接口,执行如下操作:
//      1.把设备的用户数减1,
//      2.从事件的held_device队列中删除泛设备句柄
//      3.释放句柄
//      4.句柄指针指向原打开设备的泛设备控制块,以备快速打开
//参数: handle,被关闭的设备句柄
//返回: 成功关闭返回被关闭设备的设备控制块地址,否则返回NULL
//-----------------------------------------------------------------------------
struct dev_handle *dev_close_right(struct  dev_handle *handle)
{
    struct  pan_device *pan;
    if (handle == NULL)
        return NULL;
    if(handle->iam != enum_iam_right)
        return NULL;
    mutex_pend(&tg_dev_mutex,cn_timeout_forever);
    pan = handle->dev_interfase;
    if(handle->previous == handle)
    {
        handle->owner->held_device = NULL;
    }else
    {
        handle->next->previous = handle->previous;
        handle->previous->next = handle->next;
        if(handle->owner->held_device == handle)
            handle->owner->held_device = handle->next;
    }
    mutex_post(&tg_dev_mutex);
    mb_free(pg_device_handle_pool,handle);
    semp_post(pan->left_semp);
    //返回原打开设备的泛设备控制块,以备快速打开
    return (struct  dev_handle *)pan;
}

//----读设备函数---------------------------------------------------------------
//功能: 调用设备的读函数读取数据.
//参数: handle,设备句柄
//      其他参数:由设备开发者定义
//返回: 由设备开发者定义，推荐是实际读取的数据长度.
//-----------------------------------------------------------------------------
ptu32_t dev_read(struct  dev_handle *handle,ptu32_t src_buf,
                ptu32_t des_buf,ptu32_t len)
{
    if (handle == NULL)
        return 0;
    if(handle->iam == enum_iam_left)
        return (handle->dev_interfase->left_hand.io_read
                            (handle,src_buf,des_buf,len));
    else if(handle->iam == enum_iam_right)
        return (handle->dev_interfase->right_hand.io_read
                            (handle,src_buf,des_buf,len));
    return 0;
}

//----写设备函数---------------------------------------------------------------
//功能: 调用设备的写函数写入数据.
//参数: handle,设备句柄
//      其他参数:由设备开发者定义
//返回: 由设备开发者定义，推荐是实际写入的数据长度.
//-----------------------------------------------------------------------------
ptu32_t dev_write(struct  dev_handle *handle,ptu32_t src_buf,
                ptu32_t des_buf,ptu32_t len)
{
    if (handle == NULL)
        return 0;
    if(handle->iam == enum_iam_left)
        return (handle->dev_interfase->left_hand.io_write
                            (handle,src_buf,des_buf,len));
    else if(handle->iam == enum_iam_right)
        return (handle->dev_interfase->right_hand.io_write
                            (handle,src_buf,des_buf,len));
    return 0;
}

//----控制设备函数-------------------------------------------------------------
//功能: 调用设备的控制函数.
//参数: handle,设备句柄
//      其他参数:由设备开发者定义
//返回: 由设备开发者定义
//-----------------------------------------------------------------------------
ptu32_t dev_ctrl(struct  dev_handle *handle,uint32_t cmd,
                    ptu32_t data1,ptu32_t data2)
{
    if (handle == NULL)
        return 0;
    if(handle->iam == enum_iam_left)
        return (handle->dev_interfase->left_hand.io_ctrl
                            (handle,cmd,data1,data2));
    else if(handle->iam == enum_iam_right)
        return (handle->dev_interfase->right_hand.io_ctrl
                            (handle,cmd,data1,data2));
    return 0;
}


//----清理设备-----------------------------------------------------------------
//功能: 关闭一个设备队列中的所有设备，本函数用于事件完成后清理泄漏的资源，如果
//      一个事件线程打开设备没有关闭，直接结束可能引起资源泄漏，本函数由
//      y_event_done函数调用
//参数: handle,设备队列指针，双向循环链表
//返回: 无
//-----------------------------------------------------------------------------
void dev_cleanup(struct  event_script *event)
{
    if(event == NULL)
        return;
    while(event->held_device != NULL)
    {
        if(event->held_device->iam == enum_iam_left)
            dev_close_left(event->held_device);
        else
            dev_close_right(event->held_device);
    };
}

