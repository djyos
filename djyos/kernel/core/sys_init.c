//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块: 硬件定时器
//作者：lst
//版本：V1.0.0
//文件描述: 系统初始化文件
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-04-24
//   作者: lst
//   新版本号：V1.0.0
//   修改说明: 原始版本，本文件内容原来和加载器放在一起
//------------------------------------------------------
#include "inc_os.h"

//----操作系统内核组件初始化---------------------------------------------------
//功能：初始化操作系统外围模块，包括所有内核设备
//参数：无
//返回：无
//---------------------------------------------------------------------------
void os_knl_module_init(void)
{
    module_init_rsc1();
    module_init_lock1();
    module_init_rsc2();
    module_init_memb();
    module_init_lock2();
    module_init_wdt();
    module_driver_init();
}

//----操作系统外围组件初始化---------------------------------------------------
//功能：外围组件属于可裁减的组件，在这里逐个调用组件的module_init_modulename函数
//      初始化独立模块，si模式在此初始化所有外围组件，dlsp模式和mp模式的外围组件
//      可能在这里，也可能从文件或者通信口加载和初始化。
//参数：无
//返回：无
//---------------------------------------------------------------------------
void os_ext_module_init(void)
{
    module_init_keyboard();
#if cfg_djyfs == 1
    module_init_djyfs();
#if cfg_djyfs_flash == 1
    module_init_DFFSD();
#endif
#endif
}

//----dlsp模式加载程序--------------------------------------------------------
//功能：打开autoexec.bat文件逐行执行，如果命令文件里有main函数则是应用程序，执行
//      之，如果没有则搜索module_init_modulename函数，执行之，两个函数都没有则
//      只加载，不执行里面的代码。
//参数: 无。
//返回: 无。
//----------------------------------------------------------------------------
void dlsp_module_loader(void)
{
}

//----系统启动程序-----------------------------------------------------
//功能：执行操作系统加载，模块(设备)初始化、用户初始化以及启动操作系统
//参数：无
//返回：无
//---------------------------------------------------------------------------
void start_sys(void)
{
    //自此可以调用malloc族函数，用准静态堆分配算法，若要释放内存，要求严格配对
    //调用malloc-free函数，或者不调用
    module_init_heap_static();
    __djy_set_delay();
    __djy_init_sys();
    os_knl_module_init();
    //从这里开始，si、dlsp、mp模式有所不同，这里是si模式的代码
    os_ext_module_init();
    app_init();
    module_init_shell();
    module_init_heap_dynamic();  //自此malloc函数执行块相联算法
    __djy_start_os();
}

