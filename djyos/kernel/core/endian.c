//----------------------------------------------------
//Copyright (C), 2004-2009,  luoshitian.
//版权所有 (C), 2004-2009,   lst.
//所属模块:公共函数
//作者：lst
//版本：V1.0.0
//文件描述:公共函数
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "inc_os.h"

//----读取一个小端16位数据-----------------------------------------------------
//功能: 从小端格式的存储器中读出一个16位数据。
//参数: buf，缓冲区
//      index，偏移量，即第几个16位数
//返回: 读取的16位数据
//----------------------------------------------------------------------------
uint32_t __pick_little_16bit(uint8_t *buf,uint32_t index)
{
    //即使端格相同，也不能使用 return *(uint16_t *)buf简化，
    //因为不能保证buf是一个对齐了的地址
    return buf[2*index] + (buf[2*index+1]<<8);
}

//----读取一个小端32位数据-----------------------------------------------------
//功能: 从小端格式的存储器中读出一个32位数据。
//参数: buf，缓冲区
//      index，偏移量，即第几个32位数
//返回: 读取的32位数据
//----------------------------------------------------------------------------
uint32_t __pick_little_32bit(uint8_t *buf,uint32_t index)
{
    //即使端格相同，也不能使用 return *(uint32_t *)buf简化，
    //因为不能保证buf是一个对齐了的地址
    return buf[4*index] + (buf[4*index+1]<<8)
            + (buf[4*index+2]<<16) + (buf[4*index+3]<<24);
}

//----填充一个小端16位数据-----------------------------------------------------
//功能: 把一个16位数据填充到小端格式的存储器中。
//参数: buf，缓冲区
//      index，偏移量，即第几个16位数
//      data，填充的数据
//返回: 无
//----------------------------------------------------------------------------
void __fill_little_16bit(uint8_t *buf,uint32_t index,uint32_t data)
{
    buf[2*index+0] = (uint8_t)data;
    buf[2*index+1] = (uint8_t)(data>>8);
}

//----填充一个小端32位数据-----------------------------------------------------
//功能: 把一个32位数据填充到小端格式的存储器中。
//参数: buf，缓冲区
//      index，偏移量，即第几个32位数
//      data，填充的数据
//返回: 无
//----------------------------------------------------------------------------
void __fill_little_32bit(uint8_t *buf,uint32_t index,uint32_t data)
{
    buf[4*index+0] = (uint8_t)data;
    buf[4*index+1] = (uint8_t)(data>>8);
    buf[4*index+2] = (uint8_t)(data>>16);
    buf[4*index+3] = (uint8_t)(data>>24);
}

//----读取一个大端16位数据-----------------------------------------------------
//功能: 从大端格式的存储器中读出一个16位数据。
//参数: buf，缓冲区
//      index，偏移量，即第几个16位数
//返回: 读取的16位数据
//----------------------------------------------------------------------------
uint32_t __pick_big_16bit(uint8_t *buf,uint32_t index)
{
    //即使端格相同，也不能使用 return *(uint16_t *)buf简化，
    //因为不能保证buf是一个对齐了的地址
    return buf[2*index+1] + (buf[2*index]<<8);
}

//----读取一个大端32位数据-----------------------------------------------------
//功能: 从大端格式的存储器中读出一个32位数据。
//参数: buf，缓冲区
//      index，偏移量，即第几个32位数
//返回: 读取的32位数据
//----------------------------------------------------------------------------
uint32_t __pick_big_32bit(uint8_t *buf,uint32_t index)
{
    //即使端格相同，也不能使用 return *(uint32_t *)buf简化，
    //因为不能保证buf是一个对齐了的地址
    return buf[4*index+3] + (buf[4*index+2]<<8)
            + (buf[4*index+1]<<16) + (buf[4*index]<<24);
}

//----填充一个大端16位数据-----------------------------------------------------
//功能: 把一个16位数据填充到大端格式的存储器中。
//参数: buf，缓冲区
//      index，偏移量，即第几个16位数
//      data，填充的数据
//返回: 无
//----------------------------------------------------------------------------
void __fill_big_16bit(uint8_t *buf,uint32_t index,uint32_t data)
{
    buf[2*index+1] = (uint8_t)data;
    buf[2*index+0] = (uint8_t)(data>>8);
}

//----填充一个大端32位数据-----------------------------------------------------
//功能: 把一个32位数据填充到大端格式的存储器中。
//参数: buf，缓冲区
//      index，偏移量，即第几个32位数
//      data，填充的数据
//返回: 无
//----------------------------------------------------------------------------
void __fill_big_32bit(uint8_t *buf,uint32_t index,uint32_t data)
{
    buf[4*index+3] = (uint8_t)data;
    buf[4*index+2] = (uint8_t)(data>>8);
    buf[4*index+1] = (uint8_t)(data>>16);
    buf[4*index+0] = (uint8_t)(data>>24);
}

