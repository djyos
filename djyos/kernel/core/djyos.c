//----------------------------------------------------
//Copyright (C), 2004-2009,  lst.
//版权所有 (C), 2004-2009,   lst.
//所属模块:核心模块
//作者：lst
//版本：V1.1.0
//文件描述:事件类型、事件管理以及多事件调度相关的代码全在这里了。
//其他说明:
//修订历史:
//2. 日期: 2009-03-18
//   作者: lst
//   新版本号: V1.1.0
//   修改说明:
//   1、根据无为小僧（曾波）发现的隐患，事件类型的名字如果用指针索引的话，如果
//      用户调用y_evtt_regist时使用的是局部数组，将存在保存名字的内存被释放的危
//      险。改为数组存储，最长31字符，在struct event_type结构中把evtt_name由指针
//      改为数组，修改涉及下列函数：
//          __djy_init_sys、y_evtt_regist、y_evtt_unregist、y_get_evtt_id
//   2、在y_evtt_unregist函数中释放虚拟机处发现一个bug，存在访问非法内存的危险，
//      改之。
//   3、y_evtt_done_sync函数中处理同步队列处存在严重bug，可能导致访问非法内存，
//      且使同步队列混乱，改之。
//1. 日期: 2009-01-04
//   作者: lst
//   新版本号: V1.0.0
//   修改说明: 原始版本
//------------------------------------------------------
#include "inc_os.h"
#include "gpio.h"
#include <string.h>

struct process_vm *pg_my_process;
//为cn_events_limit条事件控制块分配内存
static struct  event_script tg_event_table[cn_events_limit];
//为cn_evtts_limit个事件类型控制块分配内存
static struct event_type tg_evtt_table[cn_evtts_limit];
static struct  event_script  *pg_event_free; //空闲链表头,不排序
//轮转调度时间片，0表示禁止轮转调度，默认1，RRS = "round robin scheduling"缩写。
static uint32_t  u32g_RRS_slice = 1;

struct  event_script  *pg_event_ready;      //就绪队列头
struct  event_script  *pg_event_running;    //当前正在执行的事件
static struct  event_script  *pg_event_delay;      //闹钟同步队列表头
static uint32_t  u32g_running_start;        //当前运行中事件的开始执行时间.
uint32_t  u32g_os_ticks;             //操作系统运行ticks数
extern volatile uint32_t    u32g_delay_10uS;
uint32_t u32g_ns_of_u32for; //for(i=j;i>0;i--);语句，i和j都是u32型，每循环纳秒数
uint32_t u32g_ns_of_u16for; //for(i=j;i>0;i--);语句，i和j都是u16型，每循环纳秒数
uint32_t u32g_ns_of_u8for;  //for(i=j;i>0;i--);语句，i和j都是u8型，每循环纳秒数

//----10微秒级延时-------------------------------------------------------------
//功能：利用循环实现的微秒分辨率延时，调用__djy_set_delay函数后才能使用本函数，
//      否则在不同优化级别和不同编译器下,延时数不同.
//参数：time，延时时间，单位为10微秒,最多655毫秒。
//返回：无
//注意：不建议使用此函数做太长延时，长延时请使用函数 djy_timer_sync,
//-----------------------------------------------------------------------------
void djy_delay_10us(volatile uint16_t time)
{
    volatile uint32_t i;
    for(; time > 0; time--)
        for(i = u32g_delay_10uS; i >0 ; i--);
}

//----实时串长度---------------------------------------------------------------
//功能: strlen函数的执行时间不可控，不宜在实时环境中使用。
//参数: s，被测字符串
//      over，最长检测长度,over=0则不限长度，与strlen函数无异。
//返回: 串长度，在over范围内没有检测到串结束则返回-1
//-----------------------------------------------------------------------------
sint32_t djy_rtstrlen(const char *s,uint32_t over)
{
    uint32_t len;
    if(over == 0)
        return strlen(s);
    else
    {
        for(len = 0; len < over; len++)
        {
            if(s[len] =='\0')
            {
                return len;
            }
        }
        return cn_limit_uint32;
    }
}


//以下为多任务管理函数

//----tick中断-----------------------------------------------------------------
//功能：为操作系统产生时钟源，并比较理闹钟同步队列中的事件，如果有闹铃时间到的
//      事件，就把他们激活的ready队列中。如果时间片轮转调度被允许，还要看是否要
//      执行轮转。ticks被设置为异步信号。
//参数：line，ticks中断的中断线号，实际上不用。
//返回：无
//-----------------------------------------------------------------------------
uint32_t  __djy_isr_tick(ufast_t line)
{
    struct  event_script *pl_ecb,*pl_ecbp,*pl_ecbn;
    u32g_os_ticks++;    //系统时钟,4294967296次后溢出,将环绕
    if(pg_event_delay != NULL)
    {
        pl_ecb = pg_event_delay;
        while(1)
        {
            if(pl_ecb->delay_end == u32g_os_ticks)
            {
                if(pl_ecb->sync_head != NULL)   //事件在某同步队列中
                {
                    if(*pl_ecb->sync_head == pl_ecb)//本事件是该同步队列的首事件
                    {
                        if(pl_ecb->multi_next == pl_ecb)    //队列中只有一个事件
                        {
                            *pl_ecb->sync_head = NULL;
                            pl_ecb->multi_next = NULL;
                            pl_ecb->multi_previous = NULL;
                        }else                   //队列中有多个事件
                        {
                            //头指针指向下一个事件
                            *pl_ecb->sync_head = pl_ecb->multi_next;
                            pl_ecb->multi_previous->multi_next
                                                = pl_ecb->multi_next;
                            pl_ecb->multi_next->multi_previous
                                                = pl_ecb->multi_previous;
                        }
                    }else           //本事件不是首事件
                    {
                        pl_ecb->multi_previous->multi_next
                                            = pl_ecb->multi_next;
                        pl_ecb->multi_next->multi_previous
                                            = pl_ecb->multi_previous;
                    }
                    pl_ecb->sync_head = NULL;   //事件头指针置空
                }
                if(pl_ecb->next == pl_ecb)      //这是闹钟同步队列最后一个结点.
                {
                    pg_event_delay = NULL;
                    pl_ecb->last_status.all = pl_ecb->event_status.all;
                    pl_ecb->event_status.bit.event_delay = 0;
                    pl_ecb->event_status.bit.wait_overtime = 0;
                    __djy_event_ready(pl_ecb);
                    break;
                }else
                {
                    pg_event_delay = pl_ecb->next;
                    pl_ecb->next->previous = pl_ecb->previous;
                    pl_ecb->previous->next = pl_ecb->next;
                    pl_ecb->last_status.all = pl_ecb->event_status.all;
                    pl_ecb->event_status.bit.event_delay = 0;
                    pl_ecb->event_status.bit.wait_overtime = 0;
                    __djy_event_ready(pl_ecb);
                    pl_ecb = pg_event_delay;
                }
            }else
                break;
        }
    }
    pg_event_running->consumed_time++;
    //下面处理时间片轮转调度.
    //因在开异步信号(允许调度)才可能进入__djy_isr_tick，即使因闹钟响导致新事件加入，
    //也不影响pg_event_running是同优先级首事件的性质，但可能不等于pg_event_ready
    if(u32g_RRS_slice != 0)      //允许轮转调度
    {
        if( (pg_event_running->prio == pg_event_running->next->prio)
                    &&(pg_event_running != pg_event_running->next) )
        {//该优先级有多个事件，看轮转时间是否到
            if(pg_event_running->consumed_time%u32g_RRS_slice == 0) //时间片用完
            {
                //先处理首事件链表
                pl_ecb = pg_event_running->next;
                pl_ecbn = pg_event_running->multi_next;
                pl_ecbp = pg_event_running->multi_previous;
                if(pg_event_ready == pg_event_running)
                    pg_event_ready = pl_ecb;
                pl_ecbp->multi_next = pl_ecb;
                pl_ecb->multi_previous = pl_ecbp;
                pl_ecbn->multi_previous = pl_ecb;
                pl_ecb->multi_next = pl_ecbn;
                pg_event_running->multi_next = NULL;
                pg_event_running->multi_previous = NULL;

                //再处理就绪链表
                pl_ecbn = pg_event_ready->multi_next;
                pl_ecbp = pg_event_running->previous;
                pl_ecbp->next = pg_event_ready;
                pg_event_ready->previous = pl_ecbp;
                pg_event_running->previous = pl_ecbn->previous;
                pg_event_running->next = pl_ecbn;
                pl_ecbn->previous->next = pg_event_running;
                pl_ecbn->previous = pg_event_running;
            }
        }
    }
    return 0;
}

//----登记错误信息--------------------------------------------------------------
//功能：把系统错误信息登记到一个环形缓冲区里
//参数：error_type，错误类型
//      text，文字说明
//返回：无
//还需添加文本串的功能，---db
//-----------------------------------------------------------------------------
void djy_error_login(uint32_t error_type,const char *text)
{
    pg_event_running->error_no = error_type;
}

//----提取最后一条错误信息-----------------------------------------------------
//功能：把系统错误信息登记到一个环形缓冲区里
//参数：text，返回该错误的文字说明
//返回：错误号
//还需添加文本串和时间的功能，时间使用ticks数---db
//-----------------------------------------------------------------------------
uint32_t djy_get_last_error(char *text)
{
    return pg_event_running->error_no;
}

//----从ready队列中取出一个事件------------------------------------------------
//功能: 把一个事件从ready队列中取出，并使ready队列重新连接
//参数: event，待取出的事件指针
//返回: 无
//注: 调用者请保证在异步信号(调度)被禁止的情况下调用本函数
//-----------------------------------------------------------------------------
void __djy_cut_ready_event(struct event_script *event)
{
    struct event_script *pl_ecb;
    if(event != pg_event_ready)         //event不是ready队列头
    {
        if(event->multi_next == NULL)   //不是相应优先级的首事件
        {
            event->next->previous = event->previous;
            event->previous->next = event->next;
        }else                           //是相应优先级的首事件
        {
            pl_ecb = event->next;
            event->next->previous = event->previous;
            event->previous->next = event->next;
            if(pl_ecb->prio == event->prio)     //相应优先级不止一个事件
            {
                event->multi_previous->multi_next = pl_ecb;
                pl_ecb->multi_previous = event->multi_previous;
                event->multi_next->multi_previous = pl_ecb;
                pl_ecb->multi_next = event->multi_next;
            }else                               //相应优先级只有一个事件
            {
                //pl_ecb即event->multi_next.
                pl_ecb->multi_previous = event->multi_previous;
                event->multi_previous->multi_next = pl_ecb;
            }
        }
    }else                               //event是ready队列头
    {
        pg_event_ready = event->next;
        pl_ecb = event->next;
        event->next->previous = event->previous;
        event->previous->next = event->next;
        if(pl_ecb->prio == event->prio)     //相应优先级不止一个事件
        {
            event->multi_previous->multi_next = pl_ecb;
            pl_ecb->multi_previous = event->multi_previous;
            event->multi_next->multi_previous = pl_ecb;
            pl_ecb->multi_next = event->multi_next;
        }else                               //相应优先级只有一个事件
        {
            //pl_ecb即event->multi_next.
            pl_ecb->multi_previous = event->multi_previous;
            event->multi_previous->multi_next = pl_ecb;
        }
    }
    event->event_status.bit.event_ready = 0;
}

//----设置轮转调度时间片-------------------------------------------------------
//功能: djyos总所有参与轮转调度的事件时间片都是相同的，在这里设置。如果设为0，
//      表示不允许时间片轮转。
//参数: slices，新的轮转调度时间片，毫秒数，将被向上取整为整数个ticks时间
//返回: 无
//-----------------------------------------------------------------------------
void djy_set_RRS_slice(uint32_t slices)
{
#if (32 > cn_cpu_bits)
    //若处理器字长＜32位,需要多个周期才能更新u32g_RRS_slice,该过程不能被时钟中断打断.
    int_save_asyn_signal( );
#endif

    u32g_RRS_slice =(slices + cn_tick_ms -1)/cn_tick_ms;

#if (32 > cn_cpu_bits)
    //若处理器字长＜32位,需要多个周期才能更新u32g_RRS_slice,该过程不能被时钟中断打断.
    int_restore_asyn_signal( );
#endif
}

//----查询轮转调度时间片-------------------------------------------------------
//功能: djyos总所有参与轮转调度的事件时间片都是相同的，可用本函数查询。
//参数: 无
//返回: 当前时间片长度，毫秒数。
//-----------------------------------------------------------------------------
uint32_t djy_get_RRS_slice(void)
{
    uint32_t temp;
#if (32 > cn_cpu_bits)
    //处理器字长＜32位,需要多个周期才能读取u32g_RRS_slice,该过程不能被时钟中断打断.
    int_save_asyn_signal( );
#endif

    temp = u32g_RRS_slice * cn_tick_ms;

#if (32 > cn_cpu_bits)
    //处理器字长＜32位,需要多个周期才能读取u32g_RRS_slice,该过程不能被时钟中断打断.
    int_restore_asyn_signal( );
#endif
    return temp * cn_tick_ms;
}

//----查找可运行线程----------------------------------------------------------
//功能: 在 pg_event_ready队列中获得一个可以运行的线程.
//      1.从pg_event_ready队列头开始查找,如果该事件还没有连接线程,则试图创建之.
//      2.如果不能建立,肯定是因内存不足，则把该事件放到内存等待表中,继续往下查找.
//      3.如此反复,直到找到一个可以运行的线程.然后把pg_event_ready指针指向该事件
//参数: 无
//返回: 无
//备注: 本函数由操作系统调用,调用前保证关异步信号。
//      由于系统服务事件总是ready,所以总是能找到可以运行的线程.
//----------------------------------------------------------------------------
void __djy_select_event_to_run(void)
{
    struct  event_script *pl_ecb;
    struct  event_type *pl_evtt;  //被操作的事件的类型指针

    while(pg_event_ready->vm == NULL)
    {
        pl_evtt =& tg_evtt_table[pg_event_ready->evtt_id];
        if(pl_evtt->my_free_vm != NULL)     //该事件类型有空闲的线程,直接使用
        {
            pg_event_ready->vm = pl_evtt->my_free_vm;
            pl_evtt->my_free_vm = pl_evtt->my_free_vm->next;
        }else       //该事件类型没有空闲的线程,试图创建之
        {
            pg_event_ready->vm = __create_thread(pl_evtt);  //创建线程
            if(pg_event_ready->vm == NULL)                  //创建线程失败
            {
                pl_ecb = pg_event_ready;
                __djy_cut_ready_event(pl_ecb);
                __m_wait_memory(pl_ecb);
            }else                                           //成功创建线程
            {
                pl_evtt->vpus++;
                //取此前被重复pop的次数
                pg_event_ready->repeats = pl_evtt->repeats;
            }
        }
    }
}

//----创建进程虚拟机-----------------------------------------------------------
//功能: 创建进程虚拟机。
//参数: 无
//返回: 无
//备注: 这只是占一个位而已，在mp模式才有实用价值
//-----------------------------------------------------------------------------
void djy_create_process_vm(void)
{
    static struct process_vm my_process;
    pg_my_process = &my_process;
}

//----事件调度-----------------------------------------------------------------
//功能：剥夺running事件的cpu,把cpu交给ready队列的第一个事件.如果ready队列的第一
//      个事件尚未拥有线程,则建立之.建立线程时如果内存不足,则把这个事件放到
//      内存等待链表中,ready指针指向ready队列的下一个事件,依次推之,直到找到一个
//      线程虚拟机指针非空或者成功创建线程虚拟机的事件.
//参数：无
//返回：true = 成功切换，false = 未切换直接返回
//备注：1.本函数由操作系统调用
//      2.djyos系统认为,用户禁止中断就是为了能够连续执行,或者保护临界资源.
//      djyos中异步信号相当于高优先级线程,所以全局和异步信号禁止状态
//      下,是不允许事件调度的.
//      3.实时中断是否禁止,与调度无关.
//      4.本函数由操作系统调用,调用方保证调用时中断是允许的,不检查.
//      5.由于最低优先级的系统服务事件总是ready,因此本函数总是能够找到目标事件
//-----------------------------------------------------------------------------
bool_t __djy_schedule(void)
{
    struct  event_script *event;

    if(tg_int_global.nest_asyn_signal != 0)
        return false;
    int_save_asyn_signal();   //在上下文切换期间不能发生异步信号中断
    __djy_select_event_to_run();
    if(pg_event_ready != pg_event_running)
    {//当running事件仍在ready队列中,且内存不足以建立新虚拟机时,可能会出现优先
     //级高于running的事件全部进入内存等待队列的可能.此时执行else子句.
        u32g_running_start=djy_get_time();
        event = pg_event_running;
        pg_event_running=pg_event_ready;
        __asm_switch_context(pg_event_ready->vm ,event->vm);
    }else
    {//优先级高于running的事件全部进入内存等待队列,下一个要处理的事件就是
     //running事件,无须执行任何操作
        int_restore_asyn_signal();
        return false;
    }
    return true;
}

//----中断内执行的事件调度------------------------------------------------------
//功能：剥夺running事件的cpu,把cpu交给ready队列的第一个事件.如果ready队列的第一
//      个事件尚不拥有线程,则创建之.建立线程时如果内存不足,则把这个事件放到
//      内存等待链表中,ready指针指向ready队列的下一个事件,依次推之,直到找到一个
//      线程虚拟机指针非空或者成功创建线程虚拟机的事件.
//参数：无
//返回：无
//备注：本函数由操作系统在异步信号引擎返回前调用
//      由于最低优先级的y_idle_service事件总是ready,因此总是能够找到调度对象的.
//-----------------------------------------------------------------------------
void __djy_schedule_asyn_signal(void)
{
    struct  event_script *event;

    __djy_select_event_to_run();
    if(pg_event_ready != pg_event_running)
    {//当running事件仍在ready队列中,且内存不足以建立新虚拟机时,可能会出现优先
     //级高于running的事件全部进入内存等待队列的可能.此时执行else子句.
        u32g_running_start=djy_get_time();
        event=pg_event_running;
        pg_event_running=pg_event_ready;
        __asm_switch_context_int(pg_event_ready->vm,event->vm);
    }else
    {//优先级高于running的事件全部进入内存等待队列,下一个要处理的事件就是
     //running事件,无须执行任何操作
    }
    return;
}

//----登记事件类型------------------------------------------------------------
//功能：登记一个事件类型到系统中,事件类型经过登记后,就可以pop了,否则,系统会
//      拒绝pop该类型事件
//参数：mark，本事件类型是否mark型事件，true = 是，false = 否
//      overlay，若是mark型事件，新事件参数是否覆盖队列中已有事件，非mark事件无效
//      default_prio，本事件类型的默认优先级。
//      vpus_limit，同一事件本类型事件所能拥有的线程数量的最大值
//      thread_routine，线程入口函数(事件处理函数)
//      stack_size，执行thread_routine需要的栈尺寸，不包括thread_routine函数可能
//          调用的系统服务。
//      evtt_name，事件类型名，不同模块之间要交叉弹出事件的话，用事件类型名。
//          本参数允许是NULL，但只要非NULL，就不允许与事件类型表中已有的名字重名
//返回：新事件类型的类型号
//------------------------------------------------------------------------------
uint16_t djy_evtt_regist(bool_t mark,bool_t overlay,
                       ufast_t default_prio,
                       uint16_t vpus_limit,
                       void (*thread_routine)(struct event_script *),
                       uint32_t stack_size,
                       char *evtt_name)
{
    uint16_t i,evtt_id;
    if((default_prio >= cn_prio_sys_service) || (default_prio == 0))
    {
        djy_error_login(enum_knl_invalid_prio,"事件类型优先级非法");
        return cn_invalid_evtt_id;
    }
    int_save_asyn_signal();      //禁止调度也就是禁止异步事件
    for(evtt_id=0; evtt_id<cn_evtts_limit; evtt_id++)   //查找空闲的事件控制块
        if( tg_evtt_table[evtt_id].property.registered == 0)
            break;
    if(evtt_id == cn_evtts_limit)     //没有空闲事件控制块
    {
        djy_error_login(enum_knl_no_free_etcb,"没有空闲事件控制块");
        int_restore_asyn_signal();
        return cn_invalid_evtt_id;
    }else if(evtt_name != NULL) //新类型有名字，需检查有没有重名
    {
        for(i=0; i<cn_evtts_limit; i++)
        {
		    if(strcmp(tg_evtt_table[i].evtt_name,evtt_name) == 0)
			{
                djy_error_login(enum_knl_evtt_homonymy,"事件类型重名");
                int_restore_asyn_signal();
                return cn_invalid_evtt_id;
			}
        }
        if(djy_rtstrlen(evtt_name,31) != cn_limit_uint32)
            strcpy(tg_evtt_table[evtt_id].evtt_name,evtt_name);
        else
        {
            memcpy(tg_evtt_table[evtt_id].evtt_name,evtt_name,31);
            tg_evtt_table[evtt_id].evtt_name[31] = '\0';
        }
    }
    tg_evtt_table[evtt_id].property = (struct evtt_property){0,0,1,0};
    tg_evtt_table[evtt_id].property.mark = mark;
    tg_evtt_table[evtt_id].property.overlay = overlay;
    tg_evtt_table[evtt_id].my_free_vm = NULL;
    tg_evtt_table[evtt_id].default_prio = default_prio;
    tg_evtt_table[evtt_id].pop_sum = 0;
    tg_evtt_table[evtt_id].done_sum = 0;
    tg_evtt_table[evtt_id].repeats = 0;
    tg_evtt_table[evtt_id].vpus_limit = vpus_limit;
    tg_evtt_table[evtt_id].thread_routine = thread_routine;
    tg_evtt_table[evtt_id].stack_size = stack_size;
    tg_evtt_table[evtt_id].mark_unclear = NULL;
    tg_evtt_table[evtt_id].done_sync = NULL;
    tg_evtt_table[evtt_id].pop_sync = NULL;
    if((cn_run_mode!=cn_mode_mp)||(default_prio<0x80))
    {//运行模式为si或dlsp，或该事件类型拥有紧急优先级，需预先创建一个线程虚拟机
        tg_evtt_table[evtt_id].my_free_vm =
                                __create_thread(&tg_evtt_table[evtt_id]);
        if(tg_evtt_table[evtt_id].my_free_vm == NULL)
        {//内存不足，不能创建虚拟机
            djy_error_login(enum_mem_tried,"创建虚拟机时内存不足");
            return cn_invalid_evtt_id;
        }else
            tg_evtt_table[evtt_id].vpus = 1;
    }else
        tg_evtt_table[evtt_id].vpus = 0;
    int_restore_asyn_signal();
    return evtt_id;
}

//----取事件类型id-------------------------------------------------------------
//功能：根据事件类型的名字，返回该事件类型的id号，不查找没名字的事件类型。
//参数：evtt_name，事件类型名
//返回：事件类型id号，如果没有找到则返回cn_invalid_id。
//备注：只能找到有名字的事件类型，没名字的事件类型又叫隐身事件类型。
//----------------------------------------------------------------------------
uint16_t djy_get_evtt_id(char *evtt_name)
{
    uint16_t loop;
    if(evtt_name == NULL)
        return cn_invalid_evtt_id;
    for(loop = 0; loop < cn_evtts_limit; loop++)
    {
        if(strcmp(tg_evtt_table[loop].evtt_name,evtt_name) ==0)
            return loop;
    }
    return cn_invalid_evtt_id;
}

//----删除事件类型-------------------------------------------------------------
//功能: 删除一个事件类型,如果队列中还有该类型事件(in_use == 1),只标记该类型为被
//      删除,真正的删除工作是在 done函数里完成的.如果队列中已经没有该类型事件了,
//      将会执行真正的删除操作.无论哪种情况,系统均会拒绝弹出该类型的新事件.
//参数: evtt_id,待删除的事件类型号
//返回: true = 成功，包括成功标记；false = 失败
//-----------------------------------------------------------------------------
bool_t djy_evtt_unregist(uint16_t evtt_id)
{
    struct thread_vm *next_vm,*temp;
    struct event_type *evtt;
    bool_t result = true;
    if(evtt_id >= cn_evtts_limit)
        return false;
    evtt = &tg_evtt_table[evtt_id];
    if((evtt->property.in_use)||(evtt->done_sync != NULL)
                              || (evtt->pop_sync != NULL))
    {
        //事件类型正在使用或完成同步和弹出同步队列非空，不允许删除
        result = false;
    }else
    {
        next_vm = evtt->my_free_vm;
        //回收事件类型控制块，只需把registered属性清零。
        evtt->property.registered = 0;
        evtt->evtt_name[0] = '\0';  //清空事件类型名
        while(next_vm != NULL)        //释放该事件类型拥有的空闲虚拟机
        {
            temp = next_vm;
            next_vm = next_vm->next;
            m_free((void *)temp);
        }
    }
    return result;
}

//----建立事件链表-------------------------------------------------------------
//功能：1.根据系统设定初始化操作系统虚拟机和事件表指针
//      2.把 cn_events_limit 个事件控制块结构连接成一个空闲队列,队列的结构参见
//        文档，由指针pg_free_event索引,
//参数：无
//返回：无
//-----------------------------------------------------------------------------
void __djy_init_sys(void)
{
    uint16_t i;
    struct thread_vm *vm;
    pg_event_delay=NULL;    //延时事件链表空

    //把事件类型表全部置为没有注册,0为系统服务类型
    for(i=1; i<cn_evtts_limit; i++)
    {
        tg_evtt_table[i].property.registered = 0;
    }
    for(i = 1; i < cn_events_limit; i++)    //i=0为系统服务事件
    {
        if(i==(cn_events_limit-1))
            tg_event_table[i].next = NULL;
        else
            tg_event_table[i].next = &tg_event_table[i+1];
        //向前指针指向pg_event_free的地址,说明这事个空闲事件块.
        //没有别的含义,只是找一个唯一且不变的数值,全局变量地址在整个运行期
        //是不会变化的.
        tg_event_table[i].previous = (struct  event_script*)&pg_event_free;
        tg_event_table[i].event_id = i;    //本id号在程序运行期维持不变
    }
    pg_event_free = &tg_event_table[1];

    tg_evtt_table[0].property = (struct evtt_property){1,0,1,1};
    tg_evtt_table[0].my_free_vm = NULL;
    tg_evtt_table[0].evtt_name[0] = '\0';
    tg_evtt_table[0].default_prio = cn_prio_sys_service;
    tg_evtt_table[0].pop_sum = 0;
    tg_evtt_table[0].done_sum = 0;
    tg_evtt_table[0].repeats = 1;
    tg_evtt_table[0].vpus_limit =1;
    tg_evtt_table[0].vpus = 1;
    tg_evtt_table[0].thread_routine = __djy_service;
    tg_evtt_table[0].stack_size = 1024;
    tg_evtt_table[0].mark_unclear = tg_event_table;
    tg_evtt_table[0].done_sync = NULL;
    tg_evtt_table[0].pop_sync = NULL;

    vm = __create_thread(&tg_evtt_table[0]);
    if(vm == NULL)      //内存不足，不能创建常驻虚拟机
    {
        djy_error_login(enum_mem_tried,"创建虚拟机时内存不足");
        tg_evtt_table[0].vpus = 0;
        return ;
    }
    //以下模拟popup函数,弹出系统服务事件.当事件队列为空时,调用popup的结果
    //是不可预知的.由于系统运行时,系统服务事件总在队列中,所以事件队列是不会空
    //的,这里模拟popup事件,而不是直接调用,可以使popup函数中省掉一个判断队列是否
    //空的操作.
    tg_event_table[0] = (struct event_script){
                        tg_event_table,tg_event_table,  //next,previous
                        tg_event_table,tg_event_table,//multi_next,multi_previous
                        vm,                         //vm
                        NULL,                       //sync
                        NULL,                       //sync_head
                        0,                          //start_time
                        0,                          //consumed_time
                        0,                          //delay_start
                        0,                          //delay_end
                        enum_knl_no_error,            //error_no
                        0,0,                        //parameter0,parameter1
                        0,                          //wait_mem_size
                        {0},                          //last_status
                        {0},                          //event_status
                        cn_prio_sys_service,        //prio
                        0,                          //evtt_id
                        0,                          //sync_counter
                        0,                          //event_id
                        1,                          //repeats
                        NULL,0                   //held_device,local_memory
                        };
    tg_event_table[0].event_status.bit.event_ready = 1;
    pg_event_ready = tg_event_table;
    pg_event_running = pg_event_ready;
}

//--------检查是否允许调度------------------------------------------------------
//功能: 检查是否允许调度,允许异步信号且运行在线程态时，允许调度
//参数: 无
//返回: 允许调度返回true,否则返回false
//-----------------------------------------------------------------------------
bool_t djy_query_sch(void)
{
    return ((tg_int_global.en_asyn_signal)
                && (tg_int_global.nest_asyn_signal == 0)
                && (tg_int_global.en_trunk));
}

//----把事件插入就绪队列中----------------------------------------------------
//功能：把事件插入到就绪队列中合适的位置,该事件原来不在就绪队列中.
//参数：event_ready,待插入的事件,该事件原来不在就绪队列中
//返回：无
//------------------------------------------------------------------------------
void __djy_event_ready(struct  event_script *event_ready)
{
    struct  event_script *event;
    int_save_asyn_signal();
    event_ready->event_status.bit.event_ready = 1;
    event = pg_event_ready;
    do
    { //找到一个优先级低于新事件的事件.由于系统服务事件总是ready,因此总是能找到.
        if(event->prio <= event_ready->prio)
            event = event->next;
        else
            break;
    }while(event != pg_event_ready);
    event_ready->next = event;
    event_ready->previous = event->previous;
    event->previous->next = event_ready;
    event->previous = event_ready;

    //新插入的事件在同优先级的最后，故这样能够判断新事件是否该优先级的唯一事件。
    //若是该优先级的唯一事件，也即是该优先级首事件，则需要将其插入首事件链表
    if(event_ready->prio != event_ready->previous->prio)
    {
        event = event_ready->next;
        event->multi_previous->multi_next = event_ready;
        event_ready->multi_previous = event->multi_previous;
        event->multi_previous = event_ready;
        event_ready->multi_next = event;
    }else       //不是该优先级的首事件
    {
        event_ready->multi_next = NULL;
        event_ready->multi_previous = NULL;
    }
    if(event_ready->prio < pg_event_ready->prio)
        pg_event_ready = event_ready;
    int_restore_asyn_signal();
}

//----退出闹钟同步队列---------------------------------------------------------
//功能: 把一个事件从闹钟同步队列中取出,不管闹铃时间是否已经到达.用在带超时的同步
//      功能中，当同步条件先于超时时限到达时，从闹钟同步队列取出事件。
//参数: delay_event,待处理的事件.
//返回: 无
//备注: 1.本函数不开放给用户,仅仅是操作系统内部使用.操作系统不给用户提供提前
//      退出延时的功能,这样可以防止事件间互相干涉
//      2.本函数应该在关闭调度条件下调用,调用者保证,函数内部不检查中断状态.
//      3.本函数只把事件从闹钟同步链表中取出，并不放到就绪队列中。
//-----------------------------------------------------------------------------
void __djy_resume_delay(struct  event_script *delay_event)
{
    if(pg_event_delay->next == pg_event_delay)  //队列中只有一个事件
        pg_event_delay = NULL;
    else
    {
        if(delay_event == pg_event_delay)
            pg_event_delay = pg_event_delay->previous;
        delay_event->next->previous = delay_event->previous;
        delay_event->previous->next = delay_event->next;
    }
    delay_event->next = NULL;
    delay_event->previous = NULL;
    delay_event->delay_end = djy_get_time();
}

//----加入延时队列------------------------------------------------------------
//功能：由正在执行的事件调用,直接把自己加入延时队列，不引起调度也不操作ready
//      队列，一般由同步函数调用，在timeout!=0时把事件加入闹钟同步队列实现
//      timeout功能，是特定条件下对y_timer_sync函数的简化。
//参数：u32l_mS,延迟时间,单位是毫秒，将被向上调整为cn_tick_ms的整数倍
//返回：无
//备注：1、操作系统内部使用的函数，且需在关闭中断（禁止调度）的条件下使用。
//      2、调用本函数是running事件已经从就绪表中取出，本函数不改变就绪表。
//      3、与其他内部函数一样，由调用方保证参数合理性，即u32l_mS>0.
//-----------------------------------------------------------------------------
void __djy_addto_delay(uint32_t u32l_mS)
{
    struct  event_script * event;
    uint32_t start;

    pg_event_running->delay_start = djy_get_time();   //事件延时开始时间
    pg_event_running->delay_end = pg_event_running->delay_start
                + (u32l_mS + cn_tick_ms -1)/cn_tick_ms;  //延时结束时间

    if(pg_event_delay==NULL)    //延时队列空
    {
        pg_event_running->next = pg_event_running;
        pg_event_running->previous = pg_event_running;
        pg_event_delay=pg_event_running;
    }else
    {
        event = pg_event_delay;
        start=pg_event_running->delay_start;
        do
        {//本循环找到第一个剩余延时时间大于新延时事件的事件.
            //如果1tick=1mS,那么49.7天后将回绕,yos不能提供超过49.7天的延时
            //如果直接比较delay_end,则一个发生回绕而另一个不回绕时,将会出错,
            //-start可以避免回绕时发生错误
            if((event->delay_end - start)
                            <= (pg_event_running->delay_end - start))
                event = event->next;
            else
                break;
        }while(event != pg_event_delay);
        //如果没有找到剩余延时时间比新延时事件长的事件,新事件插入队列尾,
        //而队列尾部就是pg_event_delay的前面,event恰好等于pg_event_delay
        //如果找到剩余延时时间长于新事件的事件,新事件插入该事件前面.
        //所以无论前面循环找到与否,均可使用下列代码
        pg_event_running->next = event;
        pg_event_running->previous = event->previous;
        event->previous->next = pg_event_running;
        event->previous = pg_event_running;
        if(pg_event_delay->delay_end -start > pg_event_running->delay_end-start)
            //新事件延时小于原队列中的最小延时.
            pg_event_delay = pg_event_running;
    }
}

//----清除mark型事件标记-------------------------------------------------------
//功能: 用于mark型事件，一条mark型事件发生后，清除标记前，如果再次发生同类型的
//      事件，只是简单地repeats增量，在清除标记后，再次发生则登记新事件。登记事
//      件时，若前一条事件已经完成，则加入ready队列，并由和mark_unclear指针索引，
//      若前一条事件未完成，则只由mark_unclear指针索引，待前一条事件完成在加入
//      ready队列，以确保mark型事件只有一条在处理。
//参数: 无
//返回: 无
//-----------------------------------------------------------------------------
void djy_clear_mark(void)
{
    struct  event_type *pl_evtt;  //被操作的事件的类型指针
    pl_evtt = &tg_evtt_table[pg_event_running->evtt_id];
    pl_evtt->repeats = 0;   //mark标识状态量，一次全处理
    pl_evtt->mark_unclear = NULL;
}

//----闹钟同步-----------------------------------------------------------------
//功能：由正在执行的事件调用,使自己暂停u32l_mS毫秒后继续运行.
//参数：u32l_mS,延迟时间,单位是毫秒，0且允许轮转调度则把事件放到同优先级的
//      最后。非0值将被向上调整为cn_tick_ms的整数倍
//返回：实际延时时间(ticks)数
//备注：延时队列为双向循环链表
//-----------------------------------------------------------------------------
uint32_t djy_timer_sync(uint32_t u32l_mS)
{
    struct  event_script * event;
    uint32_t start;

    if( !djy_query_sch())
    {   //禁止调度，不能进入闹钟同步状态。
        djy_error_login(enum_knl_cant_sched,NULL);
        return 0;
    }
    int_save_asyn_signal();
    //延时量为0的算法:就绪队列中有同优先级的，把本事件放到轮转最后一个，
    //注意:这里不管轮转调度是否允许
    if(u32l_mS == 0)
    {
        if((pg_event_running->prio == pg_event_running->next->prio)
                    && (pg_event_running != pg_event_running->next)   )
        {
            pg_event_running->delay_start = djy_get_time();   //设定闹铃的时间
            __djy_cut_ready_event(pg_event_running);      //从同步队列取出
            __djy_event_ready(pg_event_running);            //放回同步队列尾部
        }else
        {
            int_restore_asyn_signal();
            return 0;   //延时量为0，且不符合轮转条件
        }
    }else
    {
        pg_event_running->delay_start = djy_get_time();   //设定闹铃的时间
        pg_event_running->delay_end = pg_event_running->delay_start
                    + (u32l_mS + cn_tick_ms -1)/cn_tick_ms;  //闹铃时间

        __djy_cut_ready_event(pg_event_running);

        pg_event_running->last_status.all = pg_event_running->event_status.all;
        pg_event_running->event_status.bit.event_delay=1;
        if(pg_event_delay==NULL)    //闹钟同步队列空
        {
            pg_event_running->next = pg_event_running;
            pg_event_running->previous = pg_event_running;
            pg_event_delay=pg_event_running;
        }else
        {
            event = pg_event_delay;
            start=pg_event_running->delay_start;
            do
            {//本循环找到第一个闹铃时间晚于新事件的事件.
                //如果1tick=1mS,那么49.7天后将回绕,djyos不能提供超过49.7天的延时
                //如果直接比较delay_end,则一个发生回绕而另一个不回绕时,将会出错,
                //-start可以避免回绕时发生错误
                if((event->delay_end - start)
                                <= (pg_event_running->delay_end - start))
                    event = event->next;
                else
                    break;
            }while(event != pg_event_delay);
            //下面把新事件插入前述找到的事件前面，如没有找到，则event将等于
            //pg_event_delay，因是双向循环队列，g_event_delay前面也就刚好是队列尾。
            pg_event_running->next = event;
            pg_event_running->previous = event->previous;
            event->previous->next = pg_event_running;
            event->previous = pg_event_running;
            if(pg_event_delay->delay_end -start
                    >pg_event_running->delay_end-start)
                //新事件延时小于原队列中的最小延时.
                pg_event_delay = pg_event_running;
        }
    }
    int_restore_asyn_signal();
    return (djy_get_time() - pg_event_running->delay_start)*cn_tick_ms;
}

//----同步事件----------------------------------------------------------------
//功能: 把正在运行的事件加入到指定事件的同步队列中去,然后重新调度
//参数: event_id,目标事件id号
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: cn_sync_success=同步条件成立返回，
//      cn_sync_timeout=超时返回，
//      cn_sync_error=出错，
//----------------------------------------------------------------------------
uint32_t djy_event_sync(uint16_t event_id,uint32_t timeout)
{
    struct  event_script * pl_ecb;
    pl_ecb = &tg_event_table[event_id];

    if(djy_query_sch() == false)  //不能在禁止调度的情况下执行同步操作
        return cn_sync_error;
    if(timeout == 0)
        return cn_sync_timeout;
    int_save_asyn_signal();
    if(pl_ecb->previous == (struct event_script *)&pg_event_free)
    {//目标事件是空闲事件
        int_restore_asyn_signal();
        return cn_sync_error;
    }
    __djy_cut_ready_event(pg_event_running);
    pg_event_running->next = NULL;
    pg_event_running->previous = NULL;

    //以下把pg_event_running加入到目标事件的同步队列中

    pg_event_running->sync_head = &pl_ecb->sync;
    if(pl_ecb->sync != NULL)
    {//被同步事件的同步队列不是空的
        pg_event_running->multi_next = pl_ecb->sync;
        pg_event_running->multi_previous = pl_ecb->sync->multi_previous;
        pl_ecb->sync->multi_previous->multi_next = pg_event_running;
        pl_ecb->sync->multi_previous = pg_event_running;
    }else
    {//被同步事件的同步队列是空的
        pl_ecb->sync = pg_event_running;
        pg_event_running->multi_next = pg_event_running;
        pg_event_running->multi_previous = pg_event_running;
    }

    pg_event_running->last_status.all = pg_event_running->event_status.all;
    pg_event_running->event_status.bit.event_sync = 1;
    if(timeout != cn_timeout_forever)
    {
        pg_event_running->event_status.bit.wait_overtime = 1;
        __djy_addto_delay((timeout + cn_tick_ms -1)/cn_tick_ms);
    }
    int_restore_asyn_signal();  //恢复中断会引发重新调度

    int_save_asyn_signal();
    //检查从哪里返回，是超时还是同步事件完成。
    if(pg_event_running->event_status.bit.event_sync)
    {//说明同步条件未到，从超时返回，应从目标事件的同步队列中取出事件。
     //此时，被同步的事件肯定还没有完成。
        pg_event_running->event_status.bit.event_sync = 0;
        int_restore_asyn_signal();
        return cn_sync_timeout;
    }else
    {
        int_restore_asyn_signal();
        return cn_sync_success;
    }
}

//----事件类型完成同步----------------------------------------------------------
//功能: 把正在运行的事件加入到指定事件类型的前同步队列中去,然后重新调度。完成
//      同步以目标事件类型的完成次数为同步条件。
//参数: evtt_id,目标事件类型号
//      done_times,完成次数，0表示待最后一条该类型事件完成.
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: cn_sync_success=同步条件成立返回，
//      cn_sync_timeout=超时返回，
//      cn_sync_error=出错，
//      本函数必须联系共享文档的相关章节才容易读懂，注释难于解释那么清楚的。
//----------------------------------------------------------------------------
uint32_t djy_evtt_done_sync(uint16_t evtt_id,uint16_t done_times,uint32_t timeout)
{
    struct  event_type *evtt;
    struct event_script *pl_ecb;
    evtt = &tg_evtt_table[evtt_id];
    if(djy_query_sch() == false)  //不能在禁止调度的情况下执行同步操作
        return cn_sync_error;
    if(timeout == 0)
        return cn_sync_timeout;
    int_save_asyn_signal();
    if(evtt->property.registered == false)
    {//该事件类型是无效事件类型
        int_restore_asyn_signal();
        return cn_sync_error;
    }
    __djy_cut_ready_event(pg_event_running);
    pg_event_running->next = NULL;
    pg_event_running->previous = NULL;
    pg_event_running->sync_counter = done_times;

    //以下把pg_event_running加入到目标事件的同步队列中
    pg_event_running->sync_head = &evtt->done_sync;
    if(evtt->done_sync != NULL)
    {//被同步事件类型的同步队列不是空的
        pl_ecb = evtt->done_sync;
        do
        {//本循环找到第一个剩余完成次数不小于新事件的事件.
            if(pl_ecb->sync_counter < done_times)
                pl_ecb = pl_ecb->multi_next;
            else
                break;
        }while(pl_ecb != evtt->done_sync);
        //下面把新事件(pg_event_running)插入到队列中
        pg_event_running->multi_next = pl_ecb;
        pg_event_running->multi_previous = pl_ecb->multi_previous;
        pl_ecb->multi_previous->multi_next = pg_event_running;
        pl_ecb->multi_previous = pg_event_running;
        if(evtt->done_sync->sync_counter > done_times)
            evtt->done_sync = pg_event_running;
    }else
    {//被同步事件类型的同步队列是空的
        evtt->done_sync = pg_event_running;
        pg_event_running->multi_next = pg_event_running;
        pg_event_running->multi_previous = pg_event_running;
    }

    pg_event_running->last_status.all = pg_event_running->event_status.all;
    pg_event_running->event_status.bit.evtt_done_sync = 1;
    if(timeout != cn_timeout_forever)
    {
        pg_event_running->event_status.bit.wait_overtime = 1;
        __djy_addto_delay((timeout + cn_tick_ms -1)/cn_tick_ms);
    }
    int_restore_asyn_signal();  //恢复调度会引发事件切换

    int_save_asyn_signal();
    //检查从哪里返回，是超时还是同步事件完成。
    if(pg_event_running->event_status.bit.evtt_done_sync)
    {//说明同步条件未到，从超时返回，应从目标事件的同步队列中取出事件。
     //此时，被同步的事件肯定还没有完成。
        pg_event_running->event_status.bit.evtt_done_sync = 0;
        int_restore_asyn_signal();
        return cn_sync_timeout;
    }else
    {
        int_restore_asyn_signal();
        return cn_sync_success;
    }
}
//----事件类型弹出同步---------------------------------------------------------
//功能: 把正在运行的事件加入到指定事件类型的弹出同步队列中去,然后重新调度。弹出
//      同步是指以该事件类型的事件弹出若干次为同步条件。
//参数: evtt_id,目标事件类型号
//      pop_times,弹出次数，不能是0.
//      timeout，超时设置,单位是毫秒，cn_timeout_forever=无限等待，0则立即按
//      超时返回。非0值将被向上调整为cn_tick_ms的整数倍
//返回: cn_sync_success=同步条件成立返回，
//      cn_sync_timeout=超时返回，
//      cn_sync_error=出错，
//      本函数必须联系共享文档的相关章节才容易读懂，注释难于解释那么清楚的。
//----------------------------------------------------------------------------
uint32_t djy_evtt_pop_sync(uint16_t evtt_id,uint16_t pop_times,uint32_t timeout)
{
    struct  event_type *evtt;
    struct event_script *pl_ecb;
    evtt = &tg_evtt_table[evtt_id];
    //不能在禁止调度的情况下执行同步操作,弹出次数不能设为0
    if((djy_query_sch() == false) || (pop_times == 0))
        return cn_sync_error;
    if(timeout == 0)
        return cn_sync_timeout;
    int_save_asyn_signal();
    if(evtt->property.registered == false)
    {//该事件类型是无效事件类型
        int_restore_asyn_signal();
        return cn_sync_error;
    }
    __djy_cut_ready_event(pg_event_running);
    pg_event_running->next = NULL;
    pg_event_running->previous = NULL;
    pg_event_running->sync_counter = pop_times;

    //以下把pg_event_running加入到目标事件的同步队列中
    pg_event_running->sync_head = &evtt->pop_sync;
    if(evtt->pop_sync != NULL)
    {//被同步事件的同步队列不是空的
        pl_ecb = evtt->pop_sync;
        do
        {//本循环找到第一个剩余弹出次数不小于新事件的事件.
            if(pl_ecb->sync_counter < pop_times)
                pl_ecb = pl_ecb->multi_next;
            else
                break;
        }while(pl_ecb != evtt->pop_sync);
        //下面把新事件(pg_event_running)插入到队列中
        pg_event_running->multi_next = pl_ecb;
        pg_event_running->multi_previous = pl_ecb->multi_previous;
        pl_ecb->multi_previous->multi_next = pg_event_running;
        pl_ecb->multi_previous = pg_event_running;
        if(evtt->pop_sync->sync_counter > pop_times)
            evtt->pop_sync = pg_event_running;
    }else
    {//被同步事件的同步队列是空的
        evtt->pop_sync = pg_event_running;
        pg_event_running->multi_next = pg_event_running;
        pg_event_running->multi_previous = pg_event_running;
    }

    pg_event_running->last_status.all = pg_event_running->event_status.all;
    pg_event_running->event_status.bit.evtt_pop_sync = 1;
    if(timeout != cn_timeout_forever)
    {
        pg_event_running->event_status.bit.wait_overtime = 1;
        __djy_addto_delay((timeout + cn_tick_ms -1)/cn_tick_ms);
    }
    int_restore_asyn_signal();  //恢复中断会引发重新调度

    int_save_asyn_signal();
    //检查从哪里返回，是超时还是同步事件完成。
    if(pg_event_running->event_status.bit.evtt_pop_sync)
    {//说明同步条件未到，从超时返回，应从目标事件的同步队列中取出事件。
     //此时，同步条件肯定还没有达成。
        pg_event_running->event_status.bit.evtt_pop_sync = 0;
        int_restore_asyn_signal();
        return cn_sync_timeout;
    }else
    {
        int_restore_asyn_signal();
        return cn_sync_success;
    }
}
//----报告事件------------------------------------------------------------------
//功能：向操作系统报告发生事件,操作系统接报后,将分配或建立合适的线程处理事件.
//参数：event_type,发生的事件类型
//      parameter0，parameter1,事件参数
//      prio,事件优先级,0表示使用默认值(存在事件类型表pg_event_type_table中)
//返回：若不能获取事件控制块，返回-1，否则返回事件id。
//注: 不会因不能获得事件控制块而阻塞。
//------------------------------------------------------------------------------
uint16_t djy_event_pop(   uint16_t evtt_id,
                        uint32_t parameter0,
                        uint32_t parameter1,
                        ufast_t prio)
{
    struct  event_script *pl_ecb;
    struct  event_type *pl_evtt;
    uint16_t result;
    ufast_t my_prio;
    if(evtt_id >= cn_evtts_limit)
        return cn_invalid_event_id;
    pl_evtt =&tg_evtt_table[evtt_id];
    if((pl_evtt->property.registered == 0)      //类型未登记
        || (prio >= cn_prio_sys_service))       //不能pop休眠事件
    {
        djy_error_login(enum_knl_etcb_error,NULL);
        return cn_invalid_event_id;
    }

    int_save_asyn_signal(); //关异步信号(关调度)
    pl_evtt->pop_sum++;
    //下面处理弹出同步队列，必须联系共享文档的相关章节才容易读懂，注释难于解释
    //那么清楚的。
    while(pl_evtt->pop_sync != NULL)
    {
        pl_ecb = pl_evtt->pop_sync;
        if(pl_ecb->sync_counter == 1)
        {
            pl_ecb->last_status.all = pl_ecb->event_status.all;
            pl_ecb->event_status.bit.evtt_pop_sync = 0;
            if(pl_ecb->event_status.bit.wait_overtime)  //指定的超时未到
            {
                __djy_resume_delay(pl_ecb);       //从闹钟队列中移除事件
                pl_ecb->event_status.bit.wait_overtime = 0;
            }
            if(pl_ecb->multi_next == pl_ecb->multi_previous)
            {//是最后一个事件
                pl_evtt->pop_sync = NULL;
            }else
            {
                pl_evtt->pop_sync = pl_ecb->multi_next;
                pl_ecb->multi_next->multi_previous = pl_ecb->multi_next;
                pl_ecb->multi_previous->multi_next = pl_ecb->multi_previous;
            }
            __djy_event_ready(pl_ecb);
        }else
        {
            pl_ecb->sync_counter--;
        }
    }

//mark型事件需要正在执行的done才能执行新事件。
//mark型事件在队列中可能的组合为:
//  已clear                  repeats       处理方式
//       未clear  mark_unclear      in_use
//1  有     无         NULL   ==0     1    新事件由mark_unclear指针指向
//2  有     有        !NULL   !=0     1    repeats++,考虑参数替换
//3  无     有        !NULL   !=0     1    repeats++,考虑参数替换
//4  无     无         NULL   ==0     0    新事件加入就绪队列
    if(pl_evtt->mark_unclear != NULL)
    {// mark型事件的第2和第3种情况
        pl_ecb = pl_evtt->mark_unclear;         //取事件控制块
        if(pl_evtt->property.overlay == 1)  //参数替换型,替换参数
        {
            pl_ecb->parameter0 = parameter0;
            pl_ecb->parameter1 = parameter1;
        }
        if(pl_evtt->repeats != cn_limit_uint16)
            pl_evtt->repeats++;         //事件发生次数增量
        result = pl_ecb->event_id;
        goto end_pop;                                       //注意 goto
    }

    if(prio != 0)
        my_prio = prio; //设置事件优先级,
    else
        my_prio = pl_evtt->default_prio;       //从事件类型中继承优先级

    //以下处理非mark型事件或者mark型事件的第1、4种情况
    if(pg_event_free==NULL)     //没有空闲的事件控制块
    {
        djy_error_login(enum_knl_no_free_ecb,NULL);
        result = cn_invalid_event_id;
    }else if((pl_evtt->property.mark == false)
                    && (pl_evtt->repeats >= pl_evtt->vpus_limit))
    {
        djy_error_login(enum_knl_vpu_over,NULL);
        result = cn_invalid_event_id;
    }else                       //有空闲事件控制块
    {

        pl_ecb = pg_event_free;         //从空闲链表中提取一个事件控制块
        pg_event_free=pg_event_free->next;  //空闲事件控制块数量减1
        if(pl_evtt->repeats != cn_limit_uint16)
            pl_evtt->repeats++;         //repeats增量
        //设置新事件的参数
        pl_ecb->next = NULL;
        pl_ecb->previous = NULL;
        pl_ecb->multi_next = NULL;
        pl_ecb->multi_previous = NULL;
        pl_ecb->vm = NULL;
        pl_ecb->sync = NULL;
        pl_ecb->sync_head = NULL;
        pl_ecb->start_time=djy_get_time();   //事件发生时间
        pl_ecb->consumed_time = 0;
        pl_ecb->delay_start = 0;
        pl_ecb->delay_end = 0;
        pl_ecb->error_no = 0;
        pl_ecb->parameter0=parameter0;          //设置事件参数
        pl_ecb->parameter1=parameter1;          //设置事件参数
        pl_ecb->wait_mem_size = 0;
        pl_ecb->last_status.all = 0;
        pl_ecb->event_status.all = 0;
        pl_ecb->prio = my_prio;
        pl_ecb->evtt_id=evtt_id;          //设置事件类型
        pl_ecb->repeats = 0;
        pl_ecb->held_device = NULL;
        pl_ecb->local_memory = 0;
        if(pl_evtt->property.mark == true)    //这是mark型事件
        {
            pl_evtt->mark_unclear = pl_ecb;
            if(pl_evtt->property.in_use == 1)   //第1种情况
            {
            }else                               //第4种情况
            {
                __djy_event_ready(pl_ecb);
            }
        }else                                   //非mark型事件
            __djy_event_ready(pl_ecb);
        pl_evtt->property.in_use = 1;
        result = pl_ecb->event_id;
    }


end_pop:
    int_restore_asyn_signal();  //恢复中断状态
    return result;
}

//----启动操作系统--------------------------------------------------------------
//功能: 初始化时钟嘀嗒，复位异步信号使能状态，选出最高优先级事件，调用
//      __asm_turnto_context把上下文切换到这个事件
//参数: 无
//返回: 无
//_____________________________________________________________________________
void __djy_start_os(void)
{
    //本句为容错性质，以防用户模块初始化过程中没有成对调用异步信号使能与禁止函数
    __int_reset_asyn_signal();
    __djy_init_tick();
    __djy_select_event_to_run();
    pg_event_running = pg_event_ready;
    __asm_turnto_context(pg_event_running->vm);
}

//----完成事件------------------------------------------------------------------
//功能：向操作系统报告事件已经完成,操作系统接到报告后,完成清理工作.
//      1.如果事件同步队列非空，把同步事件放到ready表。
//      2.如果持有设备，释放之，并把该设备等待队列中的第一个事件放到ready表。
//      3.设置该事件类型控制块的参数。
//      4.把事件块各分量设置到初始态,并放入free表。
//      5.如果是非实时虚拟机,释放动态分配的内存(如果有)
//  如果是线程需保留，则:
//      6.复位虚拟机。
//      7.不保存上下文，直接切换到新的ready表头
//  如果线程无需保留
//      6.释放虚拟机内存。
//      7.不保存上下文，直接切换到新的ready表头
//参数：无
//返回：无
//备注: 调用本函数的必定是running事件,在running事件上下文中执行，不可以调用
//      __asm_reset_thread函数。
//-----------------------------------------------------------------------------
#define cn_deliver_to   0   //虚拟机已经转交
#define cn_keep         1   //虚拟机保留不删除
#define cn_delete       2   //虚拟机应该被删除
void djy_event_done(void)
{
    struct  event_script *pl_ecb;
    struct  event_type   *pl_evtt;
    struct  event_script *pl_ecb_temp;
    ucpu_t  vm_final = cn_delete;

    //此处不用int_save_asyn_signal函数，可以在应用程序有bug，没有成对调用
    //int_save_asyn_signal和int_restore_asyn_signal的情况下，确保错误到此为止。
    __int_reset_asyn_signal();  //直到__vm_engine函数才再次打开.

    //下面处理同步队列，必须联系共享文档的相关章节才容易读懂，注释难于解释
    //那么清楚的。
    pl_ecb = pg_event_running->sync;     //取同步队列头
    while(pl_ecb != NULL)
    {
        pl_ecb->last_status.all = pl_ecb->event_status.all;   //保存当前状态
        pl_ecb->event_status.bit.event_sync = 0;     //取消"同步中"状态
        if(pl_ecb->event_status.bit.wait_overtime)   //是否在超时队列中
        {
            __djy_resume_delay(pl_ecb);    //结束超时等待
            pl_ecb->event_status.bit.wait_overtime = 0;  //取消"超时等待中"状态
        }
        pl_ecb_temp = pl_ecb;
        if(pl_ecb->multi_next == pg_event_running->sync)  //是最后一个事件
        {
            pg_event_running->sync = NULL;  //置空事件同步队列
            pl_ecb = NULL;
        }else
        {
            pl_ecb = pl_ecb->multi_next;      //取队列中下一个事件
        }
        __djy_event_ready(pl_ecb_temp);           //把事件加入到就绪队列中
    }

    dev_cleanup(pg_event_running); //检查并关闭未关闭的设备
    if(pg_event_running->local_memory != 0)
    {
        __m_cleanup(pg_event_running->event_id);    //
    }

    __djy_cut_ready_event(pg_event_running);
    pg_event_running->previous
                    = (struct  event_script*)&pg_event_free;//表示本控制块空闲
    pg_event_running->next = pg_event_free;     //pg_event_free是单向非循环队列
    pg_event_free = pg_event_running;

    //下面处理完成同步队列，必须联系共享文档的相关章节才容易读懂，注释难于解释
    //那么清楚的。
    pl_evtt =&tg_evtt_table[pg_event_running->evtt_id];
    pl_evtt->done_sum++;
    while(pl_evtt->done_sync!= NULL)
    {//链表中的事件都是要么没有指定超时，要么时限未到，或者时限虽到但还未开始
     //执行的.其他情况不会在此链表中留下痕迹，type_after_sync位也已经清除

        pl_ecb = pl_evtt->done_sync;
        //同步条件达成的条件: 1、同步计数器为1。
        //2、同步计数器为0且本类型最后一条事件已经处理完
        if((pl_ecb->sync_counter == 1)
                ||((pl_ecb->sync_counter == 0) &&(pl_evtt->repeats == 0)))
        {
            pl_ecb->last_status.all = pl_ecb->event_status.all;
            pl_ecb->event_status.bit.evtt_pop_sync = 0;
            if(pl_ecb->event_status.bit.wait_overtime)  //指定的超时未到
            {
                __djy_resume_delay(pl_ecb);       //从闹钟队列中移除事件
                pl_ecb->event_status.bit.wait_overtime = 0;
            }
            if(pl_ecb->multi_next == pl_ecb->multi_previous)
            {//是最后一个事件
                pl_evtt->done_sync = NULL;
            }else
            {
                pl_evtt->done_sync = pl_ecb->multi_next;
                pl_ecb->multi_next->multi_previous = pl_ecb->multi_next;
                pl_ecb->multi_previous->multi_next = pl_ecb->multi_previous;
            }
            __djy_event_ready(pl_ecb);
        }else
        {
            pl_ecb->sync_counter--;
        }

    }

    //以下看事件的虚拟机如何处理。
    //1、若是mark型事件，则有未clear长度事件则转交，无则看是否常驻来决定是否销毁
    //2、若非mark新事件，则repeats>vpus时把虚拟机链到my_free_vm上，否则看事件类
    //   型的优先级和运行模式来决定是否保留虚拟机
    if(pl_evtt->property.mark == true)      //mark型事件
    {
        pl_ecb = pl_evtt->mark_unclear;     //取新事件指针
        if(pl_ecb == pg_event_running)      //说明用户没有清除mark标记
        {
            pl_evtt->mark_unclear = NULL;
            pl_evtt->property.in_use = 0;
            pl_evtt->repeats = 0;
            djy_error_login(enum_knl_clear_mark,"未清除mark标记");
            if((cn_run_mode!=cn_mode_mp)||(pl_evtt->default_prio<0x80))
            {//运行模式为si或dlsp，或者该事件类型拥有紧急优先级时，须保留线程虚拟机
                vm_final = cn_keep;
            }else
            {
                vm_final = cn_delete;
            }
        }else if(pl_ecb != NULL)    //有新事件发生,虚拟机可以转交
        {
            pl_ecb->vm = pg_event_running->vm;
            vm_final = cn_deliver_to;
            __djy_event_ready(pl_ecb);      //加入就绪链表
        }else       //没有新事件产生，看是否常驻保留
        {
            pl_evtt->property.in_use = 0;
            if((cn_run_mode!=cn_mode_mp)||(pl_evtt->default_prio<0x80))
            {//运行模式为si或dlsp，或者该事件类型拥有紧急优先级时，须保留线程
                vm_final = cn_keep;
            }else
            {
                vm_final = cn_delete;
            }
        }
    }else   //非mark型事件
    {
        if(pl_evtt->repeats > pl_evtt->vpus)    //有未得到虚拟机的事件，保留之
        {
            pl_evtt->repeats--;
            vm_final = cn_keep;
        }else   //没有未得到虚拟机的事件，再看是否需保留
        {
            if(pl_evtt->repeats == 1)   //这是最后一个线程
            {
                pl_evtt->repeats = 0;
                pl_evtt->property.in_use = 0;
                if((cn_run_mode!=cn_mode_mp)||(pl_evtt->default_prio<0x80))
                {//运行模式为si或dlsp，或者该事件类型拥有紧急优先级时，
                 //须保留线程虚拟机
                    vm_final = cn_keep;
                }else
                {
                    vm_final = cn_delete;
                }
            }else                       //这不是最后一个线程
            {
                pl_evtt->repeats--;
                vm_final = cn_delete;
            }
        }

    }

    __djy_select_event_to_run();
    if(vm_final == cn_delete)      //删除虚拟机
    {
        m_free((void*)pg_event_running->vm);    //删除虚拟机
        pl_evtt->vpus--;
        pg_event_running = pg_event_ready;
        u32g_running_start = djy_get_time();
        __asm_turnto_context(pg_event_running->vm);
    }else if(vm_final == cn_keep)    //保留虚拟机
    {
        pg_event_running->vm->next = pl_evtt->my_free_vm;
        pl_evtt->my_free_vm = pg_event_running->vm;
        pl_ecb = pg_event_running;
        pg_event_running = pg_event_ready;
        u32g_running_start = djy_get_time();
        __asm_reset_switch(pl_evtt->thread_routine,
                        pg_event_running->vm,pl_ecb->vm);
    }else               //虚拟机已经转交给另一条事件
    {
        pl_ecb = pg_event_running;
        pg_event_running = pg_event_ready;
        u32g_running_start = djy_get_time();
        __asm_reset_switch(pl_evtt->thread_routine,
                        pg_event_running->vm,pl_ecb->vm);
    }
}

//----查询唤醒原因-------------------------------------------------------------
//功能: 查询正在执行的事件被执行的原因.
//参数: 无
//返回: 唤醒原因,见源程序注释.
//-----------------------------------------------------------------------------
union event_status djy_wakeup_from(void)
{
    return pg_event_running->last_status;
}

//----查询事件id-------------------------------------------------------------
//功能: 根据提供的id号查询事件指针.
//参数: id,事件id
//返回: 事件id对应的事件指针.
//-----------------------------------------------------------------------------
struct  event_script *__djy_lookup_id(uint16_t id)
{
    return &tg_event_table[id];
}
//----虚拟机引擎---------------------------------------------------------------
//功能: 启动虚拟机,执行虚拟机入口函数,事件完成后执行清理工作
//参数: thread_routine 处理该事件类型的函数指针.
//返回: 无
//-----------------------------------------------------------------------------
void __djy_vm_engine(void (*thread_routine)(struct event_script *my_event))
{
//    int_restore_asyn_signal();   //对应在y_event_done函数中关闭的中断
    thread_routine(pg_event_running);  //执行事件类型规定的函数
    //如果thread_routine中未执行y_event_done而是直接返回，则在这里调用，
    //完成清理工作,然后直接切换到新的ready事件,本函数并不返回.
    djy_event_done();
}

//----系统服务-----------------------------------------------------------------
//功能: 操作系统的系统服务功能,提供调试,统计等服务.
//参数: 无
//返回: 永不返回.
//-----------------------------------------------------------------------------
void __djy_service(struct event_script *my_event)
{
    while(1);
}

//----api启动函数--------------------------------------------------------------
//功能: 根据api号调用相应的api函数.
//参数: api_no,api号
//返回: mp模式才用，暂且放在这里
//-----------------------------------------------------------------------------
void djy_api_start(uint32_t api_no)
{
    switch(api_no)
    {
        default :break;
    }
    return;
}
//空函数,未初始化的函数指针指向.
void NULL_func(void)
{}

